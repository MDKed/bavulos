package net.mdked.Jrachgame.hud;

import net.mdked.Jrachgame.core.DasGame;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class TextCloud {
	public float x, y, xW, yW;
	public String msg;

	public TextCloud(float x, float y, String msg) {
		this.x = x + 7*DasGame.xScale;
		this.y = y - 32*DasGame.xScale;
		if (msg != null) 
			this.msg = msg;
		else this.msg = "null";
		//TODO если понадобится большая строка с надписю сделать разделение по словам и вычсисление перекидывания на следующую строку

		xW = this.msg.length() * 10 + 1;
		yW =  16 + 10;
	}

	public void draw(Graphics g) {

		g.setColor(Color.white);
		g.fillRoundRect(x, y, xW, yW, 3);
		g.setLineWidth(3);
		g.setColor(Color.gray);
		//	g.setAntiAlias(true);
		g.drawRoundRect(x, y, xW, yW, 3);
		g.setColor(Color.black);
		g.drawString(msg, x+5, y+5);

	}
}
