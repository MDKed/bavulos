package net.mdked.Jrachgame.hud;

import java.util.ArrayList;

import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.utils.Pifagor;
import net.mdked.Jrachgame.world.DragblObj;
import net.mdked.Jrachgame.world.Entity;
import net.mdked.Jrachgame.world.Facility;
import net.mdked.Jrachgame.world.Minable;
import net.mdked.Jrachgame.world.Mobster;
import net.mdked.Jrachgame.world.Pribor;
import net.mdked.Jrachgame.world.World;
import net.mdked.Jrachgame.world.mapgen.MapManager;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class ActionBar {
	public float x,y;
	private int numQ;
	private ArrayList<Punkt> punkts;
	public boolean draw, init, hglit;
	public Entity tgt;
	public String objInfo;
	public short terrain;
	/**
	 * 
	 * @param x
	 * @param y
	 * @param tgtObj
	 */
	public ActionBar(float x, float y) {
		this.x = x;
		this.y = y;
		numQ = -1;
		punkts = new ArrayList<Punkt>();

		punkts.add(new Punkt("Осмотреть",(byte) 1));
		int[] ax = Pifagor.getKOTRfromfXY(DasGame.mX, DasGame.mY);
		//		System.out.println( ax[0] + "x" + ax[1] );
		terrain = World.getColor(ax[0], ax[1]);
		objInfo = getTerrain(terrain);
		vzWizTer(terrain);

		init = true;
		draw = true;
	}

	private void vzWizTer(short ter) {
		switch (ter) {
		case 1000:
		case 1010:
		case 1020:
			punkts.add(new Punkt("Набрать жидкость", 7));
			break;
		}
	}

	public void setTgt(Entity tgt) {
		if (this.tgt != null) return;
		
		this.tgt = tgt;

		switch (tgt.enttype) {
		case 0:
			//minable
			Minable c = (Minable) tgt;
			switch (c.mtype) {
			case 0:
			case 6:
			case 7:
				objInfo = "Это дерево.";
				break;
			case 1:
				objInfo = "Это камень.";
				break;
			case 2:
			case 3:
				objInfo = "Это кустик.";
				break;
			case 4:
			case 5:
				objInfo = "Это травка.";
				punkts.add(new Punkt("Собрать растение", 8));
				break;
			}
			break;
		case 1:
			//animal
			Mobster a = (Mobster) tgt;
			switch (a.type) {
			case 1:
				objInfo = "Это Олень.";
				break;
			case 2:
				objInfo = "Это Мишка.";
				break;
			case 3:
				objInfo = "Это Человек.";
				break;
			}
			break;

		case 2:
			//facility
			Facility b = (Facility) tgt;
			switch (b.facType) {
			case 1:
				objInfo = "Это палатка.";
				break;
			case 2:
				objInfo = "Это мастерская.";
				break;
			}
			if (Pifagor.getDist(tgt.x, World.player.x, tgt.y, World.player.y)
					<= 40)
				punkts.add(new Punkt("Инв.", 4));
			//System.out.println(" dist " + Pifagor.getDist( tgt.x, World.player.pull.x, tgt.y, World.player.pull.y));
			if (World.player.pullin && b.mojPer(World.player.pull) &&
					Pifagor.getDist(tgt, World.player.pull)
					<= 40) {
				punkts.add(new Punkt("Перер.",3));
			}

			break;

		case 3:
			//sled
			objInfo = "Это следы.";
			break;

		case 6:
			objInfo = "Хм, кто же это?";
			break;

		case 7:
			DragblObj dgrob = (DragblObj) tgt;
			switch (dgrob.dtype) {
			case 0:
				objInfo = "Древесина.";
				break;
			case 2:
				objInfo = "Дохлый олень.";
				break;
			case 3:
				objInfo = "Дохлый медведь.";
				break;
			}
			punkts.add(new Punkt("Тащить.", 2));
			break;

		case 8:
			//pribor
			Pribor d = (Pribor) tgt;
			switch (d.ptype) {
			case 1:
				objInfo = "Костёр";
				//				Pribor prbr = (Pribor) tgt;
				//dobavka topliva

				if (World.player.inventory.haveItem(2, 1) >= 0) {
					//System.out.println(" jopa");
					punkts.add(new Punkt("Подкинуть дровишек.",5));
					//TODO переделать шёлканье по пунктам, сделать отдельный класс - пункт в котором будет храниться необходимая для действия инфа
				}
				if (World.player.inventory.haveItem(1, 1) >= 0) {
					punkts.add(new Punkt("Подкинуть костей.",6));
				}

				break;
			}
			break;
		case 10:
			//drop
			objInfo = "Предметы";
			if (Pifagor.getDist(tgt.x, World.player.x, tgt.y, World.player.y)
					<= 40)
				punkts.add(new Punkt("Инв.", 4));
			break;
		}
	}


	public static String getTerrain(short num) {
		switch (num) {
		case 900:
		case 910:
		case 920:
			return "Море";

		case 1000:
		case 1010:
		case 1020:
			return "Река";

		}
		return "Земля";
	}

	public void draw(Graphics g) {
		if (init && draw) {
			g.setColor(Color.black);
			g.fillRoundRect((x), (y), 0.25f*DasGame.scrWi, (0.03f*punkts.size()+0.02f)*DasGame.scrHe, 10);
			g.setColor(Color.white);
			for (int a = 0; a < punkts.size(); a++) {
				g.drawString(punkts.get(a).name, x+0.02f*DasGame.scrWi, (float) (y+0.01f*DasGame.scrHe + (a*20)));
			}
			//System.out.println(numQ);
			if (numQ >= 0 && numQ <= punkts.size() - 1) {

				g.setColor(Color.blue);
				g.setLineWidth(3);
				g.drawRect(x+6, y + 6 + ((0.03f*numQ)*DasGame.scrHe), 0.125f*DasGame.scrWi, 0.03f*DasGame.scrHe);
			}
		}
	}

	/**
	 * 1 - info;
	 * 2 - pull;
	 * 3 - pererab;
	 * 4 - openinv
	 * 5 - add wood
	 * 6 - add bone
	 * 7 - take water
	 * 8 - собрать траву
	 * @return
	 */
	public int getAction() {
		if (numQ >= 0 && numQ < punkts.size()) {
			Punkt pnkt = punkts.get(numQ);
			return pnkt.doAction;
		}
		return -1;
	}

	public boolean highlite(float mX, float mY) {
		if (mX >= x - 32 && mX <= x + (0.1f*DasGame.scrWi) + 32 && 
				mY >= y - 32 && mY <= y + 32*punkts.size() + 32) {

			numQ = (int) ((mY - y) / 20);
			return true;
		} else {
			numQ = -1;
			return false;
		}
		//сделать подсвечивание зависящее от положения мышки, и прикинуть как взаимодействия наладить(сделать переменные которые будут блокировать атаку и будут запускать взаимодействие
	}

}
