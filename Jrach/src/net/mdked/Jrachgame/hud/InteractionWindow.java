package net.mdked.Jrachgame.hud;

import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.player.inv.Equipment;
import net.mdked.Jrachgame.player.inv.Inventory;
import net.mdked.Jrachgame.player.inv.Item;
import net.mdked.Jrachgame.world.Drop;
import net.mdked.Jrachgame.world.Entity;
import net.mdked.Jrachgame.world.Facility;
import net.mdked.Jrachgame.world.World;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class InteractionWindow {
	public Entity target;
	public Inventory one, two;
	//	private float xScale = DasGame.xScale;
	private String opIn;
	//	private String[]  w1, w2, oneInf, twoInf;
	//	private Inventory one, two;
	private int hglt1, hglt2, page1, page2;
	public boolean  higlt1, higlt2 ;
	public int type;
	private boolean hibut1, butlt;
	private boolean hibut2;
	int inl = 13;

	/**
	 * 1 - inventory exch
	 * 2 - craft
	 * 3 - equip
	 * 4 - stat
	 * @param type
	 */
	public InteractionWindow(int type, Entity target) {
		one = World.player.inventory;
		
		this.target = target;
		//		w1 = one.getWInfo();
		//		oneInf = one.getItemInfo();
		this.type = type;
		page1 = 0;
		page2 = 0;

		switch (type) {

		case 1:
			//			twoInv = true;
			Inventory tInv = null;
			switch (target.enttype) {
			case 2:
				//fac
				Facility base = (Facility) target;

				switch (base.facType) {
				case 1:
					opIn = "Палатка";
					break;
				case 2:
					opIn = "Лесопилка";
					break;
				case 3:
					opIn = "Maстерская";
					break;
				}
				tInv = base.inventory;
				break;
				
			case 10:
				//drop
				Drop dp = (Drop) target;
				opIn = "Предметы на земле";
				tInv = dp.inv;
				break;
			}
			

			two = tInv;
			//			twoInf = two.getItemInfo();
			//			w2 = two.getWInfo();
			break;

		case 2:
			//			twoInv = false;
			opIn = "Крафт";
			break;

		case 3:
			//			twoInv = false;
			opIn = "Одежда";
			break;

		case 4:
			//			twoInv = false;
			opIn = "Навыки";
			break;
		}
	}

	public Item getHGItem() {
		if (type == 2 && higlt1) {
			return one.getItem(hglt1+(page1*inl));
		}
		return null;
	}
	
	public void draw(Graphics g) {
		g.setColor(Color.black);
		g.fillRoundRect(0.1f*DasGame.scrWi, 0.1f*DasGame.scrHe, 0.8f*DasGame.scrWi, 0.75f*DasGame.scrHe, 3);
		g.setColor(Color.gray);
		g.setLineWidth(0.005f*DasGame.scrWi);
		g.drawLine(0.5f*DasGame.scrWi, 0.15f*DasGame.scrHe, 0.5f*DasGame.scrWi, 0.80f*DasGame.scrHe);
		g.setColor(Color.white);
		g.drawString("Игрок", 0.11f*DasGame.scrWi, 0.11f*DasGame.scrHe);

		g.drawString(opIn, 0.91f*DasGame.scrWi - opIn.length()*12, 0.145f*DasGame.scrHe);
		butlt= false;
		//		fac
		if (type == 1 || type == 3) {
			drawList(g, true);
			drawList(g, false);
		}

		//craft
		if (type == 2) {
			drawList(g, true);

			// крафт 
			//			g.drawString(opIn, 0.91f*DasGame.scrWi - opIn.length()*12, 0.145f*DasGame.scrHe);
			for (int a = 0; a < World.player.knownRecipes.size(); a++) {
				g.setColor(Color.white);
				g.drawString(World.player.knownRecipes.get(a).name, 0.521f*DasGame.scrWi, (0.208f + (a*0.042f))*DasGame.scrHe);
				//System.out.println(twoInf[a]);
			}
			if ( hglt2 < World.player.knownRecipes.size() && higlt2) {
				//Определение цвета дайош
				drawInfo(g, false);
			}
		}
		//		if (type == 3) {
		//			drawList(g, false);
		//		}
		// Otrisovka statov
		if (type == 4) {
			g.drawString("Сила - " + World.player.strength, 0.125f*DasGame.scrWi, 0.21f*DasGame.scrHe);
			g.drawString("Ловкость - " + World.player.agility, 0.125f*DasGame.scrWi, 0.25f*DasGame.scrHe);
			g.drawString("Выносливость - " + World.player.vitality, 0.125f*DasGame.scrWi, 0.29f*DasGame.scrHe);

			// Otrisovka barov hung, temp, stamina;
			g.setColor(Color.green);
			g.drawString("Жрач", 0.119f*DasGame.scrWi, 0.53f*DasGame.scrHe );
			float gBar = World.player.hung, oBar = 0;
			boolean drO = false;;
			if (World.player.hung >= 100) {
				gBar = 100;
				oBar = World.player.hung-100;
				drO = true;
			} 
			g.fillRect(0.125f*DasGame.scrWi, 0.52f*DasGame.scrHe, 0.032f*DasGame.scrWi, ((-0.167f*DasGame.scrHe)*(gBar/100)));
			if (drO) {
				g.setColor(Color.orange);
				g.fillRect(0.125f*DasGame.scrWi, 0.52f*DasGame.scrHe,  0.032f*DasGame.scrWi, ((-0.167f*DasGame.scrHe)*(oBar/100)));

			}
			//thirst
			g.setColor(Color.cyan);
			float cBar = World.player.thirst;
			g.fillRect(0.125f*DasGame.scrWi, 0.52f*DasGame.scrHe, 0.016f*DasGame.scrWi, ((-0.167f*DasGame.scrHe)*(cBar/100)));

			g.setColor(Color.red);
			g.drawString("Темп.", 0.165f*DasGame.scrWi, 0.529f*DasGame.scrHe);
			g.fillRect(0.171f*DasGame.scrWi, 0.52f*DasGame.scrHe, 0.032f*DasGame.scrWi, ((-0.167f*DasGame.scrHe)*(World.player.temp/100)));
			//
			//
			g.setColor(Color.blue);
			g.drawString("Силы", 0.213f*DasGame.scrWi, 0.529f*DasGame.scrHe);
			g.fillRect(0.219f*DasGame.scrWi, 0.52f*DasGame.scrHe, 0.032f*DasGame.scrWi, ((-0.167f*DasGame.scrHe)*(World.player.stamina/100)));

			g.setColor(Color.gray);

			g.drawRect(0.125f*DasGame.scrWi, 0.354f*DasGame.scrHe, 0.032f*DasGame.scrWi,  0.167f*DasGame.scrHe );
			g.drawRect(0.171f*DasGame.scrWi, 0.354f*DasGame.scrHe, 0.032f*DasGame.scrWi,  0.167f*DasGame.scrHe);
			g.drawRect(0.219f*DasGame.scrWi, 0.354f*DasGame.scrHe, 0.032f*DasGame.scrWi,  0.167f*DasGame.scrHe);

			g.setColor(Color.white);

			drawList(g, false);
			if ( hglt2 < World.player.knownSkills.size() && higlt2) {
				drawInfo(g, false);
			}
		}



		//g.drawString(str, x, y)
		//TODO сделать кнопачки для переключения страниц.


	}

	private void drawInfo(Graphics g, boolean left) {

		if (!left) {
			switch (type) {
			case 2:
				int dobv = 0;
				if (World.player.knownRecipes.get(hglt2).needDrag() >= 0 ) {
					dobv = 1;
				}

				if (World.player.knownRecipes.get(hglt2).checkGtvns()) {
					g.setColor(new Color(0,255,0,0.2f));
				} else
					g.setColor(new Color(255,0,0,0.2f));
				g.fillRoundRect(0.52f*DasGame.scrWi, (0.208f + (hglt2*0.042f))*DasGame.scrHe, 0.131f*DasGame.scrWi, 0.042f*DasGame.scrHe, 10);

				g.setColor(Color.white);
				g.fillRoundRect(0.52f*DasGame.scrWi, (0.245f + (hglt2*0.042f))*DasGame.scrHe, 0.2f*DasGame.scrWi, ((0.042f*World.player.knownRecipes.get(hglt2).getndItmsLngt())+0.042f*dobv) *DasGame.scrHe, 10);

				g.setColor(Color.black);

				if ( World.player.knownRecipes.get(hglt2).getndItmsLngt() != 0 )
					for (int a = 0; a < World.player.knownRecipes.get(hglt2).getndItmsLngt(); a++) {
						g.drawString(World.player.knownRecipes.get(hglt2).neededItm.get(a).setInfo() + " - " + World.player.knownRecipes.get(hglt2).neededItm.get(a).getQi(), 0.525f*DasGame.scrWi, ( 0.25f + (hglt2*0.042f)+ (a*0.042f))*DasGame.scrHe);
					}

				if (dobv == 1) {
					String str=null;
					switch (World.player.knownRecipes.get(hglt2).needDrag()) {
					case 0:
						str = "Срубленое дерево.";
						break;
					case 2:
						str = "Мертвый олень.";
						break;
					}
					g.drawString(str, 0.525f*DasGame.scrWi, (0.245f + (hglt2*0.042f) + (World.player.knownRecipes.get(hglt2).getndItmsLngt()*0.042f))*DasGame.scrHe);
				}
				break;

			case 3:
				//				g.setColor(Color.white);
				//				g.fillRoundRect(0.52f*DasGame.scrWi, (0.245f + (hglt2*0.042f))*DasGame.scrHe, 0.3f*DasGame.scrWi, 0.042f*DasGame.scrHe, 10);
				//
				//				g.setColor(Color.black);
				//
				//				if ( World.player.knownSkills.size() != 0 )
				//					for (int a = 0; a < World.player.knownRecipes.get(hglt2).getndItms().length; a++) {
				//						g.drawString(" Lvl " + World.player.knownSkills.get(hglt2).lvl + "; XP = " + (int) World.player.knownSkills.get(hglt2).xp + " / " + (int) World.player.knownSkills.get(hglt2).neededxp , 0.525f*DasGame.scrWi, ( 0.25f + (hglt2*0.042f)+ (a*0.042f))*DasGame.scrHe);
				//					}
				break;

			case 4:
				g.setColor(Color.white);
				g.fillRoundRect(0.52f*DasGame.scrWi, (0.245f + (hglt2*0.042f))*DasGame.scrHe, 0.3f*DasGame.scrWi, 0.042f*DasGame.scrHe, 10);

				g.setColor(Color.black);

				if ( World.player.knownSkills.size() != 0 )
					for (int a = 0; a < World.player.knownSkills.size(); a++) {
						g.drawString(" Lvl " + World.player.knownSkills.get(hglt2).lvl + "; XP = " + (int) World.player.knownSkills.get(hglt2).xp + " / " + (int) World.player.knownSkills.get(hglt2).neededxp , 0.525f*DasGame.scrWi, ( 0.25f + (hglt2*0.042f)+ (a*0.042f))*DasGame.scrHe);
					}
				break;
			}
		}
	}

	public void drawList(Graphics g, boolean whLeft) {


		if (whLeft) {
			if ( type == 1 || type == 2 || type == 3) {
				g.drawString("Вес: " + one.getWInfo()[0] + "/" + one.getWInfo()[1], 0.375f*DasGame.scrWi, 0.145f*DasGame.scrHe);
				if (one.inventory.size() > inl) {
					g.drawString(page1 + "/" + String.valueOf(one.inventory.size()/inl), 0.45f*DasGame.scrWi, 0.105f*DasGame.scrHe);
					drawBut(0, g);
				}
				int ls =  one.inventory.size()-(page1*inl);
				if (ls > inl) ls = inl;
				for (int a = 0; a < ls; a++) {
					//200
					if (one.getItAvlb(a+(page1*inl))) 
						g.setColor(Color.gray);
					//					} else 	
					//						de++;
					g.drawString(one.getItemInfo(a + (inl*page1)), 0.25f*DasGame.scrWi, (0.208f + (a*0.042f))*DasGame.scrHe);
					g.setColor(Color.white);	

				}
			}
			if (higlt1) {
				g.setColor(new Color(125,255,0,0.2f));
				g.fillRoundRect(0.24f*DasGame.scrWi, (0.204f + (hglt1*0.042f))*DasGame.scrHe, 0.131f*DasGame.scrWi, 0.042f*DasGame.scrHe, 10);
				g.setColor(Color.white);
			}
		}
		else {
			if ( type == 1 ) {
				g.drawString("Вес: " + two.getWInfo()[0] + "/" + two.getWInfo()[1], 0.52f*DasGame.scrWi, 0.145f*DasGame.scrHe);
				if (two.inventory.size() > inl) {
					g.drawString(page2 + "/" + String.valueOf(two.inventory.size()/inl), 0.57f*DasGame.scrWi, 0.105f*DasGame.scrHe);
					drawBut(1, g);
				}
				//two.inventory.size() - (12*page2)
				int ls =  two.inventory.size()-(page2*inl);
				if (ls > inl) ls = inl;

				//				System.out.println(" ls2 = " + ls);
				for (int a = 0; a < ls; a++) {
					g.drawString(two.getItemInfo(a+page2*inl), 0.52f*DasGame.scrWi, (0.208f + (a*0.042f))*DasGame.scrHe);
					//System.out.println(twoInf[a]);
				}
			}
			if (type == 3) {
				Equipment eq = World.player.equip;
				for (int a = 0; a < 5; a++) {
					g.drawString(eq.getSlotName(a), 0.52f*DasGame.scrWi, (0.208f + ((a*3)*0.042f))*DasGame.scrHe);
					g.drawString(" - " + eq.getWrdShmotName(a), 0.52f*DasGame.scrWi, (0.208f + (((a*3)+1)*0.042f))*DasGame.scrHe);
					g.drawString(" - Сопр - Холод: " + eq.getTemp(a) + " Броня: " + eq.getRes(a) , 0.52f*DasGame.scrWi, (0.208f + (((a*3)+2)*0.042f))*DasGame.scrHe);

				}
			}
			if (type == 4) {
				for (int a = 0; a < World.player.knownSkills.size(); a++) {
					g.drawString(World.player.knownSkills.get(a).name, 0.52f*DasGame.scrWi, (0.208f + (a*0.042f))*DasGame.scrHe);
					//System.out.println(twoInf[a]);
				}
			}



			if (higlt2) {
				g.setColor(new Color(125,255,0,0.2f));
				g.fillRoundRect(0.52f*DasGame.scrWi, (0.204f + (hglt2*0.042f))*DasGame.scrHe, 0.131f*DasGame.scrWi, 0.042f*DasGame.scrHe, 10);
				g.setColor(Color.white);
			}
		}

	}

	private void drawBut(int i, Graphics g) {
		butlt = true;
		float drX=0, drY;
		switch (i) {
		//0.375f*DasGame.scrWi, 0.15f*DasGame.scrHe
		//0.52f*DasGame.scrWi, 0.15f*DasGame.scrHe
		case 0:
			drX = 0.4f;
			break;
		case 1:
			drX = 0.52f;
			break;
		}

		drX *= DasGame.scrWi;
		drY = 0.105f * DasGame.scrHe;

		g.drawString(">>>", drX, drY);
		if (hibut1 || hibut2) {
			g.setColor(new Color(0,1,0,0.6f));
			g.fillRect(drX, drY, 20	, 20);
			g.setColor(Color.white);
		}
	}

	public void highlite(float mX, float mY) {
		if (butlt)
			if (mY >= 0.10f*DasGame.scrHe && mY <= 0.15f*DasGame.scrHe) {
				if (mX  >= 0.375f*DasGame.scrWi && mX <= 0.45f*DasGame.scrWi) hibut1 = true;
				else hibut1 = false;
				if (mX  >= 0.51f*DasGame.scrWi && mX <= 0.55f*DasGame.scrWi) hibut2 = true;
				else hibut2 = false;
			} else {
				hibut1 = false;
				hibut2 = false;
			}
		//one
		if (
				mX >=  0.24f*DasGame.scrWi && mX <=  0.49f*DasGame.scrWi &&
				mY >= 0.125f*DasGame.scrHe && mY <= 0.99f*DasGame.scrHe	
				) 
		{
			hglt1 = (int) ((mY - 0.208f*DasGame.scrHe) / (0.04f*DasGame.scrHe));

			if ( hglt1 >= 0 && hglt1 < 13 ) {
				higlt1 = true;
			} else higlt1 = false;
		} else higlt1 = false;

		//two else higlt2 = false;
		if (	mX >= 0.52f*DasGame.scrWi && mX <= 0.781f*DasGame.scrWi &&
				mY >= 0.125f*DasGame.scrHe && mY <= 0.99f*DasGame.scrHe ) 
		{
			hglt2 = (int) ((mY - (0.208f*DasGame.scrHe)) / (0.042f*DasGame.scrHe));
			if ( hglt2 >= 0 && hglt2 < 13 ) {
				higlt2 = true;
			} else higlt2 = false;
		} else higlt2 = false;
		//		else if (!twoInv && World.player.knownRecipes.size() > 0 &&
		//				mX >= 0.52f*DasGame.scrWi && mX <= 0.781f*DasGame.scrWi &&
		//				mY >= 0.125f*DasGame.scrHe && mY <= 0.99f*DasGame.scrHe
		//				) 
		//		{
		//			hglt2 = (int) ((mY - (0.208f*DasGame.scrHe)) / (0.04f*DasGame.scrHe));
		//			//			System.out.println("hglt2 = " + hglt2);
		//
		//			if ( hglt2 >= 0 && hglt2 <= 12 && hglt2 < World.player.knownRecipes.size()) {
		//				higlt2 = true;
		//			} else higlt2 = false;
		//		} else higlt2 = false;

	}

	public void action() {
		//передача шмота
		if (higlt1) {
			if (one.inventory.size() > 0) {
				int get = hglt1+(page1*inl);
				//
				//				while (one.getItInv(get)) 
				//					get++;

				switch (type) {
				case 1:

					if (two.addItem(one.getItem(get), 1)) 
						one.removeItem(get,1);

					break;

				case 2:
				case 3:
				case 300:
					World.player.useItem(get);
					
					break;

				}
			}
		}
		if (higlt2) {
			int get = hglt2+(page2*inl);

			switch (type) {

			case 1:
				if ( two.inventory.size() > 0) {
					//					Item transf = new Item(two.getItmType(hglt2), 1);
					System.out.println( " transf ");
					
					if (one.addItem(two.getItem(get),1)) 
						two.removeItem(get,1);

				}
				break;

			case 2:
				//TODO CRAFT
				if (hglt2 < World.player.knownRecipes.size() && World.player.knownRecipes.get(get).checkGtvns()) {
					switch (World.player.knownRecipes.get(get).getType()) {
					case 1:
						//zdanie
						World.player.recipeBuff = World.player.knownRecipes.get(get);
						DasGame.beginConstr(World.player.recipeBuff.getOutType());
						break;
					case 2:
						//item
						Item craftedI = new Item(World.player.knownRecipes.get(get).getOutType(),1);
						craftedI.setQuality(World.player.useSkill(craftedI.getCraftSkill()));
						World.player.inventory.addItem(craftedI ,1);
						
						World.player.knownRecipes.get(get).remInv();
						break;
					}
				}
				break;

			case 3:
				//				System.out.println("removing shmot " + hglt2/3);
				World.player.equip.remove(hglt2/3);
				break;
			}
		}
		if (hibut1) 
			page1++;

		if (hibut2) 
			page2++;
		chkpgs();
		//		refresh();
	}

	private void chkpgs() {
		if (type == 1 || type == 2 || type == 3 ) {
			if (page1*inl == one.inventory.size())
				page1--;
			if (page1 > one.inventory.size()/inl || page1 < 0)
				page1 = 0;
		}
		if ( type == 1) {
			if (page2*inl == two.inventory.size())
				page2--;
			if (page2 > two.inventory.size()/inl || page2 < 0)
				page2 = 0;
		}
	}

	public void removedrop() {
		if (type == 1 && target.enttype == 10)
		{
			Drop dp = (Drop) target;
			if (dp.inv.getWeight() == 0	)
				World.removeEntityFromWorld(dp);
		}
		target = null;
	}
}

