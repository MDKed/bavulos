package net.mdked.Jrachgame.player;

import java.io.Serializable;

import net.mdked.Jrachgame.world.Charakter;
import net.mdked.Jrachgame.world.World;

/**
 * 
 *  family = 1 - температуры
	 * 2- еда 
	 * 3- кровотеки
	 * 4- стамина
	 * 5- кислород
	 * 6 - удушье
	 * 7 - плавание
	 * 8 - отравление
	 * 9 - healing
 * @author operator-7
 *
 */
public class Buff implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6272678162032642038L;
	public boolean chStr, chAgi, chVit,
	dotStam, dotHp, dotHung,
	chSpeed, chTempRes, chHp, chStam, dotOxy,
	 isTimed, bleed;
	public float dotHP, dotHNG, dotSTAM,
	chSTR, chAGI, chVIT, chSTAM,
	chSPEED, chTR, time = 0, chHP, dotOXY;
	public boolean active;
	public int  icon, family= -1, force;
	public Charakter nositel;

	/*
	 
	 */
	/**
	 *  
	 * @param type
	 * 0 - lght holod, 1 - hard cold
	 * 2 - hunger
	 * 3 - lght bleed;
	 * 4 - тяж кровтч
	 * 5 - запыхался
	 * 6 - пережор
	 * 7- нет воздуха
	 * 8 - задыхается
	 * 9- плавание
	 * 10 - жажда
	 * 11 - лечение
	 * @param time - v sekundax
	 */
	public Buff(int type, int time, Charakter nosa) {
		nositel = nosa;
		icon = type;
		switch (type) {
		case 0:
			//light xolod
			chSpeed = true;
			chSPEED = -0.3f;
			family = 1;
			force = 0;
			break;
		case 1:
			//hard cold
			chSpeed = true;
			chSPEED = -0.5f;
			dotHp = true;
			dotHP = -2.5f;
			dotStam = true;
			dotSTAM = -0.5f;
			family = 1;
			force = 1;
			break;
		case 2:
			//hunger
			dotHp = true;
			dotHP = -0.5f;
			chSpeed = true;
			chSPEED = -0.2f;
			family = 2;
			force = 1;
			break;
		case 3:
			//light bleed
			dotHp = true;
			dotHP = -0.4f;
			family = 3;
			this.time = time*1000;
			isTimed = true;
			force = 0;
			bleed = true;
			break;
		case 4:
			//hard bleed
			isTimed = true;
			dotHp = true;
			dotHP = -2f;
			family = 3;
			this.time = time*1000;
			force = 1;
			bleed = true;
			break;
		case 5:
			//winded
			chSpeed = true;
			chSPEED = -0.1f;
			family = 4;
			force = 0;
			break;
		case 6:
			//pereel
			chSpeed = true;
			chSPEED = -0.2f;
			chStam = true;
			chSTAM = -20;
			family = 2;
			force = 0;
			break;
			
		case 7:
			//нет воздуха
			dotOxy = true;
			dotOXY = -2.85f;
			family = 5;
			force = 0;
			break;
			
		case 8:
			//удушье
			family = 6;
			force = 0;
			
			chSpeed = true;
			chSPEED = 0.1f;
			
			dotStam = true;
			dotSTAM = -5f;
			
			dotHp = true;
			dotHP = -5f;
			break;
		case 9:
			// в воде
			Skill swiming = nositel.getSkill(0);
			family = 7;
			force = 0;
			
			chSpeed = true;
			chSPEED = (float) -(nositel.speedMax * ( 0.5f - ( 0.05f * swiming.lvl)));
			
			dotStam = true;
			dotSTAM = -10f - ( 1f * swiming.lvl);
			break;
		case 10:
			//жажда
			dotHp = true;
			dotHP = -0.5f;
			chSpeed = true;
			chSPEED = -0.2f;
			family = 4;
			force = 1;
			break;
		case 11:
			// healing
			dotHp = true;
			dotHP = 5f;
			family = 9;
			isTimed = true;
			this.time = time*1000;
			break;

		}
	}
}
