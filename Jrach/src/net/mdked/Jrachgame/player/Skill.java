package net.mdked.Jrachgame.player;

import java.io.Serializable;

public class Skill implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8794450135183031935L;
	public String name;
	public int lvl, type;
	public float xp, neededxp, qXifier;
	
	
	
	public Skill(int type, int lvl) {
		this.lvl = lvl;
		xp = 0;
		nedxp();
		checkXfier();
		this.type = type;
		setName();
	}
	
	
	private void setName() {
		switch (type) {
		case 0:
			//swiminng
			name = "Плавание";
			break;
		case 1:
			name = "Травничество";
			break;
		}
	}
	public void kachXP(int delta,int xps) {
		xp += (xps/1000f) * delta;
		checklvlup();
	}
	private void nedxp() {
		neededxp = (lvl == 0) ? 300 : lvl*600;
	}
	
	private void checklvlup() {
	
		if (xp >= neededxp && lvl < 12) {
			lvl++;
			xp -= neededxp;
			nedxp();
			checkXfier();
//			System.out.println(" Skill " + name + " level up! lvl = " + lvl);
		}
	
	}
	private void checkXfier() {
		qXifier = ((1f / 6f) * lvl) + 0.1f;
	}
	
	

}
