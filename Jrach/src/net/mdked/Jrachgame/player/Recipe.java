package net.mdked.Jrachgame.player;

import java.io.Serializable;
import java.util.ArrayList;

import net.mdked.Jrachgame.player.inv.Item;
import net.mdked.Jrachgame.world.World;

public class Recipe implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2385293948781408895L;
	public String name;
	private byte type;//1-dom; 2-shmot
	//первое число - тип шмота, второе- кол-во
	public ArrayList<Item> neededItm;
	private byte neededPul = -1;
	private int outputType, num;
	private byte plAction;
	private boolean needFire, needWorkshop;
	public boolean snap;

	public Recipe(int numer) {
		neededItm = new ArrayList<Item>();
		num = numer;
		switch (numer) {
		case 1:
			neededPul = 0;
			name = "Мастерская";
			type = 1;
			outputType = 2;
			plAction = 5;
			break;
		case 2:
			//koster
//			neededItm = new byte[][]{{2,4}};
			neededItm.add(new Item(2,4));
			name = "Костер";
			type = 1;
			outputType = 6;
			plAction = 4;
			break;
		case 3:
			//jareno myaso
//			neededItm = new byte[][] {{0,1}};
			neededItm.add(new Item(0,1));
			name = "Приготовленное мясо";
			type = 2;
			outputType = 5;
			plAction = 3;
			needFire = true;
			break;
		case 4:
			name = "Палатка";
//			neededItm = null;
			neededPul = 2;
			type = 1;
			outputType = 1;
			plAction = 5;
			break;
		case 5:
			name = "Целебный порошок";
			neededItm.add(new Item(400, 2));
			type = 2;
			outputType = 300;
			plAction = 1;
			needWorkshop = true;
			break;
		case 6:
			name = "Стена";
//			neededItm.add(new Item(400, 2));
			type = 1;
			outputType = 11;
			plAction = 1;
			snap = true;
//			needWorkshop = true;
			break;
		}
	}

	public boolean checkGtvns() {
		boolean pullgot, weightGot = false;

		if (type == 2 && World.player.inventory.checAddItm(new Item(outputType,1))) weightGot = true;
		if (type == 1) weightGot = true;
		if (needFire && !World.player.nearFire) return false;
		if (needWorkshop && !World.player.nearWshpA) return false;
		if (neededPul < 0) pullgot = true; else pullgot = false;
		
		if (neededItm != null && neededItm.size() > 0) {
//			int howneed = neededItm.length;
//			int howmuch =0;
			for (int a = 0; a < neededItm.size(); a++ ) {
//				for (int b = 0; b < World.player.inventory.inventory.size(); b++) {
//					if (World.player.inventory.inventory.get(b).getType() == neededItm[a][0] &&
//							World.player.inventory.inventory.get(b).getQi() >= neededItm[a][1]	) {
//						howmuch++;
//						break;
//					}
//				}
				if (!World.player.inventory.isIteminKomplInv(neededItm.get(a)))
					return false;
			}
//			if (howmuch == howneed) invgot = true;
		} 
//		else 
//			invgot = true;
		
		if (neededPul >= 0 && World.player.pullin && World.player.pull.dtype == neededPul) pullgot = true;
//invgot &&
		if ( pullgot && weightGot) return true;
		else
			return false;
	}
	
	public void remInv() {
		

		for (int a = 0; a < neededItm.size(); a++ ) {
			System.out.println(" otnimaem shmot");

			World.player.inventory.removeTypeItem(neededItm.get(a), neededItm.get(a).getQi());
//			for (int b = 0; b < World.player.inventory.inventory.size(); b++) {
//				if (World.player.inventory.inventory.get(b).getType() == neededItm[a][0] &&
//						World.player.inventory.inventory.get(b).getQi() >= neededItm[a][1]	) {
//					World.player.inventory.removeItem(b, neededItm[a][1]);
//					break;
//				}
//			}
		}
		if (neededPul >= 0) {
			World.removeEntityFromWorld(World.player.pull);
			World.player.stopPullin();
		}
	}
//	public byte[][] getndItms() {
//		return neededItm;
//	}
	public int getndItmsLngt() {
//		if ( neededItm != null )
			return neededItm.size();
//		else
//			return 0;
	}
	public byte needDrag() {
		return neededPul;
	}
	public byte getType() {
		return type;
	}
	public int getOutType() {
		return outputType;
	}
	public byte getPlAct() {
		return plAction;
	}
	public int getNumer() {
		return num;
	}
}
