package net.mdked.Jrachgame.player.inv;

import java.io.Serializable;

public class Equipment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3616039177718809752L;
	
	public static final int HEAD = 0, TORSO = 1, HANDS = 2, LEGS = 3, BOOTS = 4, WEAPON = 5;

	public Slot head, torso, hands, legs, boots, weapon;
	//	public int hdtemp, hdblr, hdrr,

	public Equipment() {
		head = new Slot(0);
		torso = new Slot(1);
		hands = new Slot(2);
		legs = new Slot(3);
		boots = new Slot(4);
		weapon = new Slot(5);
	}
	
	public int getArmor() {
		int ret = 0;
		for (int a = 0; a < 6; a++) {
			ret += getRes(a);
		}
		return ret;
	}

	public void wear( Item shmotka) {
		getSlot(shmotka).wear(shmotka);
	}

	public Slot getSlot(Item itm) {
		int mesto = -1;
		switch (itm.getType()) {
		case 6:
			Shmot shm = (Shmot) itm;
			mesto = shm.mesto;
			break;
		case 7:
			mesto = 5;
			break;
		}
		
		switch (mesto) {
		case 0:
			return head;
		case 1:
			return	torso;
		case 2:
			return hands;
		case 3:
			return	legs;
		case 4:
			return	boots;
		case 5:
			return weapon;
		}
		return null;
	}
	public Slot getSlot(int mesto) {
		switch (mesto) {
		case 0:
			return head;
		case 1:
			return	torso;
		case 2:
			return hands;
		case 3:
			return	legs;
		case 4:
			return	boots;
		case 5:
			return weapon;
		}
		return null;
	}

	public Item getWrdShmot(int mesto) {
		return  getSlot(mesto).slot;
	}

	public String getSlotName(int mesto) {
		switch (mesto) {
		case 0:
			return "Голова";
		case 1:
			return "Тело";
		case 2:
			return "Руки";
		case 3:
			return "Ноги";
		case 4:
			return "Обувь";
		case 5:
			return "Оружие";
		}
		return "хз";
	}
	public String getWrdShmotName(int mesto) {
		if (getWrdShmot(mesto) != null) 
			return getWrdShmot(mesto).name;
		return "Пусто.";
	}
	public int getTemp(int mesto) {
		if (getWrdShmot(mesto) != null && getWrdShmot(mesto).getType() == 6) {
			Shmot shm = (Shmot) getWrdShmot(mesto);
			return shm.tempres;
		}
			
		return 0;
	}
	
	public int getRes(int mesto) {
		if (getWrdShmot(mesto) != null && getWrdShmot(mesto).getType() == 6) {
			Shmot shm = (Shmot) getWrdShmot(mesto);
			return shm.armr.armr;
		}
		return 0;
	}

//	public int getBlR(int mesto) {
//		if (getWrdShmot(mesto) != null && getWrdShmot(mesto).getType() == 6) {
//			Shmot shm = (Shmot) getWrdShmot(mesto);
//			return shm.armr.blRes;
//		}
//		return 0;
//	}
//	public int getReR(int mesto) {
//		if (getWrdShmot(mesto) != null && getWrdShmot(mesto).getType() == 6) {
//			Shmot shm = (Shmot) getWrdShmot(mesto);
//			return shm.armr.rezhRes;
//		}
//		return 0;
//	}

	public void remove(int where) {
		getSlot(where).remove();
	}
	public void remove(Item itm) {
		getSlot(itm).remove();
	}
}
