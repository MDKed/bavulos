package net.mdked.Jrachgame.player.inv;

public class ItmContainer extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3992369386481422721L;
	/**
	 * 1 - liquid
	 */
	public int contains;
	public int volume;
	public Item itm;

	public ItmContainer(int type, int quant) {
		super(type, quant);

		switch (type) {
		case 200:
			//пластиковая бутылка
			contains = 1;
			volume = 2;

			break;
		}
	}

	public void fillFullWith(int fill) {
		int filla = -1;
		switch (fill) {
		case 900:
		case 910:
		case 920:

		case 1000:
		case 1010:
		case 1020:
			filla = 102;
		}
		fillWith(filla, volume);
	}

	public void fillWith(int fill, int q) {
		if (itm != null) return; 

		switch (contains) {

		case 1:
			if ( fill < 100 || fill >= 200) return;

			break;
		}
		itm = new Item(fill, q);
		itm.emkst = this;

		nameChek();
	}

	public void nameChek() {
		 setInfo();
		if (itm != null) {
			System.out.println(itm.getQi());
			int perc = (int) ((float) itm.getQi()/volume * 100f);
			name += " " + Integer.toString(perc) + "% (" + itm.name + ")";
		}
		
	}

}
