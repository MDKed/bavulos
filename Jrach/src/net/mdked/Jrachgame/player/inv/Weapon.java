package net.mdked.Jrachgame.player.inv;

import net.mdked.Jrachgame.world.Charakter;

public class Weapon extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7901648456248674835L;
	public int wtype, mat, toolType, drawId, damage, pen;
	public boolean balanced, tool, metal;
	public float qual, sost;
	public Charakter mastr;
	
//	blDamage, rezDamage, penDamage,
//	blRat, rezRat, penRat,
//	meDamX, trDamX, raDamX;
	

	public Weapon(int vid, int mat, float quality) {
		super(7, 1);
		nstakbl = true;
				/*
		 * разделить ли между типами, ближнее, дальнее метательное или придумать какую-то 
		 * хитрожопую систему, типа при использовании происходит определенное действие и уже
		 * исходя из типа объекта, типа мили-юзаеш и происходит мили атака с вытекающими последствиями
		 * рангед - выстрел
		 * метательное - бросок
		 * метательное можно объеденить с рангедом, на выходе же мисайл вылетает, просто у метательного
		 * будет отниматься шмот из инвентаря, а у рангеда из обоймы
		 * плюс всем можно будет кидаться и бить в мили
		 * 
		 */
		qual = quality;
		this.mat = mat;
		
		makeWeapon(vid);
		
		materialize();
		
	}


	public void setMaster(Charakter chaa) {
		mastr = chaa;
	}
	
	private void materialize() {
		float mod = 1;
		switch (mat) {
		case 1:
		case 4:
		case 6:
			mod = (metal) ? 0.5f : 0.75f;
			break;
			
		case 2:
		case 5:
		case 7:
			mod = 1f;
			break;
			
		case 3:
		case 8:
			mod = 1.25f;
			break;
		}
		
		damage *=mod;
		
//		blDamage *= mod;
//		rezDamage *= mod;
//		penDamage *= mod;
//		
//		blRat *= mod;
//		rezRat *= mod;
//		penRat *= mod;
		pen *= mod;
		
		sost *= mod;
		
//		meDamX = mod;
//		raDamX = mod;
//		trDamX = mod;
	}


	private void makeWeapon(int vid) {
		drawId = vid;
		switch (vid) {
		case -10:
			//roga olenya
			makeMelee(-10);
			break;
		case 0:
			//topor
			makeMelee(0);
			break;
		case 100:
			//trowspear
			makeTrowWeapon(0);
			break;
		}
		
		}


	private void makeTrowWeapon(int i) {
		wtype = 2;
		switch (i) {
		case 0:
			setWeight(30);
			
			damage = 45;
			pen = 15;
			
//			setBlDamage(1);
//			setBlRat(1);
//			
//			setRezDamage(5);
//			setRezRat(2);
//			
//			setPenDamage(45);
//			setPenRat(15);
			
			setSost(50);
			
			metal = true;
			balanced = true;
			
			name = getMatName(mat) + " Мет. Копье";

			
			break;
		}
	}


	private void setSost(int i) {
		sost = i*qual;		
	}


	private void makeMelee(int i) {
		wtype = 1;
		switch (i) {
		case -10:
			setWeight(100);
			
			damage = 35;
			pen = 10;
				
			setSost(1000);
			
			metal = false;
			
			tool = false;
			toolType = 1;
			
			name = getMatName(mat) + " Рога";
			break;
		case 0:
			
			setWeight(100);
			
			damage = 35;
			pen = 10;
			
//			setBlDamage(5);
//			setBlRat(1);
//			
//			setRezDamage(35);
//			setRezRat(10);
//			
//			setPenDamage(1);
//			setPenRat(1);
			
			setSost(100);
			
			metal = true;
			
			tool = true;
			toolType = 1;
			
			name = getMatName(mat) + " Топор";

			break;
		}
	}


//	private void setPenRat(int i) {
//		penRat = i*qual;		
//	}
//
//
//	private void setRezRat(int i) {
//		rezRat = i*qual;		
//	}
//
//
//	private void setBlRat(int i) {
//		blRat = i*qual;		
//	}
//
//
//	private void setPenDamage(int i) {
//		penDamage = i*qual;		
//	}
//
//
//	private void setRezDamage(int i) {
//		rezDamage = i*qual;		
//	}
//
//
//	private void setBlDamage(int i) {
//		blDamage = i*qual;
//	}

}
