package net.mdked.Jrachgame.player.inv;

import java.io.Serializable;
import java.util.ArrayList;

public class Inventory implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7222219798648181180L;
	private int maxWeight, weight;
	public ArrayList<Item> inventory;

	public Inventory(int maxWeight) {
		this.maxWeight = maxWeight*100;
		inventory = new ArrayList<Item>();
	}

	public boolean checAddItm(Item itm) {
		if (itm.getWeight() + weight <= maxWeight) 
			return true;
		else return false;
	}
	public boolean checAddItm(Item itm, int qnt) {
		if ((itm.getWeight1()*qnt) + weight <= maxWeight) 
			return true;
		else return false;
	}
	public boolean addItem(Item itm) {
		return addItem(itm, itm.getQi());
	}

	public boolean addItem(Item itm, int qnty) {
		//		System.out.println(" add " + itm + " q= " + qnty);
		if (itm == null) return false;
		if (itm.nAvailable) return false;

		if (checAddItm(itm, qnty)) {

			boolean stack = false;

			if (!itm.nstakbl)
				for (int a = 0; a < inventory.size(); a++) {
					if (inventory.get(a).getType() == itm.getType()) {
						inventory.get(a).dobavit(qnty);
						//itm.getQi()
						//						weight += itm.getWeight1()*qnty;
						stack = true;
						break;
					}
				} 

			if (!stack) {
				if (itm.getQi() == 1) {
					inventory.add(itm);
				} else {

					//					if (itm.getType() == 6) {
					//						Shmot shmt = (Shmot) itm, sma = new Shmot(shmt.getType(), shmt.type, 0, 0);
					//						inventory.add(sma);
					//					}
					//					else {
					Item itmb = new Item(itm.getType(), qnty);
					inventory.add(itmb);
					//					}

				}
				//				weight += itm.getWeight1()*qnty;
			}
			checkWeight();
			return true;
		} else return false;
	}

	private void checkWeight() {
		weight = 0;
		for (int a = 0;a < inventory.size(); a++) {
			Item itm = inventory.get(a);
			weight += itm.getWeight();
			if (itm.getType() >= 200 && itm.getType() < 300) {
				ItmContainer cont = (ItmContainer) itm;
				if (cont.itm != null)
					weight += cont.itm.getWeight();
			}

		}
	}

	public boolean removeItem(int num, int kolvo) {
		System.out.println("remove num ");
		if (num < inventory.size()) {
			//System.out.println(" ves do = " + weight);

			//			if (!
			//			inventory.get(num).otnyat(kolvo);
			//					) {
			//				if (inventory.get(num).getQi() == kolvo) {
			//					weight -= inventory.get(num).getWeight1()*kolvo;
			//					inventory.remove(num);
			//					//System.out.println(" ves posle удаления = " + weight);
			//					return true;
			//				} 
			//			} else
			//				weight -= inventory.get(num).getWeight1()*kolvo;

			//System.out.println(" ves posle = " + weight);
			//			if (inventory.get(num).getQi() == 0) 
			//				inventory.remove(num);
			removeItem(inventory.get(num), kolvo);
			return true;

		} else return false;
	}

	public boolean isIteminInv(Item itm) {
		return inventory.contains(itm);
	}

	public boolean isIteminKomplInv(Item itm) {
		for (int a = 0; a < inventory.size(); a ++) 
			if (inventory.get(a).getType() == itm.getType() && inventory.get(a).getQi() >= itm.getQi())
				return true;

		return false;
	}

	public void removeTypeItem(Item itm, int kolvo) {
		Item sotype = null;
		for (int a = 0; a < inventory.size(); a ++) 
			if (inventory.get(a).getType() == itm.getType()) {
				//				System.out.println("  found sotype shmot");

				sotype = inventory.get(a);
				break;
			}
		//		System.out.println("   sotype - " + sotype + " " + sotype.otnyat(kolvo));

		if (sotype != null && !sotype.otnyat(kolvo) && sotype.getQi() == kolvo ) {
			//			System.out.println("  remove sotype");

			inventory.remove(sotype);

		}

		checkWeight();
	}

	public void removeItemFrmCont(Item itm, int kolvo) {
		if (isIteminInv(itm.emkst)) {
			if (!itm.otnyat(kolvo) && itm.getQi() <= kolvo ) 
				itm.emkst.itm = null;
			itm.emkst.nameChek();
		}
		checkWeight();
	}

	public void removeItem(Item itm, int kolvo) {
		if (itm.emkst != null) 
			removeItemFrmCont(itm, kolvo);

		if (isIteminInv(itm)) {

			if (!itm.otnyat(kolvo) && itm.getQi() <= kolvo ) 
				inventory.remove(itm);
			checkWeight();
		}
	}

	public String[] getItemInfo() {
		if (inventory.size() > 0) {
			String[] itin = new String[inventory.size()];
			for (int a = 0; a < inventory.size(); a++) {
				itin[a] = inventory.get(a).name + " q= " + inventory.get(a).getQ();
				//System.out.println(itin[a]);
			}
			return itin;
		}
		else return null;
	}
	public String getItemInfo(int num) {
		if (num < inventory.size())
			return inventory.get(num).name + " q= " + inventory.get(num).getQ();
		return null;
	}

	public int haveItem(int itmType, int num) {
		for (int a = 0; a < inventory.size(); a++ ) {
			Item itm = inventory.get(a);
			if (itm.getType() == itmType && itm.getQi() >= num) return a;
		}
		return -1;
	}

	public ItmContainer getEmptWatCont() {
		for (int a = 0; a < inventory.size(); a++ ) {
			Item itm = inventory.get(a);
			if (itm.getType() >= 200 && itm.getType() < 300 ) {
				ItmContainer cont = (ItmContainer) itm;
				if (cont.itm == null) return cont;
			}
		}
		return null;
	}

	public boolean haveFuel(byte fuelType) {
		for (int a = 0; a < inventory.size(); a++ ) {
			Item itm = inventory.get(a);
			if (itm.getFuelT() == fuelType ) return true;
		}
		return false;

	}

	public int getItmType(int index) {
		return inventory.get(index).getType();
	}
	public Item getItem(int index) {
		if (index >= inventory.size()) return null;
		return inventory.get(index);
	}
	public boolean getItAvlb(int index) {
		Item ck = getItem(index);
		if (ck != null) 
			return ck.nAvailable;
		return true;
	}
	//Float.toString(weight) , Float.toString(maxWeight)
	// Float.toString(weight).substring(0, Float.toString(weight).indexOf(".")+2) , Float.toString(maxWeight).substring(0, Float.toString(maxWeight).indexOf(".")+2)
	public String[] getWInfo() {
		String[] otv = new String[] {Float.toString((float) weight/100 ), Float.toString((float) maxWeight/100)};
		return otv;
	}
	
	public int getWeight() {
		return weight;
	}
}
