package net.mdked.Jrachgame.player.inv;

import java.io.Serializable;

/**
 * case 0:
 *			return "Мясо";
 *		case 1:
 *			return "Кость";
 *		case 2:
 *			return "Дрова";
 *		case 3:
 *			return "Камень";
 *		case 4:
 *			return "Кожа";
 *		case 5:
 *			return "Поджареное мясо";
 *			
 *			6- шмот
 *		7 - weapon
 * 100 - жидкости
 * 300 - лекарства
 * @author operator-7
 *
 */
public class Item implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3489910975727236642L;

	private int type, quantity, craftSkill;
	private int weight,weight1;
	private float itemQ = 1;
	//quality
	private byte fuelType = -1;
	//fuel q: -1=notFuel 0 = Lq burning 1 = normQ burning 2 = hiQ burning
	private byte useAction;
	public String name;
	public boolean nAvailable, nstakbl;
	public ItmContainer emkst;
	// 0 - eat, 1 - wear, 2 - drink, 3 - use contents, 4 - healing;
	/**
	 * 100 - еда
	 * 200 - емкости
	 * 300 - лекарства
	 * 400 - ингридиенты
	 * @param type
	 * @param quant
	 */
	public Item(int type, int quant) {
		this.type = type;
		quantity = quant;
		switch (type) {
		case 0:
			weight1 = 50;
			itemQ = 0;
			useAction = 0;
			break;
		case 1:
			weight1 =  10;
			fuelType = 0;
			itemQ = 0;
			useAction = -1;
			break;
		case 2:
			weight1 =  50;
			fuelType = 0;
			itemQ = 1;
			useAction= -1;
			break;
		case 3:
			weight1 =  100;
			useAction = -1;
			break;
		case 4:
			weight1 =  50;
			useAction = -1;
			break;
		case 5:
			weight1 = 30;
			itemQ = 2;
			useAction = 0;
			break;
		case 6:
		case 7:
			//weapon
			//shmot
			useAction = 1;
			break;

		case 100:
			//voda gryazn
			weight1 = 25;
			itemQ = 0;
			useAction = -1;
			break;

		case 101:
			//voda solenaya
			weight1 = 25;
			itemQ = 0;
			useAction = -1;
			break;

		case 102:
			//voda piutevaya
			weight1 = 25;
			itemQ = 2;
			useAction = 2;
			break;

		case 200:
			//пластиковая бутылка
			weight1 = 10;
			itemQ = 0;
			useAction = 3;
			break;

		case 300:
			//poroshok zelebnii
			weight1 = 10;
			useAction = 4;
			craftSkill = 1;
			break;

		case 400:
			//zelebnie travi
			weight1 = 5;
			useAction = 4;
			break;
		}

		weight = weight1 * quantity;

		setInfo();
	}
	public void setWeight(int wt) {
		weight1 = wt;
		weight = wt*quantity;
	}
	public float getWeight() {
		return weight;
	}
	public float getWeight1() {
		return weight1;
	}


	public void setQuality(float quality) {
		itemQ = quality;
	}

	public String setInfo() {
		switch (type) {
		case 0:
			name = "Мясо";
			break;
		case 1:
			name = "Кость";
			break;
		case 2:
			name = "Дрова";
			break;
		case 3:
			name = "Камень";
			break;
		case 4:
			name = "Кожа";
			break;
		case 5:
			name = "Поджареное мясо";
			break;

		case 100:
			name = "Сырая вода";
			break;
		case 101:
			name = "Соленая вода";
			break;
		case 102:
			name = "Питевая вода";
			break;

		case 200:
			name = "Пласт. Бутылка 1л";
			break;

		case 300:
			name = "Целебный порошок";
			break;
			
		case 400:
			name = "Целебные травы";
			break;

		}
		return name;
	}

	/**
	 * Ткань, кожа, кевлар
	 * 1 2 3 
	 * древесина камень медь железо сталь
	 * 4 5 6 7 8
	 */
	public static String getMatName(int mat) {
		switch (mat) {
		case 1:
			return "Тканевой";
		case 2:
			return "Кожанный";
		case 3:
			return "Кевларовый";
		case 4:
			return "Деревянный";
		case 5:
			return "Каменный";
		case 6:
			return "Медный";
		case 7:
			return "Железный";
		case 8:
			return "Стальной";
		}
		return "Эфирный";
	}

	public String getQ() {
		return Integer.toString(quantity);
	}
	/**
	 * kolvo
	 * @return
	 */
	public int getQi() {
		return quantity;
	}
	public void dobavit(int skolko) {
		quantity += skolko;
		weight += skolko*weight1;
	}

	public boolean otnyat(int kolvo) {
		if (quantity > 0  && kolvo < quantity) {
			quantity -= kolvo;
			weight -= weight1*kolvo;
			return true;
		} else return false;
	}
	public int getType() {
		return type;
	}
	public byte getFuelT() {
		return fuelType;
	}
	public float getItmQ() {
		return itemQ;
	}
	public byte getUsAct() {
		return useAction;
	}
	
	public int getCraftSkill() {
		return craftSkill;
	}
}
