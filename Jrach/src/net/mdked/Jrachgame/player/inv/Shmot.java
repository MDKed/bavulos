package net.mdked.Jrachgame.player.inv;

import net.mdked.Jrachgame.world.Armor;

import org.newdawn.slick.Color;
/**
 * type - chast
 * subtype - variant
 * @author operator-7
 *
 */
public class Shmot extends Item {
	//TODO заделать как в дф, тело-вид-материал-качество


	/**
	 * 
	 */
	private static final long serialVersionUID = 3343361764916025402L;

	public int tempres,  mesto, stype, mater;
	public Armor armr;
	public float sost, qual;
	public Color clr;
	//	public boolean weard;

	public Shmot(int miesto, int vid, int mat, float quality) {

		super(6, 1);
		nstakbl = true;

		mesto = miesto;
		stype = vid;
		sost = 1;
		mater = mat;
		qual = quality;

		switch (miesto) {

		case 0:
			//golova
			makeHead();
			break;

		case 1:
			//torso
			makeTorso();
			break;

		case 2:
			//hands
			makeHands();
			break;

		case 3:
			//legs
			makeLegs();
			break;

		case 4:
			//boots
			makeBoots();
			break;
		}
		sost *= mat % 3 +1;
		sost *= quality;

		tempres *= quality;
		
		armr.armr *= mat % 3 +1;
		armr.armr *= quality;
		
//		armr.blRes *= mat % 3 +1;
//		armr.blRes *= quality;
//
//		armr.rezhRes *= mat % 3 +1;
//		armr.rezhRes *= quality;
//
//		armr.penRes *= mat % 3 +1;
//		armr.penRes *= quality;
	}

	private void makeBoots() {
		switch (stype) {
		case 0:
			tempres = 10;
			armr = new Armor(2);
			name = "Сапоги";
			setWeight(15);
			break;
		case 1:
			tempres = 5;
			armr = new Armor(1);
			name = "Кросовки";
			setWeight(10);
			break;
		}
	}

	private void makeLegs() {
		switch (stype) {
		case 0:
			tempres = 10;
			armr = new Armor(2);
			name = "Джинсы";
			setWeight(20);
			break;
		case 1:
			tempres = 5;
			armr = new Armor(1);
			name = "Шорты";
			setWeight(10);
			break;
		}
	}

	private void makeHands() {
		switch (stype) {
		case 0:
			tempres = 5;
			armr = new Armor(1);
			name = "Перчатки";
			setWeight(10);
			break;
		case 1:
			tempres = 15;
			armr = new Armor(1);
			name = "Варюшки";
			setWeight(10);
			break;
		}
	}

	private void makeTorso() {
		switch (stype) {
		case -10:
			//olen skin
			tempres = 20;
			armr = new Armor(5);
			name = "Шкура";
			setWeight(100);
			break;
		case 0:
			tempres = 5;
			armr = new Armor(1);
			name = "Майка";
			setWeight(20);
			break;
		case 1:
			tempres = 10;
			armr = new Armor(1);
			name = "Футболка";
			setWeight(30);
			break;
		case 2:
			tempres = 30;
			armr = new Armor(5);
			name = "Куртка";
			setWeight(150);
			break;
		}

	}

	private void makeHead() {
		switch (stype) {
		case 0:
			//abstract
			tempres = 5;
			armr = new Armor(1);
			name = "Абстрактная шляпа";
			setWeight(5);
			break;
		case 1:
			//шапка
			tempres = 10;
			armr = new Armor(1);
			name = "Шапка";
			setWeight(5);
			break;
		case 2:
			tempres = 6;
			armr = new Armor(1);
			name = "Кепка";
			setWeight(5);
			break;
		}
	}

}
