package net.mdked.Jrachgame.player;


import java.util.ArrayList;

import net.mdked.Jrachgame.player.inv.Inventory;
import net.mdked.Jrachgame.player.inv.ItmContainer;
import net.mdked.Jrachgame.player.inv.Shmot;
import net.mdked.Jrachgame.player.inv.Weapon;
import net.mdked.Jrachgame.utils.Pifagor;
import net.mdked.Jrachgame.world.Charakter;
import net.mdked.Jrachgame.world.Entity;



public class Player extends Charakter {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1619932920223923429L;

//	public float ;

	


	
	// inbatl,, agro
//	public byte weaponWld, xhd, yhd;
	
	public Entity homePoint;
	
	public ArrayList<Recipe> knownRecipes;

	public Recipe recipeBuff;

	

	
	//	plrWkDwn, plrIdleDwn, plrWkRight, plrIdleRight, plrIdleUp, 
	//	plrRunUp, plrSwimDn, plrDrwn, plrSwimRight, plrSwimUp, plrAtkDwn, plrAtkRight, plrAtkUp;



	public Player() {

		passable = true;

		enttype = 6;
		width = 6;
		height = 28;

		strength = 10;
		maxStr = strength;
		agility = 10;
		maxAgi = agility;
		vitality = 10;
		maxVit = vitality;

		hp = 100;
		hpMax = hp;

		oxygen = 100;
		oxygenMax = oxygen;

		stamina = 100;
		staminaMax = stamina;
		hung = 100;
		thirst = 100;
		temp = 100;
		tempMax = temp;

		speed = 70f;
		speedMax = speed;
		tempRes = 20f;
		tempResMax = tempRes;

		inventory = new Inventory(strength);
		knownRecipes = new ArrayList<Recipe>();
		

		giveRecipe(1);
		giveRecipe(2);
		giveRecipe(3);
		giveRecipe(4);
		giveRecipe(6);
		
		giveSkill(0,0);
		giveSkill(1,0);
		//		for (int x = 0; x < 3; x++)
		for (int a = 0; a < 5; a++) {
			Shmot wer = new Shmot(a,0,1,1);
			inventory.addItem(wer, 1);
			useItem(wer);
		}
		
		inventory.addItem(new Weapon(0,7,1f));
		inventory.addItem(new Weapon(100,7,1f));
		inventory.addItem(new ItmContainer(200,1));
		
	}

	public void giveRecipe( int type ) {
		for (int a = 0; a < knownRecipes.size(); a ++ ) 
			if (knownRecipes.get(a).getNumer() == type ) return;

		knownRecipes.add( new Recipe(type) );

	}

	
	

	public void setHomePoint(Entity home) {
		homePoint = home;
	}

	public void giveTechLev1() {
		giveRecipe(5);
	}

		
	//	public void initAnim(Animation plr) {
	//		plrA = Pifagor.makePlrAnim(plr);
	//		shirt, legs, boots, hands, hat, onehaxe
	//	}
	


}
