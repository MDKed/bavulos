package net.mdked.Jrachgame.world;

import grid.GridLocation;
import grid.GridMap;
import grid.GridPath;
import grid.GridPathfinding;
import net.mdked.Jrachgame.player.inv.Equipment;
import net.mdked.Jrachgame.player.inv.Shmot;
import net.mdked.Jrachgame.player.inv.Weapon;
import net.mdked.Jrachgame.utils.Pifagor;
import net.mdked.Jrachgame.world.ai.PFThread;
import net.mdked.Jrachgame.world.mapgen.MapManager;
import core.Path;

public class Mobster  extends Charakter {
	/**
	 * 
	 */
	private static final long serialVersionUID = -417204071135383030L;
	public static  int IDLE = 0, ROAM = 1, ATTACK = 2;

	private float  idle=0, idleMax = 1000, agro=0, agroMax = 5000, cooldown, ms300, distProid;
	//TODO Научить плавать
	//	public float tgtX, tgtY, hp, rot, speed;
	//	private boolean   attack = false, run = false, candmg = true,stpsnd, dyinsnd, agrsound, attksnd;

	public int type, logicState;
	private Path path;
	public GridPathfinding pathfinding;

	public GridLocation tgtLoc;

	private boolean agressive, watret, waitforpath;
	private int ms1000;

	//	public transient Animation[] anima;



	//	public Rectangle getBounds() {
	//		return new Rectangle(x-13, y-10, (float) 25, (float) 20);
	//	}

	public Mobster(int type) {
		pathfinding = new GridPathfinding();

		bot = true;
		enttype = 1;
		width = 5;
		logicState = 0;
		idle = (float) (Math.random() * 500);

		hung = 100;

		oxygen = 100;
		oxygenMax = oxygen;
		temp = 100;
		tempMax = temp;
		//		stand = sprite.getSprite(0, 0);

		switch (type) {
		case 0:
			//			deadani = new Animation(new SpriteSheet(sprite.getSubImage(0, 64, 96, 32), 32, 32), 100);
			//			atani =  new Animation(new SpriteSheet(sprite.getSubImage(0, 32, 128, 32), 32, 32), 100);
			//			moveani =  new Animation(new SpriteSheet(sprite.getSubImage(0, 0, 128, 32), 32, 32), 100);

			this.type = 0;
			hp = 100;
			hpMax = hp;
			//			stamina = 100;
			//			staminaMax = 100;
			agroMax = 5000;
			speed = 80f;
			agressive = false;

			stamina = 150;
			staminaMax = 150;

			equip.torso.slot = new Shmot(Equipment.TORSO, -10, 4, 1);
			equip.weapon.slot = new Weapon(-10, 1, 1);
			//			setTgt();

			giveSkill(0, 3);
			break;
		case 1:
			//	System.out.println("medved");
			//			deadani = new Animation(new SpriteSheet(sprite.getSubImage(0, 64, 192, 32), 32, 32), 100);
			//			atani =  new Animation(new SpriteSheet(sprite.getSubImage(0, 32, 160, 32), 32, 32), 100);
			//			moveani =  new Animation(new SpriteSheet(sprite.getSubImage(0, 0, 256, 32), 32, 32), 100);

			this.type = 1;
			hp = 200;
			hpMax = hp;
			stamina = 200;
			staminaMax = 200;
			agroMax = 20000;
			speed = 70f;
			agressive = true;
			//			setTgt();
			giveSkill(0, 3);
			break;
			
		case 3:
			//NPC
			enttype = 12;
			this.type = 3;
			SpawnNpc(1);
			agroMax = 20000;
			agressive = true;
			break;
		}
		//		deadani.setLooping(false);
		//		atani.setLooping(false);
	}
	/**
	 * 0- idle
	 * 1 - move
	 * 2 - attk
	 * 3-runaway
	 * @param delta
	 */
	public void think(int delta) {
		//		System.out.println(this + " think state = " + logicState + " xy-" + x + "x" + y);
		playerTurn(delta);
		//TODO возврат на сушу
		if (inWater && !watret ) {
			watret = true;

		}

		if (logicState != 2 && logicState != 3) checkForDanger();

		switch ( logicState ) {
		case 0:
			idle += delta;
			if (idle >= idleMax) 
				newTask();

			break;

		case 1:
			if (!mova(delta))
				newTask();
			reachedTgt();
			break;

		case 2:
			//			System.out.println(" case 2");
			refreshTgt(delta);
			//			System.out.println(this + " think time = " + (System.currentTimeMillis() - start));
			if (!toClose()) {
				mova(delta);
				reachedTgt();
			}
			else 
				attack();	
			//			else System.out.println("To close");
			agrManagment(delta);

			break;

		case 3:
			lookAtTarget();
			mova(delta);
			agrManagment(delta);
			break;
		}
		checkAlive();
	}

	private void attack() {
		//		System.out.println(this + " trying to attack");
		endAction(-1);
		useInHands();
	}

	private void agrManagment(int delta) {
		agro += delta;
		if (agro > agroMax) releaseAgro();

	}

	private void releaseAgro() {
		target = null;
		agro = 0;
		logicState = 0;
		newTask();
	}

	private boolean toClose() {
		if (Pifagor.getDist(this, target) < 20) {
			return true;
		}

		else return false;
	}

	private boolean pryamo() {
		if (target != null && Pifagor.getDist(this, target) >= 20 && Pifagor.getDist(this, target) < 50 ) {
			return true;
		}
		if (logicState == 2 && path == null) return true;
		
		if (logicState == 3) return true;
		
		

		return false;
	}

	private void refreshTgt(int delta) {
		ms1000 += delta;
		if (ms1000 >= 1000) {

			ms1000 = 0;
			getPathToTgt();
		} 
	}
	private void runaway() {
		lookAtTarget();
	}

	private boolean mova(int delta) {
		if (!pryamo())
			rotToTgt();
		else 
			lookAtTarget();
		return move(delta);
	}

	private void rotToTgt() {
		if (tgtLoc == null) return;
		//		int[] 
		//				otnkotr = Pifagor.getOtnXYKotr(x, y);

		int[] xyu = Pifagor.getXYfromKOTR(Pifagor.getCh(this), tgtLoc.getX(), tgtLoc.getY());
		rot = (float) Pifagor.getDirectionTo(x, y, xyu[0], xyu[1]  );

		movgrn = true;
		//		System.out.println( " ROT xy= " + x +"x" + y + " tgt xy = " + xyu[0] + "x" + xyu[1] );
	}

	private void lookAtTarget() {
		if (logicState == 2)
			rot = (float) Pifagor.getDirectionTo(this, target);
		else 
			rot = (float) ( Pifagor.getDirectionTo(this, target) + Math.toRadians(180));	

		movgrn = true;
	}

	private boolean reachedTgt() {
		//		int[] chunk = Pifagor.getCh(this);
		if (tgtLoc != null) {
			//			int[] 
			//					ot = Pifagor.getOtnXYKotr(x, y);
			int[] tgtxy = Pifagor.getXYfromKOTR(Pifagor.getCh(this), tgtLoc.getX(), tgtLoc.getY());

			//			System.out.println(" mob at " + tgtxy[0] +"x" + tgtxy[1] + " target at " + tgtLoc.getX()*32 + "x" + tgtLoc.getY()*32);
			if (x >= tgtxy[0] - 2 &&  x <= tgtxy[0] + 2 && y >= tgtxy[1] - 2  && y <= tgtxy[1] + 2 ) {

				//			if ( x == tgtxy[0] && y == tgtxy[1] ) {
				getNextXY();
				//				System.out.println(this + " reached tgt = " + logicState);

				return true;
			}
		}
		return false;
	}

	public void getNextXY() {
		//		float[] retXY = new float[2];
		//сделать получение куда идти
		if ( path != null && path.hasNextMove() ) {

			tgtLoc = (GridLocation) path.getNextMove();
			//
			//			retXY[0] = ( cornerXch * WorldData.MAPXF ) + ( next.getX() * 16 ) +8;
			//			retXY[1] = ( cornerYch * WorldData.MAPXF ) + ( next.getY() * 16 ) +8;
			//			System.out.println(" Движемся ");
		} else {
			//						System.out.println(" Путь кончился ");
			startIdle();
			//			pathEnded = true;
			//			retXY[0] = -1;
			//			retXY[1] = -1;
		}

		//		return retXY;
	}

	public void startIdle() {
		logicState = 0;
		actionType = 0;
	}

	public void newTask() {
		//random move
		//		path = 
		//		switch (logicState) {
		//		case 0:
		idle = 0;
		idleMax = (float) (1000 + (Math.random()*2000));
		getRandPath();
		logicState = 1;
		//			break;
		//		}

	}

	public void takeDamage() {
		if (Math.random() <= 0.25d)
			logicState = 2;

		if (hp < hpMax * 0.25f)
			logicState = 3;
	}

	public void checkForDanger() {
		//TODO сделать чтоб животные друг-друга жрали
		//		for (int a = 0; a < World.)
		if (Pifagor.getDist(this, World.player) <= 30) {
			//			System.out.println("   " + this + " Player is near!");
			target = World.player;
			if (agressive) 
				logicState = 2;
			else {
				path = null;
				tgtLoc = null;
				logicState = 3;
			}
		}
	}




	public void getPathToTgt() {
		if (target != null) 
			getPathToTgt(target.x, target.y);
		//
		else 
			newTask();

	}

	public void getRandPath() {
		if (!waitforpath) {
			waitforpath = true;
			//bilo nizhe
			PFThread pftr = new PFThread(this);
			pftr.start();

			//			int dist = 20;
			//			int[] otktr = Pifagor.getOtnXYKotr(x, y);
			//			//		int[] mobKotr = Pifagor.getKOTRfromfXY(x, y);
			//			GridLocation end,start;
			//			int[] chunk = Pifagor.getCh(this);
			//			GridMap gMap = MapManager.getMap(chunk[0], chunk[1]).gridMap;
			//
			//			//		do {
			//			int tgtX = (int) (1 + otktr[0] + ((Math.random()-0.5) * dist)),
			//					tgtY = (int) (1 + otktr[1] + ((Math.random()-0.5) * dist));
			//			if (tgtX >= World.chunksize) tgtX = World.chunksize - dist;
			//			if (tgtY >= World.chunksize) tgtY = World.chunksize - dist;
			//			if (tgtX < 0) tgtX = 0;
			//			if (tgtY < 0) tgtY = 0;
			//
			//			//		System.out.println(" rand path xy " + tgtX +"x" + tgtY );
			//			//		if (x < 0) tgtX++;
			//
			//			//		endI = (int) gMap.get(tgtX, tgtY);
			//			//		end = Pifagor.getGridLoc(tgtX, tgtY, true);
			//			//		} while (endI == GridMap.WALL);
			//			//TODO оптимизировать поиск путей
			//
			//			start = Pifagor.getGridLoc(otktr[0], otktr[1], false);
			//			if (gMap.get(start.getX(), start.getY()) == GridMap.WALL) {
			//				int xm,xmm,ym,ymm;
			//				if ( x < 0 ) {
			//					xm = 1;
			//					xmm = -1;
			//				} else {
			//					xm = -1;
			//					xmm = 1;
			//				}
			//				if (y < 0) {
			//					ym = 1;
			//					ymm = -1;
			//				}
			//				else {
			//					ym = -1;
			//					ymm = 1;
			//				}
			//
			//				int xpl = Math.abs(x) % 32 < 16 ? xm : xmm;
			//				int ypl = Math.abs(y) % 32 < 16 ? ym : ymm;
			//
			//				start = Pifagor.getGridLoc(otktr[0]+xpl, otktr[1]+ypl, false);
			//				//			System.out.println( x % 32 + "xxx" + y % 32);
			//			}
			//
			//			//		int pluz = 0;
			//			//		do {
			//			//			start = Pifagor.getGridLoc(otktr[0]+pluz, otktr[1], false);
			//			//			pluz++;
			//			////			System.out.println("pluz = " + pluz);
			//			//		} while (gMap.get(start.getX(), start.getY()) == GridMap.WALL); 
			//			//		
			//			int pluzz = 0;
			//			do {
			//				end = Pifagor.getGridLoc(tgtX+pluzz, tgtY, true);
			//				if (tgtX+pluzz < 511)
			//					pluzz++;
			//				else pluzz -= 15;
			////							System.out.println("tgtx= " + tgtX + " tgtY= " + tgtY);
			//
			//			} while (gMap.get(end.getX(), end.getY()) == GridMap.WALL);



		}
		//		long starata = System.currentTimeMillis();
		//		path =  pathfinding.getPath(start, end , gMap );
		//		long endn = System.currentTimeMillis() - starata;
		//		System.out.println(" path tiem " + endn + " path = " + start.getX() + "x" + start.getY() + " end = " + end.getX() +"x"+ end.getY());
		//		getNextXY();d
		//		if (tgtLoc != null) {
		//			//			System.out.println(" move to " + tgtLoc.getX() + "x" + tgtLoc.getY());
		//			if (MapManager.getMap(chunk[0], chunk[1]).gridMap.get(tgtLoc.getX(), tgtLoc.getY()) == GridMap.WALL) {
		//				System.out.println(" Wazalalzlazlll");
		//				tgtLoc = new GridLocation(tgtLoc.getX()+1, tgtLoc.getY(), true);
		//			}
		//		}
	}

	public void getPathToTgt(float tgtX, float tgtY) {
		if (!waitforpath) {
			waitforpath = true;
			PFThread pftr = new PFThread(this, tgtX, tgtY);
			pftr.start();
		}

		//		int[] otktr = Pifagor.getOtnXYKotr(x, y),
		//				otgt = Pifagor.getOtnXYKotr(tgtX, tgtY);
		//		if (otktr[0] == otgt[0] && otktr[1] == otgt[1] ) return;
		//
		//		//		System.out.println("animal at " + otktr[0] + "x" + otktr[1] + " target at " + otgt[0] +"x"+ otgt[1]);
		//		int[] chunk = Pifagor.getCh(this);
		//		GridMap gMap = MapManager.getMap(chunk[0], chunk[1]).gridMap;
		//		GridLocation end = Pifagor.getGridLoc(otgt[0], otgt[1], true);
		//		GridPathfinding pathfinding = new GridPathfinding();
		//		//		System.out.println(" time to lag!");
		//		path =  pathfinding.getPath(Pifagor.getGridLoc(otktr[0], otktr[1], false), end , gMap );
		//		//		System.out.println(" path is " + path);
		//		getNextXY();
		//		getNextXY();

	}
	public void setPath(GridPath path2) {
		path = path2;
		waitforpath = false;
	}


















	//	
	//	
	//	public void attacked(Entity who) {
	//		if (!attack && target == null) {
	//			//System.out.println("zhivot atakovan");
	//			target = who;
	//			double agPrc = 0.5;
	//			switch (type) {
	//			case 0:
	//				agPrc = 0.2;
	//				break;
	//			case 1:
	//				agPrc = 1;
	//				break;
	//			}
	//			if ( Math.random() <= agPrc ) {
	//				//	System.out.println("i atakyet");
	//				attack(who);
	//			} else  {
	//				//	System.out.println("i ybegaet");
	//				run = true;
	//			}
	//			if (!agrsound) {
	//				switch (type) {
	//				case 0:
	//					DasGame.playSound(7,this);
	//					agrsound = true;
	//					break;
	//				case 1:
	//					DasGame.playSound(11, this);
	//					agrsound = true;
	//					break;
	//				}
	//			}
	//		}
	//	}
	//
	//	public void think(int delta) {
	//
	//		if (hp <= 0 && !dead && !dying) {
	//			//System.out.println("  animal is dead");
	//			moving = false;
	//			stpsnd = false;
	//			attacking = false;
	////			dying = true;
	//			attack = false;
	////			criticalWound = false;
	//
	//
	//			if (!dyinsnd) {
	//				switch (type) {
	//				case 0:
	//					DasGame.playSound(10, this);
	//					dyinsnd = true;
	//					break;
	//				case 1:
	//					DasGame.playSound(14, this);
	//					dyinsnd = true;
	//					break;
	//				}
	//			}
	//
	//			if (target instanceof Player) {
	//				int pnts = 100;
	//				switch (type) {
	//				case 1:
	//					pnts += 400;
	//					break;
	//				}
	//				World.player.score += pnts;
	//			}
	//			target = null;
	//		}
	////		if (dying && deadani.isStopped()) {
	////			dead = true;
	////			dying = false;
	////			//			deadani.restart();
	////			leaveBody();
	////		}
	//		if (!dead && !dying) {
	//			agrsound = false;
	//			//step sound
	//			if (!stpsnd && moving && type == 0 && moveani.getFrame() == 3) {
	//				//System.out.println("igrai zvuk olen "+ moveani.getFrame() + " " + this.toString());
	//
	//				DasGame.playSound(1, this);
	//				DasGame.makeSled(x-2, y, (byte) 1, rot,false);
	//				DasGame.makeSled(x+2, y-2, (byte) 1, rot,false);
	//				stpsnd = true;
	//			}
	//			if (!stpsnd && moving && type == 1 && moveani.getFrame() == 1 || !stpsnd && type == 1 && moving && moveani.getFrame() == 4) {
	//				stpsnd = true;
	//				//System.out.println("igrai zvuk medve "+ moveani.getFrame() + " " + this.toString());
	//				DasGame.playSound(2, this);
	////				switch (moveani.getFrame()) {
	////				case 1:
	////					DasGame.makeSled(x-9, y, (byte) 2, rot, false);
	////					break;
	////				case 4:
	////					DasGame.makeSled(x+9, y, (byte) 2, rot, true);
	////					break;
	////				}
	//
	//
	//			}
	//			if (stpsnd && type == 0 && moveani.getFrame() == 2 )
	//				stpsnd = false;
	//			if (stpsnd && type == 1 && moveani.getFrame() == 2 || moveani.getFrame() == 5 )
	//				stpsnd = false;
	//			//----
	//			if (!attack && run || !attack && Pifagor.getDist(this, World.player) <= 40) {
	//				switch (type) {
	//				//логика для оленя, убегать от плеера
	//				case 0:
	//					haveTgt = true;
	//					tgtX = x + (x - World.player.x)*10;
	//					tgtY = y + (y - World.player.y)*10;
	//					run = false;
	//					break;
	//				case 1:
	//					if (World.player.hp > 0)
	//						attacked(World.player);
	//					break;
	//				}
	//			}
	//
	//
	//			moving = false;
	//			if (!attack && haveTgt && x >= tgtX-10 && x <= tgtX+10 && y >= tgtY-10 &&  y <= tgtY+10) {
	//
	//				resetTgt();
	//			}
	//			//фаза боя
	//			if (attack) {
	//
	//				if (atani.isStopped()) {
	//					atani.restart();
	//					attacking = false;
	//
	//				}
	//				if (!candmg) {
	//					if (cooldown >= 850) {
	//						candmg = true;
	//						cooldown = 0;
	//						attksnd = false;
	//					} else
	//						cooldown += delta;
	//
	//				}
	//				if (attacking && candmg ) {
	//					switch (type) {
	//					case 0:
	//						if (atani.getFrame() == 3) {
	//							candmg = false;
	//							damageTgt();
	//							if (!attksnd)
	//								DasGame.playSound(8, this);
	//						}
	//						break;
	//					case 1:
	//						if (atani.getFrame() == 4) {
	//							candmg = false;
	//							damageTgt();
	//							if (!attksnd)
	//								DasGame.playSound(12, this);
	//						}
	//						break;
	//					}
	//				}
	//
	//				if (agro < agroMax) {
	//					agro++;
	//
	//					setTgt();
	//				} else {
	//					agrsound = false;
	//					attack = false;
	//					resetTgt();
	//				}
	//				if (target != null && !attacking && Pifagor.getDist(x, target.x, y, target.y) <= 20 && candmg) {
	//					//System.out.println("  ykushu!");
	//					attacking = true;
	//					haveTgt = false;
	//					agro = 0;
	//				}
	//
	//			}
	//			//нормальная фаза
	//			if (!haveTgt && idle < idleMax) {
	//				stpsnd = false;
	//				//	System.out.println(" olen jdet "+ idle + "/" + idleMax);
	//				idle += delta;
	//			} else if (!haveTgt && idle >= idleMax){
	//
	//				resetTgt();
	//				setTgt();
	//			}
	//		}
	//		else if (dead ) {
	//			/*if (respTimer < 10000)
	//				respTimer += delta;
	//			else {*/
	//			//System.out.println(" poka zhivotnae");
	//			//			x = (float) ((128 * 32) * Math.random());
	//			//			y = (float) ((128 * 32) * Math.random());
	//			//
	//			//			if (type == 0) {
	//			//				hp = 100;
	//			//			} else hp = 200;
	//			//
	//			//
	//			//			//respTimer = 0;
	//			//			agrsound = false;
	//			//			dead = false;
	//			//			setTgt();
	//
	//			//}
	//			World.removeEntityFromWorld(this);
	//		}
	//		
	//		//movement
	//		if (!attacking && haveTgt && !dead && !dying) {
	//			//System.out.println(" olens GO hp = " + animal.hp);
	//			float oldX = x;
	//			float oldY = y;
	//			float speedd = speed;
	//			
	//			if (criticalWound) { 
	//				if (Math.random() <= 0.1d) {
	//					byte kind = (byte) (3 + (byte) (Math.random() * 2));
	//					DasGame.makeSled(x, y, kind, 0, false);
	//				}
	//				speedd /= 2;
	//			}
	//
	//			if (tgtX - 5 > x ) {
	//				//System.out.println( animal.x);
	//
	//				x += speedd * delta;
	//
	//				if (World.isAccsbl(this)) {
	//					moving = true;
	//					faceRight = true;
	//				} else {
	//					//						System.out.println("RASPIDORASILO");
	//					x =  oldX;
	//					resetTgt();
	//					setTgt();
	//				}
	//
	//			}
	//			if (tgtX + 5 < x) {
	//				//float oldX = animal.x;
	//				x += -speedd * delta;
	//
	//				if (World.isAccsbl(this)) {
	//					moving = true;
	//					faceRight = false;
	//				} else {
	//					x = oldX;
	//					resetTgt();
	//					setTgt();
	//				}
	//			}
	//			if (tgtY - 5 > y ) {
	//
	//				//float oldY = animal.y;
	//				y += speedd * delta;
	//
	//				if (World.isAccsbl(this)) {
	//					moving = true;
	//				} else {
	//					y =  oldY;
	//					resetTgt();
	//					setTgt();
	//				}
	//
	//			}
	//			if (tgtY + 5 < y) {
	//				//float oldY = animal.y;
	//				y += -speedd * delta;
	//
	//				if (World.isAccsbl(this)) {
	//					moving = true;
	//				} else {
	//					y =  oldY;
	//					resetTgt();
	//					setTgt();
	//				}
	//			}
	//			rot = (float)Math.atan2((oldY-y), (oldX-x)) ;
	//		}
	//	}
	//
	//	public void attack(Entity target) {
	//		attack = true;
	//		setTgt();
	//	}
	//
	//	public void resetTgt() {
	//		//System.out.println("  resert");
	//		run = false;
	//		idle = 0;
	//		haveTgt = false;
	//	}
	//	public void damageTgt() {
	//		float dmg = 15;
	//		switch (type) {
	//		case 0:
	//			dmg += Math.random()*10;
	//			break;
	//		case 1:
	//			dmg = (float) (25 + (Math.random() * 15));
	//		}
	//		if (target instanceof Player) {
	//			World.player.hp -= dmg;
	//			if ( Math.random() <= 0.2d ) {
	//				switch (type) {
	//				case 0:
	//					World.player.addBuff(new Buff(3,10));
	//					break;
	//				case 1:
	//					World.player.addBuff(new Buff(4,20));
	//					break;
	//				}
	//			}
	//			DasGame.playSound(6, World.player);
	//			if (World.player.hp <=0 ) {
	//				attack = false;
	//				agro = 0;
	//				resetTgt();
	//			}
	//		}
	//		if (target instanceof Animal) {
	//			Animal an = (Animal) target;
	//			an.hp -=dmg;
	//		}
	//	}
	//
	//	public void setTgt() {
	//		if (!haveTgt && !attack) {
	//			haveTgt = true;
	//			//idle = 0;
	//			idleMax = (float) (2000 + (Math.random() * 3000));
	//			tgtX = (float) ( x + (Math.random()*500) - 250);
	//			tgtY = (float) ( y + ( Math.random()*500) - 250);
	//			//System.out.println(" olen idet v " + tgtX + "x" + tgtY);
	//		}
	//		if (attack && Pifagor.getDist(x, target.x, y, target.y) > 15) {
	//			haveTgt = true;
	//			tgtX = target.x;
	//			tgtY = target.y;
	//		}
	//	}
	//	private void leaveBody() {
	//		World.createDragAble(type, x, y);
	//	}
}
