package net.mdked.Jrachgame.world.ai;

import grid.GridLocation;
import grid.GridMap;
import grid.GridPathfinding;
import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.utils.Pifagor;
import net.mdked.Jrachgame.world.Mobster;
import net.mdked.Jrachgame.world.World;
import net.mdked.Jrachgame.world.mapgen.MapManager;

public class PFThread implements Runnable{
	private Thread t;

	private Mobster faza;
	private float tgtX, tgtY;

	private int type;
	


	public PFThread(Mobster zakaz, float x, float y) {
		type = 1;
		faza = zakaz;
		tgtX = x;
		tgtY = y;
	}

	public PFThread(Mobster zak) {
		type = 2;
		faza = zak;

	}

	@Override
	public void run() {
		if (type == 1) {
			int[] otktr = Pifagor.getOtnXYKotr(faza.x, faza.y),
					otgt = Pifagor.getOtnXYKotr(tgtX, tgtY);
			if (otktr[0] == otgt[0] && otktr[1] == otgt[1] ) return;

			int[] chunk = Pifagor.getCh(faza);
			GridMap gMap = World.map.gridMap;
			GridLocation end = Pifagor.getGridLoc(otgt[0], otgt[1], true);
			GridPathfinding pathfinding = new GridPathfinding();
			faza.setPath(pathfinding.getPath(Pifagor.getGridLoc(otktr[0], otktr[1], false), end , gMap ));
			faza.getNextXY();
		}

		if ( type == 2 ) {
			
			int dist = 20;
			int[] otktr = Pifagor.getOtnXYKotr(faza.x, faza.y);
			GridLocation end,start;
			int[] chunk = Pifagor.getCh(faza);
			GridMap gMap = World.map.gridMap;

			int tgtX = (int) (1 + otktr[0] + ((Math.random()-0.5) * dist)),
					tgtY = (int) (1 + otktr[1] + ((Math.random()-0.5) * dist));
			if (tgtX >= World.chunksize) tgtX = World.chunksize - dist;
			if (tgtY >= World.chunksize) tgtY = World.chunksize - dist;
			if (tgtX < 0) tgtX = 0;
			if (tgtY < 0) tgtY = 0;


			start = Pifagor.getGridLoc(otktr[0], otktr[1], false);
			if (gMap.get(start.getX(), start.getY()) == GridMap.WALL) {
				int xm,xmm,ym,ymm;
				if ( faza.x < 0 ) {
					xm = 1;
					xmm = -1;
				} else {
					xm = -1;
					xmm = 1;
				}
				if (faza.y < 0) {
					ym = 1;
					ymm = -1;
				}
				else {
					ym = -1;
					ymm = 1;
				}

				int xpl = Math.abs(faza.x) % 32 < 16 ? xm : xmm;
				int ypl = Math.abs(faza.y) % 32 < 16 ? ym : ymm;

				start = Pifagor.getGridLoc(otktr[0]+xpl, otktr[1]+ypl, false);
			}

				
			int pluzz = 0;
			do {
				end = Pifagor.getGridLoc(tgtX+pluzz, tgtY, true);
				if (tgtX+pluzz < 511)
					pluzz++;
				else pluzz -= 15;
//							System.out.println("tgtx= " + tgtX + " tgtY= " + tgtY);

			} while (gMap.get(end.getX(), end.getY()) == GridMap.WALL);
			
			long starata = System.currentTimeMillis();
			
			faza.setPath(faza.pathfinding.getPath(start, end , gMap ));
			faza.getNextXY();
			long endn = System.currentTimeMillis() - starata;
//			if (endn > 100)
//			System.out.println("    !!!!!!!!!!!path tiem " + endn + " path = " + start.getX() + "x" + start.getY() + " end = " + end.getX() +"x"+ end.getY());

		}
	}

	public void start ()
	{
//		System.out.println("Starting PF for " + faza );
		if (t == null)
		{
			t = new Thread (this);
//			t.setPriority(4);
			t.start ();
		}
	}
}
