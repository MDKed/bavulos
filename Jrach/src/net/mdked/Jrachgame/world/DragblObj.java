package net.mdked.Jrachgame.world;

import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.utils.Pifagor;

public class DragblObj extends Mineral {
	/**
	 * 
	 */
	private static final long serialVersionUID = 32277303480735773L;
	
	public Charakter host;
	public int dtype;
	private int sledkd = 0;
	
	public DragblObj(Minable faza) {
		super(faza.typeofres);
		passable = true;
		enttype = 7;
		x = faza.x;
		y = faza.y;
		
		World.addEntitytoQWorld(this, false);
		
		System.out.println(" dragbl id- " + id + " worid " + World.getIds());
		
	}

	public DragblObj(int type) {
		super(type);
//		System.out.println("  drggggggggbl type= " + type);
		enttype = 7;
		passable = true;
		dtype = type;
	}
	

	public void startPull(Charakter plr) {
		host = plr;
	}

	public void stopPull() {
		host = null;
	}

	public void pull(int delta) {
		if (host != null && Pifagor.getDist(host.x, x, host.y, y) > 20) {
			if (sledkd >= 100) {
				DasGame.makeSled(x-2, y, (byte) 6, 0, false);
				sledkd = 0;
			}
			sledkd += delta;
			
			Pifagor.moveByRot(this, delta);
//			float spd = host.speed/1000;
//			rot = Pifagor.getDirectionTo(this, host);
			
//			if (x < host.x) x += spd*delta;
//			if (x > host.x) x -= spd*delta;
//			if (y < host.y) y += spd*delta;
//			if (y > host.y) y -= spd*delta;
		}
	}
}
