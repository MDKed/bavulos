package net.mdked.Jrachgame.world;

import java.util.ArrayList;

import net.mdked.Jrachgame.player.inv.Inventory;
import net.mdked.Jrachgame.player.inv.Item;
import net.mdked.Jrachgame.player.inv.Shmot;
/**
 * 1 - palatk
 * 2 - lesopilk
 * @author operator-7
 *
 */
public class Facility extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6363804202210807768L;
	//сделать список типов предприятий зависящий от типа статического объекта,
	// и уже сообразить на тип функций который он исполняет
	private ArrayList<Byte> vzmjnst;
	public Inventory inventory;
	public int facType;

	public Facility(int type, float x, float y) {
		enttype = 2;
		inventory = new Inventory(100);
		vzmjnst = new ArrayList<Byte>();
		facType = type;
		this.x = x;
		this.y = y;
		/*
		 * types - 
		 * 1 - palatka start: myaso, kosti, koja
		 * 2 - workshop
		 */
		switch (type) {
		case 1:
//			soType = 4;
			width = 16;
			
			World.player.setHomePoint(this);
//			for (int xa = 0; xa < 20; xa++)
//			addItem(new Shmot(0,xa,1,0,0));
		
			break;
		case 2:
//			soType = 5;
			width = 16;
			vzmjnst.add((byte) 0);
			vzmjnst.add((byte) 2);
			vzmjnst.add((byte) 3);
			break;
		}
//		World.addEntityToWorld(this, true);
	}

	public boolean mojPer(Mineral object) {
		for (int a = 0; a<vzmjnst.size(); a++) {
			if (object.resType == vzmjnst.get(a)) return true;
		}
		return false;
	}
	
	public void pererab(Mineral perT) {
		switch (perT.resType) {
		case 0:
			addItem(new Item(2,perT.minSumm));
			break;

		case 2:
		case 3:
			addItem(new Item(0,perT.minSumm));
			addItem(new Item(1,(int) (perT.minSumm*0.1)));
			addItem(new Item(4,(int) (perT.minSumm*0.2)));
			break;

		}
		World.player.stopPullin();
		World.removeEntityFromWorld(perT);
	}

	private void addItem(Item additem) {
		boolean gotovo = false;
		while (!gotovo) {
			if (!inventory.addItem(additem)) {
				if ( !additem.otnyat(1) ) {
					gotovo = true;
				}
			} else gotovo = true;
		}
	}
}
