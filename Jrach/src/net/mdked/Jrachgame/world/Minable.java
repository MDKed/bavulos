package net.mdked.Jrachgame.world;

import net.mdked.Jrachgame.world.mapgen.MapGen;

import org.newdawn.slick.Color;

/**
 *
 * 0- лес
 * 1- камень
 * 2- куст зелень
 * 3- куст сухой
 * 4- трава
 * 5- трава+цветок
 * 6- лиственница
 * 7 - лысое
 * @author Папка-Ситх
 *
 */
public class Minable extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8096204108375205914L;


	public float hp;
	public byte armortype;
	public byte rank, typeofres, mtype;
	public boolean destroin;
//	public Color  fColor, sColor;
	public int fColor, sColor;
	
//	public shorwwt;

	/**
	 * типы брони :
	 *  1 -бьет только рубящее
	 *  2- кирка
	 * @param x
	 * @param y
	 * @param type
	 * @param rank
	 */
	public Minable(float x, float y, byte type, byte rank, short biome) {
		//
		enttype = 0;
		this.x = x;
		this.y = y;
		this.rank = rank;
		width = 7;
		hp = (float) (rank*(50 + (Math.random()*10)));
		switch (type) {
		
		case 0:
			mtype = type;
			armortype=1;
			this.typeofres = 0;
			break;
			
		case 1:
			mtype = type;
			armortype =2;
			break;
			
		case 2:
			mtype = type;
			armortype=0;
			setFColor(biome);
			passable = true;
			break;
			
		case 3:
			mtype = type;
			armortype = 0;
			passable = true;
			break;
			
		case 4:
			mtype = type;
			armortype = 0;
			setFColor(biome);
			passable = true;
			break;
			
		case 5:
			mtype = type;
			armortype = 0;
			setFColor(biome);
			setSColor();
			passable = true;
			break;
			
		case 6:
			mtype = type;
			armortype = 1;
			setFColor(biome);
			this.typeofres = 0;
			break;
			
		case 7:
			mtype = type;
			armortype = 1;
			this.typeofres = 0;
			break;
		}
//		System.out.println( " obj biome = " + biome + "/n color = " + fColor);
//		World.addEntityToWorld(this);
	}

//	public void setColors() {
//		fColor = 
//				fColor.decode(ffColor);
//				if (!ssColor.isEmpty())
//					sColor = sColor.decode(ssColor);
//	}

	private void setSColor() {
		sColor = (int) (Math.random()*4);
//		switch (clrn){
//		case 0:
//			ssColor = Color.red.toString();
//			break;
//		case 1:
//			ssColor = Color.blue.toString();
//			break;
//		case 2:
//			ssColor = Color.yellow.toString();
//			break;
//		case 3:
//			ssColor = Color.pink.toString();
//			break;
//		}
	}
	private void setFColor(short biome) {
		fColor = biome;
		
	}
	
	public Color getFColor() {
		return MapGen.getColor((short) fColor);
	}
	public Color getSColor() {
		return MapGen.getColor((short) sColor);
	}

	public boolean damage(float dmg, byte arPiercType) {
		if (arPiercType == armortype && hp > 0) {
			hp -= dmg;
//			System.out.println(" CHope");
			emmitText("Chop", z + 32 , 0);
			if (hp <= 0) {
				leaveDragbl();
				
//				switch (typeofres) {
//				case 0:
//					destroin = true;
//					break;
//				case 1:
//					//TODO камень будет выпадать шмотом
//					break;
//				}
				//TODO spawn loot, delete obj;
			}
			return true;
		} 
		return false;
	}
//	Minable minable = (Minable) so;
//	if (minable.destroin) {
//		fallingtree.draw(wtdX-(32*xScale), wtdY-(64*xScale), 64*xScale ,64*xScale);
//		if (fallingtree.isStopped()) {
//			minable.leaveDragbl();
//			fallingtree.restart();
//			World.objecz.remove(so);
//
//			//сделать рубку
//		}
//	}
//	else
	
	public void leaveDragbl() {
		//		switch (typeofres) {
		//		case 0:
		new DragblObj(this);
//		World.objecz.remove(this);
		World.removeEntityFromWorld(this);
//		World.removeEntityInOch(this);
		//			break;
		//		}
		//		corpse.x = x;
		//		corpse.y = y;
		//		World.objecz.add(corpse);
	}
}
