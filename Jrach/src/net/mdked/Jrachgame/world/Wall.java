package net.mdked.Jrachgame.world;

public class Wall extends Entity {

	public int ico, type;

	public Wall(float x, float y, int type) {
		enttype = 11;
		this.x = x;
		this.y = y;
		this.type = type;
	}

	public void checkType(boolean step) {
		boolean up = false, right = false, down = false, left = false;
		for (int a = 0; a < World.map.objects.size(); a++) {

			Entity chek = World.map.objects.get(a);
			if (chek.enttype != 11) continue;
			Wall chekoi = (Wall) chek;
			if (chek.x == x) {
				if (chek.y == y+32) {
					down = true;
					if (step)
						chekoi.checkType(false);
				}
				if (chek.y == y-32) {
					up = true;
					if (step)
						chekoi.checkType(false);
				}
			}
			if (chek.y == y) {
				if (chek.x == x+32) {
					right = true;
					if (step)
						chekoi.checkType(false);
				}
				if (chek.x == x-32) {
					left = true;
					if (step)
						chekoi.checkType(false);
				}
			}

		}

		if (right && !left && !down && !up ) 
			ico = 1;
		else if (!right && left && !down && !up )
			ico = 3;
		else if (!right && !left && down && !up )
			ico = 2;
		else if (!right && !left && !down && up )
			ico = 4;
		else if (right && left && !down && !up )
			ico = 5;
		else if (!right && !left && down && up )
			ico = 6;
		else if (right && !left && down && !up )
			ico = 7;
		else if (!right && left && down && !up )
			ico = 8;
		else if (!right && left && !down && up )
			ico = 9;
		else if (right && !left && !down && up )
			ico = 10;
		else if (right && !left && down && up )
			ico = 11;
		else if (right && left && down && !up )
			ico = 12;
		else if (!right && left && down && up )
			ico = 13;
		else if (right && left && !down && up )
			ico = 14;
		else if (right && left && down && up )
			ico = 15;
		else 
			ico = 0;

	}

}
