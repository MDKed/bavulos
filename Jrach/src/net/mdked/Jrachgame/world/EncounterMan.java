package net.mdked.Jrachgame.world;

import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.utils.Pifagor;

public class EncounterMan {

	public static void randomAnimalSpawn(int ochX, int ochY) {
		if ( World.animalz.size() < 5 && Math.random() <= 0.11 ) { 
			Mobster prdmt;
			int type;
			//			if (Math.random() <= 0.01 ) 
			//				type = 1;
			//			else 
			type = 3;

			prdmt = new Mobster(type);

			switch (type) {
			case 0:
				prdmt.corpusA = Pifagor.makeAnimAnim(DasGame.deer, type);
				break;
			}

			getSpawnPlace(prdmt, ochX, ochY);

			if (World.isAccsbl(prdmt))
				World.addEntityToWorld(prdmt, false);

		}
	}



	public static void getSpawnPlace(Entity ent, int ochX, int ochY) {
		int dist = 100;
		//422
		int tryx = 0;
		//(float) (World.player.rot - Math.toRadians(90) );
		do {
			float rotation = (float) Math.toRadians(360*Math.random());

			//			ent.x = (float)( World.objchunksize*ochX + ((World.objchunksize) * Math.random())) * 32f;
			//			ent.y = (float)( World.objchunksize*ochY + ((World.objchunksize) * Math.random())) * 32f;
			ent.x = (float) (World.player.x + (dist * Math.sin(rotation)) + ( (120f*Math.random()) - 60f));
			ent.y =  (float) (World.player.y - (dist * Math.cos(rotation))+ ( (120f*Math.random()) - 60f));

			tryx++;
			//			System.out.println(tryx);
			if (tryx > 10) break;
		}
		while ( World.isAccsbl(ent) );

		//		System.out.println(" color = " + MapManager.getColor(ret[0], ret[1]) +
		//				" collision = " + CollisionMan.checkCollison(ent) +
		//				" id =" + World.ids);
	}


}
