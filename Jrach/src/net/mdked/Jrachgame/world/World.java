package net.mdked.Jrachgame.world;

import grid.GridMap;

import java.util.ArrayList;

import net.mdked.Jrachgame.core.CollisionMan;
import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.player.Player;
import net.mdked.Jrachgame.player.inv.Weapon;
import net.mdked.Jrachgame.utils.Pifagor;
import net.mdked.Jrachgame.utils.PropManager;
import net.mdked.Jrachgame.utils.controls.Controls;
import net.mdked.Jrachgame.world.efcs.Sled;
import net.mdked.Jrachgame.world.mapgen.MapChunk;
import net.mdked.Jrachgame.world.mapgen.MapGen;
import net.mdked.Jrachgame.world.mapgen.MapManager;
import net.mdked.Jrachgame.world.mapgen.ObjChunk;

public class World {
	public transient static ArrayList<Mobster> animalz;
	//	public static ArrayList<Entity> objecz;
	public static MapChunk map;

	public static ArrayList<Missile> misll;
	public static ArrayList<Pribor> pribrs;
	public static ArrayList<Facility> facs;
//	public static ArrayList<Wall> wals;
	public static ArrayList<Sled> sledi;
	public static Player player;
	//	public static int a;


	//mapdata bigmap,  arhipelago
	public static int seed, chunksize, objchunksize;
	public static float roughness;
	public static double waterlevel;

	public static float windspd, windrot;
	//	public static double lastMidHt;
	//	public volatile static MapChunk bigmap;

	//	public transient volatile static ArrayList<MapChunk> mapCache;
	//	public transient static ArrayList<ObjChunk> objChunks;
	//	public volatile static ArrayList<MapChunk> bigmapCache;

	private static ArrayList<Entity> deleteQue;
	private static ArrayList<AddQoo> addQue;

	public static boolean acbar;
	public static int ids;
	
	public static int[] currCHXY;

	public static Controls controls;



	public World() {
		controls = new Controls();

		sledi = new ArrayList<Sled>();
		animalz = new ArrayList<Mobster>();
		//		objecz = new ArrayList<Entity>();
		misll = new ArrayList<Missile>();
		pribrs = new ArrayList<Pribor>();
		facs = new ArrayList<Facility>();
		player = new Player();

		deleteQue = new ArrayList<Entity>();
		addQue = new ArrayList<AddQoo>();

		//		mapCache = new ArrayList<MapChunk>();
		//		objChunks = new ArrayList<ObjChunk>();
		//		bigmapCache = new ArrayList<MapChunk>();


		//		temperature = 0;

		//mapgen data
		seed = (int) (Math.random() * Integer.MAX_VALUE-1);
		System.out.println(" Seed = " + seed);
		roughness = 10f;
		waterlevel = 0.05d;
		chunksize = 512;
		objchunksize = 32;

		windrot = (float) Math.toRadians(45);
		windspd = 10;




	}

	public static void setMap(MapChunk mape) {
		map = mape;
		int topid= 0;
		for (int a =0 ; a< map.objects.size(); a++) {
			int ido = map.objects.get(a).id;
			if (ido > topid) topid = ido;
		}
		ids = topid;
		
		DasGame.worldIsReady = true;

	}
	/*
	 * маппп манагер будет запрашивать объекты из ближайших чанков если они не в буффере
	 * в треде будет пытаться загрузить или создать
	 */
	//	public static synchronized boolean addMapToCache( MapChunk map ) {
	//		mapCache.add(map);
	//		//		lastMidHt = map.midHt;
	//		//		System.out.println(" Added map " + map.xCh + "x" + map.yCh);
	//		return true;
	//	}

	public static synchronized void addWall( int xK, int yK, int wat ) {
		// берём чанк и хуярим туда клетку стенки
		//		int[] chk= Pifagor.getXchYch(xK, yK);
		//		MapChunk chunk = DasGame.mapMan.getMap(chk[0], chk[1]);
		map.gridMap.set(xK,yK,wat);
		//		System.out.println(chunk);
		//		if (!chunk.formed) return;
		//		int xkotr= (wChX*World.objchunksize) + xK,
		//				ykotr = (wChY*World.objchunksize) + yK;
		//		int[] kotr = Pifagor.getXYminCHfd(xK, yK);
		//		chunk.gridMap.set(kotr[0], kotr[1], wat);


	}

	public synchronized static void insertObjFromChunkToWorld( ObjChunk map ) {
		//		System.out.println(" Inserting objects ");
		//		objChunks.add(map);
		for (int a = 0; a < map.objcz.size(); a ++) {
			addEntitytoQWorld(map.objcz.get(a), true);
		}
	}

	private static synchronized boolean checkForImen(Entity object) {
		for (int a = 0; a < map.objects.size(); a++) 
			if (object.id == map.objects.get(a).id) return false;

		return true;
	}
	public static void addEntityToWorld( Entity object, boolean noIns) {

		if (!checkForImen(object)) return;
		boolean	insOch = false, wAdd = false;

		if (!noIns)
			switch (object.enttype) {
			case 0:
			case 2:
			case 7:
			case 8:
				insOch = true;
				break;
			}

		//		if (insOch) insertEntityInOCh(object);

		map.objects.add(object);
		//		if (object.id == 0) object.id = ++World.ids;

		switch (object.enttype) {
		//		case 0:
		//			//			mapChunk.statobj.add((StaticObj) object);
		//			World.stObjecz.add((StaticObj) object);
		//			break;
		case 1:
		case 12:
			//			mapChunk.animalz.add((Animal) object);
			World.animalz.add((Mobster) object);
			break;
		case 2:
			wAdd = true;
			facs.add((Facility) object);
			break;
		case 3:
		case 9:
			World.sledi.add((Sled) object);
			break;
		case 5:
			World.misll.add((Missile) object);
			break;

		case 8:
			//bribor
			World.pribrs.add((Pribor) object);
			wAdd = true;
			break;
			
		case 11:
			wAdd = true;
			break;


		}
		if (wAdd) 
		{
			int[] kotr = Pifagor.getKOTRfromfXY(object.x, object.y);
			addWall(kotr[0], kotr[1], -1);
		}
	}

	public static void createMissile(Weapon wpn, Entity origin) {
		//от игрока идет проверка что у него в руках, если это метательное
		//		то запускается анимация броска, по завершению анимации из игрока вылетает
		// копье либо что-то другое
		//		switch (wpn.drawId) {
		//		case 100:
		Missile spear = new Missile(wpn);
		spear.rot = (float) Pifagor.getDirectionTo(player, DasGame.mouz);
		spear.x = origin.x;
		spear.y = origin.y;
		spear.host = origin;
		// сделать определение скорости зависящее от расстояния и цели от скила
		spear.z = 16;
		spear.zSpeed = 40;
		World.addEntityToWorld(spear, false);
		//				World.misll.add(spear);
		//				World.objecz.add(spear);
		//						playerC.spearQ--;

		DasGame.playSound(4, player);
		//			break:
		//		}
	}





	//	private static void insertEntityInOCh(Entity object) {
	//		//		int[] oxy = Pifagor.getOCHfXY(object.x, object.y);
	//		//		ObjChunk och = MapManager.getOCh(oxy[0], oxy[1]);
	//		//		och.objcz.add(object);
	//		DasGame.mapMan.addEntity(object);
	//	}
	//
	//	public static void removeEntityInOch(Entity object) {
	//		//		int[] oxy = Pifagor.getOCHfXY(object.x, object.y);
	//		//		ObjChunk och = MapManager.getOCh(oxy[0], oxy[1]);
	//		//		if (och.objcz.contains(object))
	//		//			och.objcz.remove(object);
	//		DasGame.mapMan.remEntity(object);
	//	}

	public static void createDragAble(Charakter chara, float x, float y) {
		DragblObj corpse = null;
		//		System.out.println(" creating drggggggggbl");
		if (chara.enttype == 1) {
			Mobster moba = (Mobster) chara;
			switch (moba.type) {
			case 0:
				corpse = new DragblObj(2);
				break;
			case 1: 
				corpse = new DragblObj(3);
				break;

			}
		}

		corpse.x = x;
		corpse.y = y;
		addEntityToWorld(corpse, false);

	}
	/**
	 * получение цвета по котру
	 * @param x
	 * @param y
	 * @return
	 */
	public synchronized static short getColor(int x, int y) {
		//		MapChunk chunk = MapGen.getChunkXY(x, y);
//		int[] xy = Pifagor.get(x, y);
		if (map != null && x >= 0 && x < chunksize && y >= 0 && y < chunksize) 
			return map.getColorDat(x, y);

		else 
			return 900;
	}

	public static boolean isAccsbl( Entity ent ) {
		int[] ret = Pifagor.getKOTRfromfXY(ent.x, ent.y);

		if ( getColor(ret[0], ret[1]) >= 900 || CollisionMan.checkCollison(ent) ) 
			return false;
		else 
			return true;
	}

	public static boolean inWater( Entity ent ) {

		int[] ret = Pifagor.getKOTRfromfXY(ent.x, ent.y);

		if ( getColor(ret[0], ret[1]) >= 900 ) 
			return true;
		else 
			return false;
	}
	//	public synchronized static void setWMap(MapChunk map) {
	//		bigmap = map;
	//	}
	//	public synchronized static MapChunk getWMap() {
	//		return	bigmap;
	//	}


	public static synchronized void removeEntityFromWorld(Entity entity) {
		deleteQue.add(entity);
	}
	public static synchronized void addEntitytoQWorld(Entity entity, boolean insOch) {
		addQue.add(new AddQoo(entity, insOch));
	}

	public static void addQuery() {
		if (addQue.size() > 1)
			System.out.println(" addqsize " + addQue.size());
		while (addQue.size() > 0 ) {
			AddQoo go = addQue.remove(0);
			addEntityToWorld(go.obj, go.insOch);
		}
	}

	public static void deleteQuery() {
		boolean dbo = false, reWall = false;
		if (deleteQue.size() > 1) {
			System.out.println("Deleting Query " + deleteQue.size() + " obj- " + map.objects.size());
			dbo = true;
		}

		for ( int a = 0; a < deleteQue.size(); a++ ) {
			Entity ento = deleteQue.get(a);

			map.objects.remove(ento);

			switch (ento.enttype) {
			case 8:
			case 0:
				reWall = true;
				break;

			case 1:
				animalz.remove(ento);
				//				System.out.println("  animal deleting a=" + a);
				continue;

			case 2:
				facs.remove(ento);
				continue;

			case 3:
			case 9:
				sledi.remove(ento);
				continue;
			}
			if (reWall) {
				int[] kotr = Pifagor.getKOTRfromfXY(ento.x, ento.y);
				addWall(kotr[0], kotr[1], 1);
			}
		}

		deleteQue.clear();



		if (dbo)
			System.out.println("Deleting Query end " + deleteQue.size() + " obj- " + map.objects.size());
		//		System.out.println("clearing del que");
	}
	public static synchronized int getNId() {
		return ++ids;
	}

	public void setCurCH(int i, int j) {
		currCHXY = new int[2];
		currCHXY[0] = i;
		currCHXY[1] = j;
//		Integer.parseInt(PropManager.prop.getProperty("cMapX"))
		PropManager.saveProperty("cMapX", Integer.toString(i));
		PropManager.saveProperty("cMapY", Integer.toString(j));
	}

	public static int getIds() {
		return ids;
	}

}
