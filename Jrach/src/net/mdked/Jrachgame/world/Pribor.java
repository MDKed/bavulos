package net.mdked.Jrachgame.world;



public class Pribor extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2853302099938824632L;

	private boolean isFueled,active;
	private byte fuelType;
	// fuel type = 0-goruchka
	private float fuelSumm;
	private float fuelSpend;
	public int ptype, timer;

	//скорость потребления

	/**
	 * type :
	 * 1- koster
	 * @param x
	 * @param y
	 * @param type
	 */
	public Pribor(float x, float y, byte type) {
		enttype = 8;
		switch (type) {
		case 1:
			ptype = type;
			this.x = x;
			this.y = y;
			isFueled = true;
			fuelType = 0;
			fuelSpend = 0.006f;
			fuelSumm = 150;
			active = true;
			break;
		}
	}

	public boolean burn(int delta) {
		timer += delta;
		if (fuelSumm > 0) {
			fuelSumm -= fuelSpend*delta;
			active = true;
//			System.out.println(timer);
			int timert = (int) (600+( Math.random()*200));
			if (timer >= timert) {
				timer = 0;
				emmitEff(0, 10);
				
			}
//			System.out.println(this + " - " + fuelSumm);
			return true;

		} else if (active && fuelSumm <= 0)  {
			//System.out.println("  костер погас");
			active = false;
			return false;
		}
		else return false;
	}
	public boolean isFueled() {
		return isFueled;
	}
	public boolean addFuel(byte fuelType, byte fuelQ) {
		if (fuelType == this.fuelType) {
			fuelSumm += 100 + (100*fuelQ);
			active = true;
			return true;
		} else return false;
	}
	public byte getFuel() {
		return fuelType;
	}
	public boolean active() {
		return active;
	}
}
