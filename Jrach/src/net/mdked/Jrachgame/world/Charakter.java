package net.mdked.Jrachgame.world;

import java.util.ArrayList;

import org.newdawn.slick.Animation;

import net.mdked.Jrachgame.core.CollisionMan;
import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.player.Buff;
import net.mdked.Jrachgame.player.Skill;
import net.mdked.Jrachgame.player.inv.Equipment;
import net.mdked.Jrachgame.player.inv.Inventory;
import net.mdked.Jrachgame.player.inv.Item;
import net.mdked.Jrachgame.player.inv.ItmContainer;
import net.mdked.Jrachgame.player.inv.Shmot;
import net.mdked.Jrachgame.player.inv.Weapon;
import net.mdked.Jrachgame.utils.DamageUtil;
import net.mdked.Jrachgame.utils.Pifagor;

public class Charakter extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1138145919378873307L;

	public float hp, speed, hpMax, hpStand, speedMax, rot, oldX, oldY, temp, hung, stamina,  tempRes,
	tempMax, staminaMax,staminaStand,  tempResMax, thirst,
	sprintMod = 0.3f,  oxygen, oxygenMax;

	public int strength, agility, vitality,
	maxStr,maxAgi,maxVit;

	public int actionType=0, weaponanim = 0, lastframe = 0;

	public boolean faceLeft, moving, attacking, nearFire, actionAnim, nearWshpA, 
	shotdone = true, dyin, dead, pullin, sprint, winded, makestep, inWater, sink, bot;

	public byte xhd, yhd;

	public DragblObj pull;

	public Entity target;

	public transient Animation[] corpusA, shirt, legs, boots, hands, hat, onehaxe, spear;

	public Inventory inventory;
	public Equipment equip;

	public ArrayList<Buff> buffs;

	public ArrayList<Skill> knownSkills;


	public boolean movgrn;

	public Charakter() {
		equip = new Equipment();
		knownSkills = new ArrayList<Skill>();
		buffs = new ArrayList<Buff>();
	}

	public void SpawnNpc(int var) {
		passable = true;

		width = 6;
		height = 28;

		strength = 10;
		maxStr = strength;
		agility = 10;
		maxAgi = agility;
		vitality = 10;
		maxVit = vitality;

		hp = 100;
		hpMax = hp;

		oxygen = 100;
		oxygenMax = oxygen;

		stamina = 100;
		staminaMax = stamina;
		hung = 100;
		thirst = 100;
		temp = 100;
		tempMax = temp;

		speed = 70f;
		speedMax = speed;
		tempRes = 20f;
		tempResMax = tempRes;

		inventory = new Inventory(strength);
		for (int a = 0; a < 5; a++) {
			Shmot wer = new Shmot(a,0,1,1);
			inventory.addItem(wer, 1);
			useItem(wer);
		}
		Weapon toporsk = new Weapon(0,7,1f);
		inventory.addItem(toporsk);
		useItem(toporsk);
		inventory.addItem(new Weapon(100,7,1f));
		
		giveSkill(0,0);
		giveSkill(1,0);

		DasGame.makeNPCAnim(this, var);
	}




	public void addBuff(Buff buff) {
		boolean dontAdd=false;
		//		System.out.println("  adding buff fm=" + buff.family );
		//проверка нет ли такого-же семейства или более сильного
		for (int a = 0; a < buffs.size(); a++) {
			if (buffs.get(a).family == buff.family && buff.force > buffs.get(a).force){
				removeBuff(a);
				break;
			}
			if (buffs.get(a).family == buff.family && buff.force == buffs.get(a).force) {
				//odinakovii buff
				if (buffs.get(a).isTimed)
					buffs.get(a).time += buff.time;
				dontAdd = true;
			}
		}
		if (!dontAdd) {
			buffs.add(buff);
			buff.active = true;

			if (buff.chStr) strength += buff.chSTR;
			if (buff.chVit) vitality += buff.chVIT;
			if (buff.chAgi) agility += buff.chAGI;

			if (buff.chHp) hpMax += buff.chHP;
			if (buff.chTempRes) tempRes += buff.chTR;
			if (buff.chSpeed) speed += buff.chSPEED;
			if (buff.chStam) staminaMax += buff.chSTAM;
		}
	}

	public void removeBuff(int num) {
		if (num >= 0) {
			Buff rbf = buffs.get(num);
			if (rbf == null) return;

			if (rbf.chStr) strength -= rbf.chSTR;
			if (rbf.chVit) vitality -= rbf.chVIT;
			if (rbf.chAgi) agility -= rbf.chAGI;

			if (rbf.chHp) hpMax -= rbf.chHP;
			if (rbf.chTempRes) tempRes -= rbf.chTR;
			if (rbf.chSpeed) speed -= rbf.chSPEED;
			if (rbf.chStam) staminaMax -= rbf.chSTAM;
			buffs.remove(num);
		}
	}

	public int getBfNum(Buff bf) {
		for (int a = 0; a < buffs.size(); a++) {
			if (buffs.get(a).family == bf.family && bf.force == buffs.get(a).force) {
				return a;
			}
		}
		return -1;
	}

	public void checkBuffs(int delta) {
		for (int a = 0; a < buffs.size(); a++) {
			Buff bfs = buffs.get(a);
			if (bfs.active) {
				if (bfs.isTimed) {
					bfs.time -= delta;
					if (bfs.time <= 0 ) {
						removeBuff(a);
						break;
					}
				}

				//hp
				if (bfs.dotHp) {
					if (bfs.bleed) bleed();
					hp += (bfs.dotHP/1000)*delta;
				}
				if (hp > hpMax) hp = hpMax;

				//голод
				if (bfs.dotHung) hung += (bfs.dotHNG/1000)*delta;

				//стамина
				if (bfs.dotStam) stamina += (bfs.dotSTAM/1000)*delta;

				//воздух
				if (bfs.dotOxy) oxygen += (bfs.dotOXY/1000)*delta;


			}
		}

		//cancel
		if (hung <= 100) removeBuff(getBfNum(new Buff(6,0, this)));
		if (hung > 10) removeBuff(getBfNum(new Buff(2,0, this)));

		if (temp > 20 ) {
			removeBuff(getBfNum(new Buff(1,0, this)));
			if (temp < 40)
				addBuff(new Buff(0,0, this));
		}
		if (temp >= 40) removeBuff(getBfNum(new Buff(0,0, this)));

		if (winded && stamina >= 90) {
			removeBuff(getBfNum(new Buff(5,0, this)));
			winded = false;
		}

		if (oxygen > 0) removeBuff(getBfNum(new Buff(8,0, this)));
	}

	public Skill getSkill(int type) {
		for (int a=0; a < knownSkills.size(); a++)
			if (knownSkills.get(a).type == type) 
				return knownSkills.get(a);
		return null;

	}

/**
 * 0 - swim
 * 1 - herb
 * @param type
 * @param lvl
 */
	public void giveSkill( int type, int lvl ) {
		if ( getSkill(type) == null ) 
			knownSkills.add(new Skill(type, lvl));
	}


	public void inWater() {
		inWater = World.inWater(this);

		if (inWater) {
			Skill plav = getSkill(0);
			addBuff(new Buff(9, 0, this));

			//poka ne vkachan skill = tonet
			if ( plav.lvl < 1 || stamina < 10 ) {
				addBuff(new Buff(7, 0, this));
				sink = true;
			} else {
				removeBuff(getBfNum(new Buff(7, 0, this)));
				sink = false;
			}

		} else {
			removeBuff(getBfNum(new Buff(7, 0, this)));
			removeBuff(getBfNum(new Buff(9, 0, this)));
			sink = false;
		}
	}

	/**
	 * 0- swing
	 * 1 - throw
	 * 2 - melee hit
	 * 3 - building light
	 * 4 - building med
	 * 5 - building high
	 * @param typeOfAct
	 */
	public boolean playerAction(int typeOfAct) {
		int swngprice = 2,
				thrwprice = 10,
				meleeprice = 5,
				blightpr = 30,
				bmedprc = 50,
				bheavyprc = 80;
		switch (typeOfAct) {
		case 0:
			//swing

			if (stamina - swngprice >= 0) {
				stamina -= swngprice;
				return true;
			} else return false;

		case 1:
			//throw

			if (stamina - thrwprice >= 0) {
				stamina -= thrwprice;
				return true;
			} else return false;

		case 2:
			//melee hit
			if (stamina - meleeprice >= 0) {
				stamina -= meleeprice;
				return true;
			} else return false;

		case 3:
			//building light
			if (stamina - blightpr >= 0) {
				stamina -= blightpr;
				return true;
			} else return false;

		case 4:
			//b med
			if (stamina - bmedprc >= 0) {
				stamina -= bmedprc;
				return true;
			} else return false;

		case 5:
			//b heavy
			if (stamina - bheavyprc >= 0) {
				stamina -= bheavyprc;
				return true;
			} else return false;
		}
		return false;
	}

	public void dealDamage() {
		//				System.out.println(" dd " + target + " dist = " + Pifagor.getDist(this, target));
		System.out.println( " target is " + target.id + " type " + target.enttype);
		if (target instanceof Charakter &&

				Pifagor.getDist(x, target.x, y, target.y) <= 50 ) {

			DasGame.playSound(15, this);
			Charakter tgt = (Charakter) target;
			Weapon wpn = (Weapon) equip.getWrdShmot(Equipment.WEAPON);
			wpn.mastr = this;
			DamageUtil.dealDamage(wpn, tgt);

			//System.out.println(" igrok nanes dmg");
		}
		Weapon wpn = (Weapon) equip.getWrdShmot(Equipment.WEAPON);
		if ( wpn.tool && target instanceof Minable && 
				Pifagor.getDist(x, target.x, y, target.y) <= 25) {
			DasGame.playSound(16, this);
			Minable min = (Minable) target;

			min.damage(wpn.damage, (byte) wpn.toolType);
		}
	}

	private void bleed() {
		if (Math.random() <= 0.1d) {
			byte kind = (byte) (3 + (byte) (Math.random() * 2));
			DasGame.makeSled(x, y, kind, 0, false);
		}
	}
	//	public Rectangle getBounds() {
	//		return new Rectangle(x-3, y-3, (float) 6, (float) 6);
	//	}
	public void pullAnimal(DragblObj drag) {
		if (!pullin && Pifagor.getDist(x, drag.x, y, drag.y) < 40) {
			pull = drag;
			pullin = true;
			drag.startPull(this);
			speed -= speedMax/2;
		} else if (pullin) {
			stopPullin();
			pull = drag;
			pullin = true;
			drag.startPull(this);
			speed -= speedMax/2;
		}
	}
	public void stopPullin() {
		if (pullin) {
			pullin = false;
			pull.stopPull();
			pull = null;
			speed += speedMax/2;
		}
	}


	public void useItem(int num) {
		//		System.out.println(" useitm");
		Item usI = inventory.getItem(num);
		if (usI != null) {
			usedaItem(usI);
		}
	}

	public void collect(Entity col) {
		switch (col.enttype) {

		case 0:
			Minable cole = (Minable) col;
			switch (cole.mtype) {
			case 5:
				World.removeEntityFromWorld(col);
				Item colItem = new Item(400,1);
				colItem.setQuality(useSkill(1));
				inventory.addItem(colItem);
				break;

			}
			break;

		}
	}

	public float useSkill(int skill) {
		for (int a = 0; a < knownSkills.size(); a++) 
			if (knownSkills.get(a).type == skill) {
				knownSkills.get(a).kachXP(1000, 10);
				return knownSkills.get(a).qXifier;
			}

		return 0;

	}

	public void useItem(Item itm) {
		if ( itm != null && inventory.isIteminInv(itm) && itm.getUsAct() >= 0) 
			usedaItem(itm);
	}
	public void usedaItem(Item itm) {
		if (itm == null) return;
		//		System.out.println(" trying to use item " + itm.getType());


		switch (itm.getUsAct()) {
		case 0:
			hung += 10 + (itm.getItmQ()*5);
			inventory.removeItem(itm, 1);
			break;
		case 1:
			//wear
			equip.wear(itm);
			break;

		case 2:
			//			System.out.println("Dring");
			thirst += 20 + (itm.getItmQ()*10);

			inventory.removeItem(itm, 1);
			break;

		case 3:
			ItmContainer cont = (ItmContainer) itm;
			usedaItem(cont.itm);
			break;

		case 4:
			//healing
			Buff heal = new Buff(11,10, this);
			heal.dotHP *= itm.getItmQ();

			//			System.out.println(" Healing for " + heal.dotHP + " ");
			this.addBuff(heal);
			inventory.removeItem(itm, 1);
			break;
		}

	}

	public void useInHands() {
		if (equip.weapon.slot != null) {

			Weapon wpn = (Weapon) equip.weapon.slot;
			switch (wpn.wtype) {
			case 1:
				//melee
				startAction(1);
				break;
			case 2:
				startAction(2);
				break;
			}
		}
	}

	public void makeAction(int actType) {

		switch (actType) {
		case 1:
			if (!bot && playerAction(2))
				dealDamage();
			else if (bot) {
				dealDamage();
				System.out.println( " damage!");
			}

			break;

		case 2:
			if (playerAction(1))
				throwInHand();
			break;
		case 3:
			//death
			System.out.println(" DEAD XXXXXXX");
			World.createDragAble(this, x, y);
			World.removeEntityFromWorld(this);

			break;
		}
	}


	private void throwInHand() {
		Weapon wpn = (Weapon) equip.weapon.slot;
		wpn.setMaster(this);
		World.createMissile(wpn, this);
		removeItem(wpn);


	}

	public void removeItem(Item itm) {
		inventory.removeItem(itm, 1);
		if (itm.nAvailable) 
			equip.remove(itm);

	}

	public void endAction( int actType ) {
		if (actionType != actType) return;
		//		System.out.println("Ending action " + actionType);
		actionAnim = false;
		startAction(0);
		//		switch(actionType) {
		//		case -1:
		//			startAction(0);
		//			break;
		//		case 1:
		//
		//			startAction(0);
		//			break;
		//		case 2:
		//			//			World.createMissile((Weapon) equip.weapon.slot, this);
		//			startAction(0);
		//			break;
		//			
		//		}


	}

	/**
	 * actType
	 * -1 = move;
	 * 0 = idle;
	 *  1 = melee;
	 * 2 = throw;
	 * 3 = die;
	 * @param actType
	 * @return
	 */
	public boolean startAction(int actType) {
		if (actType == actionType) return true;

		//		System.out.println(" starting action " + actType + " " + this + " actanim = " + actionAnim + " acttype = " + actionType);

		if (!actionAnim || actionType == 0) {
			if (!bot && actType == 1 && !playerAction(0)  ) return false;


			//			System.out.println("  act type=" + actType);
			actionType = actType;
			actionAnim = true;
			return true;
		} 
		//		if (force) {
		//			actionType = actType;
		//		}
		//		if (actType == 1) {
		//			actionType = actType;
		//		}
		return false;
	}

	public void beginSprint() {
		if (!sprint && !winded) {
			speed += speedMax*sprintMod;
			sprint = true;
		}
	}
	public void stopSprint() {
		if (sprint) {
			speed -= speedMax*sprintMod;
			sprint = false;
		}
	}

	public void addhd(int heading) {
		switch (heading) {
		case 0:
			yhd = -1;
			break;

		case 1:
			xhd = 1;
			faceLeft = false;
			break;

		case 2:
			yhd = 1;
			break;

		case 3:
			xhd = -1;
			faceLeft = true;
			break;
		}
		movgrn = true;
	}

	private void obnulhd() {
		yhd =0;
		xhd =0;
		movgrn = false;
	}

	/**
	 * 0-up
	 * 1-right
	 * 2-down
	 * 3-left
	 * @param heading
	 */
	public boolean move(int delta) {
		boolean ret = false;
		float sprintCostPS = 15;
		oldX = x;
		oldY = y;
//				System.out.println( this + " action = " + actionType);
		if (movgrn) {

			if (!bot) rot = (float) Pifagor.getDirectionTo(x, y, x+xhd, y+yhd);
			Pifagor.moveByRot(this, delta);

			if (!CollisionMan.checkCollison(this) && startAction(-1)) {
				ret = true;

				//				System.out.println("zazazaza " + actionType);
				//				if (bot)
				//					System.out.println(" move-move " + movgrn + " xy = " + x + "x" + y);
				//				startAction(-1);
				moving = true;
				if (pullin) pull.pull(delta);
				if (sprint && stamina > 20) {
					//						System.out.println("  Igrok bezhit otnimaem staminu - " +(sprintCostPS/1000)*delta);
					stamina -= (sprintCostPS/1000)*delta;
				} 
				if (sprint && stamina < 20) {
					addBuff(new Buff(5,0, this));
					winded = true;
					stopSprint();
				}

			} 
			else {
				x = oldX;
				y = oldY;
				ret = false;
			}
			obnulhd();
		} else endAction(-1);
		return ret;

	}

	public void drawAnim(int cdx, int cdy) {
		int head = DasGame.getADir(this);
//				System.out.println(" head " + head);
		boolean mrr = faceLeft;
		if (enttype == 1 && head == DasGame.RIGHT ||enttype == 12 && head == DasGame.LEFT ) mrr = true;
//		System.out.println(" head 2 = " + head);
		DasGame.drawCntrd(getAnim(head, true, corpusA), cdx, cdy, mrr, DasGame.xScale);

		int frame = getAnim(head, true, corpusA).getFrame();
		if (enttype == 6 || enttype == 12) {
			
//			if (enttype == 12) System.out.println("NPC DRAW speed = " + speed);
			
			if (equip.getWrdShmot(1) != null)
				DasGame.drawCntrd(getAnim(head, false, shirt).getImage(frame), cdx, cdy, mrr, DasGame.xScale);
			if (equip.getWrdShmot(3) != null)
				DasGame.drawCntrd(getAnim(head, false, legs).getImage(frame), cdx, cdy, mrr, DasGame.xScale);
			if (equip.getWrdShmot(4) != null)
				DasGame.drawCntrd(getAnim(head, false, boots).getImage(frame), cdx, cdy, mrr, DasGame.xScale);
			if (equip.getWrdShmot(2) != null)
				DasGame.drawCntrd(getAnim(head, false, hands).getImage(frame), cdx, cdy, mrr, DasGame.xScale);
			if (equip.getWrdShmot(0) != null)
				DasGame.drawCntrd(getAnim(head, false, hat).getImage(frame), cdx, cdy, mrr, DasGame.xScale);
			if (equip.getWrdShmot(5) != null) {
				Weapon wpn = (Weapon) equip.getWrdShmot(5);
				switch(wpn.drawId) {
				case 0:
					DasGame.drawCntrd(getAnim(head, false, onehaxe).getImage(frame), cdx, cdy, mrr, DasGame.xScale);
					break;

				case 100:
					DasGame.drawCntrd(getAnim(head, false, spear).getImage(frame), cdx, cdy, mrr, DasGame.xScale);
					break;

				}
				if (actionType >= 1  && lastframe != frame) {
					lastframe = frame;
					if (weaponanim == 2) {
						makeAction(actionType);
						if (actionType == 2) 
							endAnim();
					}

					weaponanim ++;
					//					System.out.println(weaponanim);

				}

			}
		} else {
			//NPC
			if ( actionType != 3 && actionType >= 1  && lastframe != frame ) {
				lastframe = frame;
				if (weaponanim == 2) {
//					System.out.println( " frame 2");
					makeAction(actionType);
					if (actionType == 2) 
						endAnim();
				}
				weaponanim ++;



				//				System.out.println(weaponanim);

			}
			//TODO ANIMATION OF DEATH && weaponanim >= 1 
			if (actionType == 3 ) {
				makeAction(actionType);
				endAction(3);

			}
		}
		if (actionType >= 1) {

			if ( weaponanim >= 5) {
				//				System.out.println( "frame 6>");
				endAnim();
			}
		}


	}
	private void endAnim() {
		int head = DasGame.getADir(this);
		weaponanim = 0;
		lastframe = 0;
		getAnim(head, true, corpusA).restart();
		endAction(actionType);
	}

	public void checkAlive() {
		if (actionType != 3 && hp <= 0) {
			endAction(actionType);
			startAction(3);
		}
	}

	public void playerTurn(int delta) {
		//		System.out.println(" PTurn " + this);
		inWater();
		checkBuffs(delta);

		//		if ( temp <= tempMax) 
		//			temp += (((World.temperature + tempRes)/40)/1000)*delta;
		//System.out.println("WT: " + World.temperature + " Player TR: " + playerC.tempRes + " = " +  (World.temperature + playerC.tempRes)/(delta*40));
		//System.out.println("player temp : " + playerC.temp + " delta: " + delta);


		if (hp > hpMax) hp = hpMax;

		if ( stamina <= staminaMax && !inWater ) {
			float plus = (hung/10)/1000*delta;
			//			if (plus < 0) System.out.println("woopseee");
			stamina += plus;
		}
		//		System.out.println("  temp " + this);

		if (temp > tempMax) temp = tempMax;
		if (temp < 0) temp = 0;
		if (temp >= 20 && temp < 40) addBuff(new Buff(0,0, this));
		else if (temp < 20) addBuff(new Buff(1,0, this));

		//		System.out.println("  stam " + this);


		if (stamina > staminaMax) stamina = staminaMax;
		if (stamina < 0) stamina = 0;

		if (enttype != 1) {

			//			System.out.println("  hung " + this);


			if (hung > 0) hung -= (0.15f/1000)*delta;
			if (hung > 200) hung = 200;
			if (hung < 0) hung = 0;
			if (hung < 10) addBuff(new Buff(2,0, this));
			if (hung > 100) addBuff(new Buff(6,0, this));

			if (thirst > 0) thirst -= (0.077f/1000)*delta;
			if (thirst > 100) thirst = 100;
			if (thirst < 0) thirst = 0;
			if (thirst < 10) addBuff(new Buff(10,0, this));
		}

		//water
		//		System.out.println("  water " + this);

		if (!sink) oxygen += (10f/1000f)*delta;
		if (inWater) {
			getSkill(0).kachXP(delta, 30);
		}

		//		System.out.println("  oxy " + this + " xx" +oxygen );

		if (oxygen > oxygenMax) oxygen = oxygenMax;
		if (oxygen < 0) oxygen = 0;
		if ( oxygen <= 0 ) addBuff(new Buff(8,0, this));

		if (speed <= speedMax/10) speed = speedMax/10;

	}

	private Animation getAnim(int head, boolean plr, Animation[] anim) {

		if ( plr && inWater && oxygen <= 0) 
			return anim[7];

		switch (actionType) {

		case -1:
			//moving
			if (!inWater)
				switch (head) {
				case 0:
					return anim[4];
				case 1:
				case 3:
					return anim[3];
				case 2:
					return anim[1];

				}
			else 
				switch (head) {
				case 0:
					return anim[9];
				case 1:
				case 3:
					return anim[8];
				case 2:
					return anim[6];
				}
			break;

		case 0:
			//idle
			//			System.out.println(" idle");
			if (!inWater)
				switch (head) {
				case 0:
					return anim[5];
				case 1:
				case 3:
					return anim[2];
				case 2:
					return anim[0];
				}
			else 
				switch (head) {
				case 0:
					return anim[9];
				case 1:
				case 3:
					return anim[8];
				case 2:
					return anim[6];
				}
			break;

		case 1:
		case 2:
			//mellee
			//			System.out.println(" melee");
			if (!inWater)
				switch (head) {
				case 0:
					return anim[12];
				case 1:
				case 3:
					return anim[11];
				case 2:
					return anim[10];
				}
			else 
				switch (head) {
				case 0:
					return anim[9];
				case 1:
				case 3:
					return anim[8];
				case 2:
					return anim[6];
				}
			break;
		case 3:
			//death
			return anim[13];

		}

		return anim[0];

	}




	public void makeSound(int soundType) {
		int var = 0;

		switch (enttype) {
		case 1:
			//animal
			var = 100;
			Mobster mobe = (Mobster) this;
			var += mobe.type;
			break;
		case 6:
			var = 200;
			break;
		}


		switch (soundType) {
		//damage
		case 1:
			switch (var) {
			case 100:
				DasGame.playSound(9 ,this);
				break;
			case 101:
				DasGame.playSound(13, this);
				break;
				//player
			case 200:
				DasGame.playSound(17, this);
				break;
			}
			break;
		}

	}
}
