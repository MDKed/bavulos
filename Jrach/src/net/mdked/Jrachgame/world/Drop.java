package net.mdked.Jrachgame.world;

import net.mdked.Jrachgame.player.inv.Inventory;

public class Drop extends Entity {
	
	public int ico;
	
	public Inventory inv;
	
	public Drop(float x, float y, int icon) {
		enttype = 10;
		passable = true;
		
		this.x = x;
		this.y = y;
		ico = icon;
		
		inv = new Inventory(100000000);
	}

}
