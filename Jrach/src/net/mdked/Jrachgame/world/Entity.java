package net.mdked.Jrachgame.world;

import java.io.Serializable;

import net.mdked.Jrachgame.world.efcs.Sprtefct;
/**
 * 0- static
 * 1- animal
 * 2 - fac
 * 3 - sled
 * 4 - mousepoint
 * 5 - missile
 * 6 - player
 * 7 - drgble
 * 8 - pribor
 * 9 - spfx
 * 10 - drop
 * 11 - wall;
 * 12 - npc
 *
 * width - /2 shirini
 * @author Paulus
 *
 */
public class Entity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -39812041012614981L;
	public float x,y,z;
	public int width, height;
	public byte enttype;
	public boolean passable, sveto;
	public int id;
	
	public Entity() {
		id = World.getNId();
	}
	
	public void emmitEff(int type, int z) {
		World.addEntityToWorld(new Sprtefct(x - 5, y, this.z + z,  type,  15, 15, 0), false);
	}
	public void emmitText(String text, float z, int color) {
		Sprtefct textef = new Sprtefct(x - 5, y, this.z + z, 1,  15, 15, color);
		textef.text = text;
		World.addEntityToWorld(textef , false);
	}
}
