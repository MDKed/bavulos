package net.mdked.Jrachgame.world.mapgen;

import grid.GridMap;

import org.newdawn.slick.Color;

import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.utils.FileManager;
import net.mdked.Jrachgame.utils.Pifagor;
import net.mdked.Jrachgame.world.World;

public class MapGen {

	private MapChunk map;

	int mapSize;


	//	public static Double getMdHtFromArch( int chX, int chY) {
	//		//		System.out.println(" arh heyt");
	////		int[] arhXY = Pifagor.getXchYch(chX, chY, true);
	////
	////		MapChunk bigmap =  getBigMap(arhXY[0], arhXY[1]);
	//
	//		int[] chnk = Pifagor.getXYminCHfd(chX, chY, true);
	//
	//		return World.getWMap().heightMap[chnk[0]][chnk[1]];
	//	}
	//	public static boolean getMPRDYFromArh( int chX, int chY ) {
	//		int[] arhXY = Pifagor.getXchYch(chX, chY, true);
	//		MapChunk bigmap =  getBigMap(arhXY[0], arhXY[1]);
	//		int[] chnk = Pifagor.getXYminCHfd(chX, chY, true);
	//
	//		return bigmap.gotMap(chnk[0], chnk[1]);
	//	}
	//	
	//	public static void setMPRDY( int chX, int chY, boolean bol) {
	//		int[] arhXY = Pifagor.getXchYch(chX, chY, true);
	//		MapChunk bigmap =  getBigMap(arhXY[0], arhXY[1]);
	//		int[] chnk = Pifagor.getXYminCHfd(chX, chY, true);
	//
	//		bigmap.setgotMap(chnk[0], chnk[1], bol);
	//	}

//	public static MapChunk getChunkXY(int x, int y) {
//		int[] xyCh = Pifagor.getXchYch(x, y);
//		return getChunk(xyCh[0], xyCh[1]);
//	}

//	public static MapChunk getChunk(int xCh, int yCh) {
//		MapChunk targetCh = checkCache(xCh, yCh);
//
//		if ( targetCh == null ) {
//			//			targetCh = (MapChunk) FileManager.loadObject(xCh, yCh, 1);
//			DasGame.mapMan.initLoad(xCh, yCh, 1);
//		}
//		return targetCh;
//	}

	//	public synchronized static MapChunk getBigMap(int xCh, int yCh) {
	//		//		System.out.println(" get big map");
	//
	//		MapChunk map = checkCacheBM(xCh, yCh);
	//		if ( map == null ) map = (MapChunk) FileManager.loadObject(xCh, yCh, 2);
	//		return map;
	//	}


	//	private static MapChunk checkCacheBM(int xCh, int yCh) {
	//		//		System.out.println(" check bigmap cache");
	//
	//		for ( int a = 0; a < World.bigmapCache.size(); a++ ) {
	//			if (World.bigmapCache.get(a).xCh == xCh && yCh == World.bigmapCache.get(a).yCh ) 
	//				return World.bigmapCache.get(a);
	//		}
	//		return null;
	//	}

	public static Double byteToDouble(byte height) {
		Double one = (height + 127.0);
		return one / 256.0;
	}
	public static byte doubleToByte(Double height) {
		return (byte) ((height * 256) - 127);
	}

	public  Double getHeight(int x, int y ) {
		if (map.got[x][y])
			return  byteToDouble( map.heightMap[x][y] );
		else 
			return null;
	}

	private Double setHeight( int x, int y, Double val) {
		//		MapChunk targetCh = getChunkXY(x, y);
		//		int xy[] = Pifagor.getXYminCH(x, y);
		map.heightMap[x][y] = doubleToByte(val);
		map.got[x][y] = true;
		//		System.out.println("op op " + val);
		return val;
	}

//	public synchronized static MapChunk checkCache( int xCh, int yCh ) {
//		for ( int a = 0; a < World.mapCache.size(); a++ ) {
//			if (World.mapCache.get(a) != null)
//				if (World.mapCache.get(a).xCh == xCh && yCh == World.mapCache.get(a).yCh ) 
//					return World.mapCache.get(a);
//		}
//		return null;
//	}
	//, boolean bMap
	public MapGen(int xCh, int yCh) {
		System.out.println(" starting gen");
		//		long starttiem= System.currentTimeMillis();

		//		if ( !bMap ) {
		map = 
				//					new SimpleMapChunkGen(xCh, yCh).map;, bMap
				new MapChunk(xCh, yCh);

		mapSize = World.chunksize;

		for ( int x = 0; x < World.chunksize; x++ ) {
			for (int y = 0; y < World.chunksize; y++ ) {
				map.heightMap[x][y] = doubleToByte(val(x, y ));
				//					System.out.println( genChunk.formed + " xx" + x + 'x' + y + val(x, y));
			}

		}
		colorDatChnk();
		map.setFormed(true);
		System.out.println(" starting Ogen");
		new ObjChGen(xCh, yCh, map);
		
		


		//		}
		//		else 
		//		{
		//			//			System.out.println("  big map");
		//
		//			//			map = getBigMap(xCh, yCh);
		//			map = new MapChunk(xCh, yCh, bMap);
		//			mapSize = World.arhipelago;
		//
		//			//			if ( map.formed ) return ;
		//			//			else {
		//			//				System.out.println(" start value");
		//
		//			for ( int x = 0; x < World.arhipelago; x++ ) {
		//				for (int y = 0; y < World.arhipelago; y++ ) {
		//					//						System.out.println(" big " + x + "x" + y);
		//
		//					map.heightMap[x][y] = val(x, y );
		//				}
		//
		//			}
		//			map.setFormed(true);
		//			World.setWMap(map);
		//
		//			//			}
		//
		//		}

		map.heightMap = null;
		FileManager.SaveObject(map);
		World.setMap(map);
		//		World.insertObjFromChunkToWorld(map);
//		DasGame.mapMan.removeOrder(xCh, yCh);
		//		long tiem =System.currentTimeMillis()-starttiem;
		//		System.out.println(" mapgen time = " + tiem);
	}


	private  double val(int x,int y,double v) {

		double znach = Math.max(0, Math.min(1.0, v));

		return setHeight(x, y, znach);



	}

	public double val(int x,int y) {
		//				System.out.println(" val " + x + "x" + y);

		if (x <= 0 || x >= mapSize || y <= 0 || y >= mapSize) {
			//			if (!map.big) {
			//				int bX = x + (map.xCh*World.chunksize);
			//				int bY =  y + (map.yCh*World.chunksize);
			//				//нужно определять какой чанк граничит, и брать его высоту из архипелага
			//				int[] xyCH = Pifagor.getXchYch(bX, bY, false);
			//				//				return getMdHtFromArch(xyCH[0], xyCH[1]);
			//
			//				//							int[] xy = Pifagor.getXchYch(x, y, false);
			//				//				if (getMPRDYFromArh(xyCH[0], xyCH[1])) {
			//				//					int[] xx = Pifagor.getXYminCHfd(bX, bY, false);
			//				//					MapChunk tCH = getChunk(xyCH[0], xyCH[1]);
			//				//					double res = tCH.heightMap[xx[0]][xx[1]];
			//				//					
			//				////					System.out.println(" map chunk was gend " + xyCH[0] + "z" + xyCH[1] + xx[0] + "x" + xx[1] + " " + res);
			//				//					return res;
			//				//				} else
			//				return (getMdHtFromArch(xyCH[0], xyCH[1])+ map.midHt)/2;

			//								//хуйня
			//												return tCH.midHt;
			//				
			//							int[] xyz = Pifagor.getXYminCH(x, y, false);
			//				
			//							return tCH.heightMap[xyz[0]][xyz[1]];
			//			} 
			//			else 
			return 0d;
		}
		//		System.out.println("get ht");

		Double getHgt = getHeight(x, y);
		//		System.out.println( "after ht ");

		if ( getHgt != null ) {
			return getHgt;
		}
		//				System.out.println( "after ret ");

		int base = 1;

		while (((x & base) == 0) && ((y & base) == 0))
			base <<= 1;

		if (((x & base) != 0) && ((y & base) != 0))
			squareStep(x, y, base);
		else
			diamondStep(x, y, base);

		//		System.out.println(" return");

		return getHeight(x, y);


	}
	void squareStep(int x, int y,int blockSize) {
		//		System.out.println(" sqare");

		if (getHeight(x, y) == null) {
			val(x, y,
					displace((val(x - blockSize, y - blockSize) +
							val(x + blockSize, y - blockSize) +
							val(x - blockSize, y + blockSize) +
							val(x + blockSize, y + blockSize)) / 4, blockSize, x, y));
		}

	}
	void diamondStep(int x,int y,int blockSize) {
		//		System.out.println(" diamonds");

		if ( getHeight(x, y) == null) {
			val(x, y,
					displace((val(x - blockSize, y) +
							val(x + blockSize, y) +
							val(x, y - blockSize) +
							val(x, y + blockSize)) / 4, blockSize, x, y));
		}

	}
	public static double randFromPair(int x,int y) {
		double xm7 = 0, xm13= 0, xm1301081= 0, ym8461= 0, ym105467= 0, ym105943= 0;
		for (int i = 0; i < 32; i++) {
			xm7 = x % 7;
			xm13 = x % 13;
			xm1301081 = x % 1301081;
			ym8461 = y % 8461;
			ym105467 = y % 105467;
			ym105943 = y % 105943;

			y = (int) (x + World.seed);
			x += (xm7 + xm13 + xm1301081 + ym8461 + ym105467 + ym105943);
		}

		return (xm7 + xm13 + xm1301081 + ym8461 + ym105467 + ym105943) / 1520972.0;
	}
	double displace(double v,int blockSize,int x,int y) {
		int sizeX = World.chunksize;
		//last World.roughness - 0.5 map.big ? World.arhipelago : World.arhipelago; //
		//		return (v + (randFromPair(x+map.xCh, y+map.yCh) - 0.5) * blockSize * 2 / mapSize * rofna);

		//		float rofna = map.big ? World.roughness : 0.5f ;
		//		double randomx = map.big ? randFromPair(x+map.xCh, y+map.yCh) - 0.5 : (randFromPair(x+map.xCh, y+map.yCh) - 0.5) * 0.5;
		return (v + (randFromPair(x+map.xCh, y+map.yCh) - 0.5) * blockSize * 2 / sizeX * World.roughness );
	}

	//	public Pixmap getMinimap() {
	//
	//		int minisize = GameData.getInstance().minimapSize;
	//
	//		Pixmap minimap = new Pixmap(minisize,minisize,Format.RGBA8888);
	//
	//		int zx= GameData.getInstance().mapSize / minisize;
	//		for (int x = 0; x < minisize; x++) {
	//			for (int y = 0; y < minisize; y++) {
	//				float val = (float) val(x*zx,y*zx);
	//				//minimap.drawPixel(x, y, getMapColor(x*zx, y*zx));
	//				int clr;
	//				if (val <= waterlevel) 
	//					//					clr = Color.rgba8888(0f, 0f, val + 0.5f, 1);
	//					clr = Color.rgba8888(getColor((short) 900));
	//				else if (val > waterlevel && val <= 0.3f ) 
	//					clr = Color.rgba8888(getColor((short) 201));
	//				else if (val > 0.3f && val <= 0.4f ) 
	//					clr = Color.rgba8888(getColor((short) 602));
	//				else if (val > 0.4f && val <= 0.6f)
	//					clr = Color.rgba8888(getColor((short) 303));
	//				else if (val > 0.6f && val <= 0.7f) 
	//					clr = Color.rgba8888(getColor((short) 402));
	//				else
	//					//					clr =  Color.rgba8888(1f, 1f, 1f, val);
	//					clr = Color.rgba8888(getColor((short) 604));
	//
	//				minimap.drawPixel(x, minisize-y -1, clr);
	//
	//			}
	//		}
	//
	//		return minimap;
	//	}

	//	public Color getMapColor(int x, int y, MapChunk mapChunk) {
	//		//		if (x < 0 || y < 0 || x >= GameData.getInstance().mapSize ||  y >= GameData.getInstance().mapSize ) 
	//		//			return Color.rgba8888(getColor((short) 900));
	//		int[] xy = Pifagor.getXYminCH(x, y, false);
	//		return getColor(mapChunk.getColorDat(xy[0], xy[1]));
	//
	//	}



	public static short getChnkBiome( int chX, int chY ) {

		//		Double height = randFromPair(chX, chY);
		//				getMdHtFromArch(chX, chY);

		//		if (height <= World.waterlevel) return 900;

		int temperature = getTemperature(chY);

		int vlaznost = getHumidity(chX);

		switch (temperature) {
		//centre
		case 2:
			switch (vlaznost) {
			case 2:
				//jungle
				return 100;
			case 1:
				//ymerennost
				return 200;
			case 0:
				//pust
				return 300;
			}
		case 1:
			//ymerennost
			return 200;
		case 0:
			//taiga\sneg
			return 400;
		}

		//		int a = (int) ((height - World.waterlevel) / 0.2d);
		//		if (a > 3) a = 3;
		//		short biome = (short) ((vlaznost*100) + a+1);
		//				System.out.println( " Возвращаем биом " + biome );
		//		return setChunkBiome(chX, chY, biome);
		return 400;


	}

	public static int getTemperature(int cHy) {
		//зависимость от дальности экватора и близости к полюсам
		//3 температуры 
		//0 - жунгли умерено пустыня
		//1 - умеренно
		//2 - холодно
		//		int hum = (int) (6 - ( (  Math.abs(cHy) / World.worldHeight ) * 6 ));
		int temp = Math.max(0, 2- Math.abs(cHy)); 
		return temp;
	}
	public static int getHumidity(int cHx) {
		//j-y-p-y
		int hum = Math.abs(cHx) % 4 ;
		if (hum > 2) {
			hum %=2;
		}
		return 2-hum;

	}

	//	private static short setChunkBiome(int chX, int chY, short s) {
	//		MapChunk map = getChunk(chX, chY);
	//		map.biome = s;
	//		return s;
	//	}

	private void makeRiva(int x, int y) {
		int nx = x, ny = y, nnx = nx, nny = ny, mnx = nx, mny = ny, ox=x, oy=y;
		do {
			for (int a = -1; a < 2; a++) {
				for (int b = -1; b < 2; b++) {
					if ( a==-1 && b== -1 ) continue;
					if ( a==1 && b== -1 ) continue;
					if ( a==-1 && b== 1 ) continue;
					if ( a==1 && b== 1 ) continue;

					int sx = nx+a, sy =ny+b;
					if( sx <= 0 || sx >= World.chunksize || sy <=0 || sy >= World.chunksize) continue;

					if (val(nx, ny) > World.waterlevel) {
						map.setColorDat(sx, sy, makeRandomBiome(1000 , nx+a, ny+b));
						map.gridMap.set(sx, sy, -1);
					}
				}
			}


			float min = 1000;
			for (int xx = -1; xx < 2; xx++) {
				for (int yy = -1; yy < 2; yy++) {
					if (xx == 0 && yy == 0) continue;

					nnx = nx+xx;
					nny = ny+yy;
					if (val(nnx, nny) <= min) 	{
						mnx = nnx;		
						mny = nny;
						min = (float) val(nnx, nny);
					}

				}
			}
			if (mnx == ox && mny == oy) {
				//				System.out.println( "river gen OLD");

				break;
			}
			ox = nx;
			oy = ny;
			nx = mnx;
			ny = mny;
			//			System.out.println( "river gen " + nx + "x" + ny + " o=" + ox +"x"+ oy + " m=" + mnx + "x" + mny);

		}
		while 
			( val(nx, ny	) > World.waterlevel );



	}

	public void colorDatChnk() {
		int chX = map.xCh;
		int chY = map.yCh;

		int chSize = World.chunksize;
		//		int startChuX = chX-1;
		//		int startChuY = chY-1;

		for (int x=0; x < chSize; x++) {
			for (int y=0; y < chSize; y++) {


				int trX = (chX*chSize) + x,
						trY = (chY*chSize) + y;

				if ( map.getColorDat(x, y) >= 1000 ) continue;

				if (val(x, y) <= World.waterlevel ) {
					map.setColorDat(x, y, makeRandomBiome(900 , x, y));
					map.gridMap.set(x, y, GridMap.WALL);
					//					System.out.println("вода");
					continue;
				}

				int a = (int) ((val(x,y) - World.waterlevel) / 0.2d);
				if (a > 3) a = 3;
				short biome = (short) (map.biome + a+1);
				map.setColorDat(x, y, makeRandomBiome(biome, trX, trY));
				// river chance
				float proz=0;
				switch (map.biome) {
				case 100:
					//jungle, ym, pust, taiga
					proz = 0.005f;
					break;
				case 200:
				case 400:
					proz = 0.001f;
					break;
				case 300:
					proz = 0.0001f;
					break;
				}
				if (Math.random() <= proz) 
					makeRiva(x, y);

			}
		}
		//		mapp.clrmaps[chX][chY].ready = true;
	}

	private short makeRandomBiome(int biome, int x, int y) {
		int type = (int) Math.max(0, (Math.min(2, (randFromPair(x, y) * 3))));
		type *=10;
		//		if (biome+type == 641) System.out.println(" AWWW SHIEEEET " + biome);
		return (short) (biome+type);
	}


	//	private int getChunkBiome(int xCh, int yCh) {
	//		MapChunk chnk = getChunk(xCh, yCh);
	//		if (chnk != null) {
	//			System.out.println("  no chunkbiome at " + xCh + "x" + yCh);
	//			return chnk.biome;
	//		}
	//		else 
	//			return -1;
	//	}

	//определение цвета считывая напрямую карту.
	//	public short doItHardWay(int x, int y) {
	//		Double height = val(x,y);
	//		if (height <= waterlevel) return 900;
	//		//vlajnost
	//		float rad = 128,mindis = rad, dis = 0;
	//		for (int radius = 1; radius < rad; radius++) {
	//			for ( int xx = x - radius; xx <= x + radius; xx++ ) {
	//				for ( int yy = y - radius; yy <= y + radius; yy++ ) {
	//
	//					if ( xx == 0 - radius || xx == radius ||  yy == 0 - radius || yy == radius ) {
	//
	//						if (val(xx,yy) <= waterlevel) dis = (float) Math.hypot(xx-x, yy -y);
	//						if (dis < mindis) mindis = dis;
	//
	//					}
	//				}
	//			}
	//			if (dis != 0) break;
	//		}
	//
	//		int vlaznost;
	//		if (mindis != rad) {
	//			vlaznost = (int) (6 - (mindis / (rad/6)));
	//			if (vlaznost == 0) vlaznost = 1;
	//		} else vlaznost = 1;
	//
	//		int a = (int) ((height - waterlevel) / ((1 - waterlevel)/4));
	//		if (a == 4) a -= 1;
	//
	//		return mapp.setColorDat(x, y, (short) ((vlaznost*100) + a+1));
	//	}

	public static Color getColor(short colDat) {
		Color clr = new Color(0f, 0f, 0.58125f, 1);

		switch (colDat) {


		case 0:
			clr = Color.red;
			break;
		case 1:
			clr = Color.blue;
			break;
		case 2:
			clr = Color.yellow;
			break;
		case 3:
			clr = Color.pink;
			break;


		case 900:
			//водица
			clr = new Color(0f, 0f, 0.58125f, 1);
			break;

		case 101:
		case 100: 
			//тропический дождливый лес
			clr = new Color(0.609375f, 0.73f, 0.66f, 1f);
			break;

		case 200:
			// умеренный дождливый лес
			//			clr = new Color(0.63f, 0.765f, 0.656f, 1f);
			//			clr = new Color(0.703f, 0.785f, 0.66f, 1f);
			clr = new Color(0.765f, 0.828f, 0.664f, 1f);


			break;

			//		case 603:
		case 400:
			//Тайга
			clr = new Color(0.796f, 0.828f, 0.73f, 1f);
			break;

			//		case 400 :
			// Снег
			//			clr = new Color(0.914f, 0.914f, 0.914f, 1f);
			//			break;
			//FIXME дописать вариации цветов или придумать чо-то
		case 103 :
		case 104  :
			//тропический сезонный лес
			clr = new Color(0.66f, 0.796f, 0.64f, 1f);
			break;

		case 202 :
		case 203 :
			//Умеренный лиственный лес
			clr = new Color(0.703f, 0.785f, 0.66f, 1f);
			break;

			//		case 403 :
		case 401 :
			//			кустарники
			clr = new Color(0.765f, 0.796f, 0.73f, 1f);
			break;

		case 403 :
			//			//тундра
			clr = new Color(0.863f, 0.863f, 0.73f, 1f);
			break;

		case 201 :
			//		case 302 :
			//		case 202 :
			//трава
			clr = new Color(0.765f, 0.828f, 0.664f, 1f);
			break;

			//		case 102 :
		case 300 :
		case 302 :
			//			умеренная пустныя
			clr = new Color(0.89f, 0.906f, 0.789f, 1f);
			break;

		case 304 :
			//			пустошь
			clr = new Color(0.73f, 0.73f, 0.73f, 1f);
			break;

			//		case 104 :
			//сухая земля
			//			clr = new Color(0.597f, 0.597f, 0.597f, 1f);
			//			break;
		}
		return clr;
	}


}
