package net.mdked.Jrachgame.world.mapgen;

import grid.GridMap;

import java.io.Serializable;
import java.util.ArrayList;

import net.mdked.Jrachgame.utils.FileManager;
import net.mdked.Jrachgame.world.Data;
import net.mdked.Jrachgame.world.Entity;
import net.mdked.Jrachgame.world.World;

public class MapChunk extends Data implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4111326103216497767L;

	public int xCh, yCh;
	public short[][] biomeC;
//	public boolean[][] gotMap;
	//	public Image[][] map;
	public byte[][] heightMap;
	public boolean[][] got;
	public GridMap gridMap;
	
	public short biome;
	
	public ArrayList<Entity> objects;
//	public int ids;
//	public ArrayList<Animal> animalz;

//	public double midHt;
//, big
	public boolean formed;
//, boolean big
	public MapChunk( int xCh, int yCh ) {
		dType = 1;
		this.xCh = xCh;
		this.yCh = yCh;
//		this.big = big;
		
//		if (!big) {
			//		map = new Image[World.chunksize][World.chunksize];
		
			heightMap = new byte[World.chunksize][World.chunksize];
			biomeC = new short[World.chunksize][World.chunksize];
			got = new boolean[World.chunksize][World.chunksize];
			gridMap = new GridMap(World.chunksize, World.chunksize);
			
			objects = new ArrayList<Entity>();
//			midHt = MapGen.getMdHtFromArch(xCh, yCh);
//			midHt = 0.3d;
//			World.addMapToCache(this);

			biome = MapGen.getChnkBiome(xCh, yCh);
//			humidity = (short) MapGen.getHumidity(yCh);
//		} else {
//			gotMap = new boolean[World.arhipelago][World.arhipelago];
//			heightMap = new Double[World.arhipelago][World.arhipelago];
//			World.bigmapCache.add(this);
//		}
		
		FileManager.SaveObject(this);
	}

//	public synchronized boolean gotMap(int x, int y ) {
//		return gotMap[x][y];
//	}
//	public synchronized void setgotMap(int x, int y, boolean bol) {
//		gotMap[x][y] = bol;
//	}
	
	public synchronized short getColorDat(int x, int y) {
		return biomeC[x][y];
	}
	public synchronized short setColorDat(int x, int y, short clr) {
		if (x >= 0 && x < World.chunksize && y >= 0 && y < World.chunksize)
			biomeC[x][y] = clr;
		return clr;
	}

	public synchronized void setFormed(boolean bol) {
		formed = bol;
	}




}
