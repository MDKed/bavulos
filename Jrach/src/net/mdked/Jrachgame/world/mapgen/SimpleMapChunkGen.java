package net.mdked.Jrachgame.world.mapgen;

import net.mdked.Jrachgame.world.World;

public class SimpleMapChunkGen {

	public MapChunk map;
	public double[][] heiz;
	private int mapsize;

//	public SimpleMapChunkGen( int xCh, int yCh ) {
//		map = new MapChunk(xCh, yCh, false);
//		mapsize = World.chunksize;
//		heiz = new double[3][3];
//		getheiz();
//		
//		for (int x = 0; x < mapsize; x ++ ) {
//			
//			for ( int y = 0; y < mapsize; y ++ ) {
//				Double xStart, yEnd;
//				xStart = getXStart(x,y);
//				yEnd = getYend(x, y);
//				
//				map.heightMap[x][y] = xStart + (y * ((yEnd-xStart)/mapsize));
//			
//					
//			}
//		}
//
//	}
//	
	private Double getYend(int x, int y) {
		Double yEnd;
		if (x < mapsize/2 ) {
			if ( y < mapsize/2 ) {
				Double yS = (heiz[0][1] + heiz[1][1]) / 2,
						yE =  heiz[1][1];
				yEnd = yS  + (x * ((yE-yS)/(mapsize/2)));

			}
			else {
				Double yS = (heiz[0][2] + heiz[1][1]) / 2,
						yE = (heiz[1][2] +heiz[1][1]) / 2;
				yEnd = yS + (x * ((yE-yS)/(mapsize/2)));
			}
			
		} else {
			if ( y < mapsize/2 ) {
				Double xS = heiz[1][1],
						xE = (heiz[2][1] + heiz[1][1]) / 2;
				yEnd = xS  + (x * ((xE-xS)/(mapsize/2)));

			}
			else {
				Double xS = ( heiz[1][2] +heiz[1][1] ) / 2,
						xE = ( heiz[2][2] + heiz[1][1] ) / 2;
				yEnd = xS + (x * ((xE-xS)/(mapsize/2)));
			}
		}
		return yEnd;
	}
	private Double getXStart(int x, int y) {
		Double xStart;
		if (x < mapsize/2 ) {
			if ( y < mapsize/2 ) {
				Double xS = (heiz[0][0] + heiz[1][1]) / 2,
						xE = (heiz[1][0] + heiz[1][1]) / 2;
				xStart = xS  + (x * ((xE-xS)/(mapsize/2)));

			}
			else {
				Double xS = (heiz[0][1] + heiz[1][1]) / 2,
						xE = heiz[1][1];
				xStart = xS + (x * ((xE-xS)/(mapsize/2)));
			}
			
		} else {
			if ( y < mapsize/2 ) {
				Double xS = (heiz[1][0] + heiz[1][1]) / 2,
						xE = (heiz[2][0] + heiz[1][1]) / 2;
				xStart = xS  + (x * ((xE-xS)/(mapsize/2)));

			}
			else {
				Double xS = heiz[1][1],
						xE = (heiz[2][1] + heiz[1][1]) / 2;
				xStart = xS + (x * ((xE-xS)/(mapsize/2)));
			}
		}
		return xStart;
	}


//	private void getheiz() {
//		for ( int hX = -1; hX < 2; hX++ ) {
//			for (int hY = -1; hY < 2; hY++ ) {
//				heiz[hX+1][hY+1] = 
//						(MapGen.getMdHtFromArch(map.xCh + hX, map.yCh + hY ) 
//								+ MapGen.getMdHtFromArch(map.xCh, map.yCh))/2;
//			}
//		}
//	}

}
