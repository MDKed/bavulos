package net.mdked.Jrachgame.world.mapgen;

import grid.GridMap;
import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.utils.FileManager;
import net.mdked.Jrachgame.utils.Pifagor;
import net.mdked.Jrachgame.world.Minable;
import net.mdked.Jrachgame.world.World;

public class ObjChGen {

	private int wala = GridMap.WALL;

	public ObjChGen( int objChX, int objChY, MapChunk map) {
//		ObjChunk oCh = new ObjChunk(objChX, objChY);
		for (int x = 0; x < World.chunksize; x ++ ) {
			for (int y = 0; y < World.chunksize; y ++ ) {
//				int[] kotr = {(objChX * World.objchunksize)+x, (objChY * World.objchunksize)+ y};
				int[] where = { (int) ((x*32)+16+(Math.random()*8)),
						(int) ((y*32)+16+(Math.random()*8))};

				//						System.out.println(MapManager.getColor(kotr[0], kotr[1]));
				if (map.getColorDat(x, y) < 900 ) {
//					int[] mapxy = Pifagor.getXchYch(kotr[0], kotr[1]);
					//					System.out.println(MapManager.getMap(mapxy[0], mapxy[1]).biome);
					if (placeRandomObject(where, map))
						map.gridMap.set(x, y, wala);
					wala = GridMap.WALL;

					//					int[] xyq = Pifagor.getXYminCH(kotr[0], kotr[1]);
				}
			}
		}
//		FileManager.SaveObject(oCh);
//		int[] map = Pifagor.getChunkFromOChunk(objChX, objChY);
//		FileManager.SaveObject(World.map);
//		World.insertObjFromChunkToWorld(oCh);
		//		DasGame.mapMan.ordOCh.remove(oder);
//		DasGame.mapMan.removeOChOder(oder);
	}

	public boolean placeRandomObject( int[] where, MapChunk map) {

		//		int[] where2 = Pifagor.getXchYch(x, y);
		//FIXME optimize
		int[] proc = new int[4];
		switch( map.biome ) {
		case 100:
			proc[0] = 10;
			proc[1] = 5;
			proc[2] = 25;
			proc[3] = 2;
			//			grasProc = 10;
			//			kustProc = 5;
			//			treeProc = 25;
			//			stoneProc = 2;
			break;
		case 200:
			proc[0] = 15;
			proc[1] = 10;
			proc[2] = 15;
			proc[3] = 5;
			break;
		case 300:
			proc[0] = 1;
			proc[1] = 5;
			proc[2] = 5;
			proc[3] = 10;
			break;
		case 400:
			proc[0] = 5;
			proc[1] = 5;
			proc[2] = 20;
			proc[3] = 5;
			break;
		}

		//		switch (map.getColorDat(xK, yK)) {
		//		case 101:
		//		case 201:
		//		case 301:
		//		case 401:
		//		case 111:
		//		case 211:
		//		case 311:
		//		case 411:
		//		case 121:
		//		case 221:
		//		case 321:
		//		case 421:
		//			//travka kustiki;
		//			int type = (int) (randFromPair(where[0], where[1])*2);
		//			createRandomObj(where, type, proc);
		//			break;
		//
		//		case 102:
		//		case 202:
		//		case 302:
		//		case 402:
		//		case 103:
		//		case 203:
		//		case 303:
		//		case 403:
		//		case 104:
		//		case 204:
		//		case 304:
		//		case 404:
		//		case 112:
		//		case 212:
		//		case 312:
		//		case 412:
		//		case 113:
		//		case 213:
		//		case 313:
		//		case 413:
		//		case 114:
		//		case 214:
		//		case 314:
		//		case 414:
		//		case 122:
		//		case 222:
		//		case 322:
		//		case 422:
		//		case 123:
		//		case 223:
		//		case 323:
		//		case 423:
		//		case 124:
		//		case 224:
		//		case 324:
		//		case 424:
		//			int typea = (int) (randFromPair(where[0], where[1])*4);
		//			createRandomObj(where, typea, proc);
		//			break;
		//		}
		int type = (int) (Math.random()*4);
		//		System.out.println("type=" + type);
		return createRandomObj(where, type, proc, map);

	}

	private boolean createRandomObj(int[] where, int type, int[] proc, MapChunk map) {
		int oType=0;

		int rand =(int)(Math.random()*101);
		if (rand < proc[type]) 
		{
			switch (type) {
			case 0:
				//trava
				if ((int)(Math.random()*101) < 75) 
					oType = 4;
				else 
					oType = 5;
				break;

			case 1:
				//kust
				if (map.biome == 300) 
					oType = 3;
				else 
					oType = 2;
				break;

			case 2:
				//derevo
				if (map.biome != 400)
					oType = 6;
				else if (map.biome == 300)
					oType = 7;
				else
					oType = 0;
				break;

			case 3:
				//kamen
				oType = 1;
				break;


			}
			Minable mana = new Minable(where[0], where[1],(byte) oType,(byte) 1, map.biome);
			if (mana.passable) wala = 1;
			map.objects.add(mana);
			return true;
		}
		return false;
	}
}
