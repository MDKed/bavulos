package net.mdked.Jrachgame.world.mapgen;

public class MapGenRun implements Runnable {
	
	private Thread t;
	
	
	int chX, chY;
//	boolean big;
	
	public MapGenRun(int chX, int chY) {
		this.chX = chX;
		this.chY = chY;
//		this.big = big;
	}

	@Override
	public void run() {
		new MapGen(chX, chY);
		System.out.println(" -==-Map Generated " + chX + "x" + chY );
	}
	
	public void start ()
	   {
	      System.out.println("Starting " + chX + "x" + chY );
	      if (t == null)
	      {
	         t = new Thread (this);
	         t.start ();
	      }
	   }

}
