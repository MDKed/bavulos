package net.mdked.Jrachgame.world.mapgen;

import java.util.ArrayList;

import net.mdked.Jrachgame.utils.FileManager;
import net.mdked.Jrachgame.utils.Pifagor;
import net.mdked.Jrachgame.world.EncounterMan;
import net.mdked.Jrachgame.world.Entity;
import net.mdked.Jrachgame.world.World;

public class MapManager  implements Runnable {

	private Thread t;

//	public boolean objRef, mapRef, setWall;

//	private ArrayList<SetWallQue> swq;
//	private ArrayList<ObjLoadAsk> olq, mapGenq;
//	private ArrayList<Entity> removeEnQue, insertEnQue;
//
//	private  ArrayList<int[]> ordered, ordOCh;
	
	private int chX, chY;
	
	public MapManager(int chX, int chY ) {
		this.chX = chX;
		this.chY = chY;
	}
	


	//private int swX, swY, swT;

//	public  void askObjRefr() {
//		objRef = true;
//	}
//	public  void askMapRefr() {
//		mapRef = true;
//	}

	//	public MapChunk getMap(int xCh, int yCh) {
	//		return MapGen.getChunk(xCh, yCh);
	//	}

//	public synchronized void generateOChnk( int xOCh, int yOch ) {
//		if (isOChOrd(xOCh, yOch)) return;
//		//proverka est li karta na kotoroy budet generaziya
//		int[] xyCH = Pifagor.getChunkFromOChunk(xOCh, yOch);
//		//		if (MapGen.checkCache(xyCH[0], xyCH[1]) != null && MapGen.checkCache(xyCH[0], xyCH[1]).formed) {
//		if (World.map.formed) {
//			int[] oder = {xOCh, yOch};
//			ordOCh.add(oder);
//
//			ObjChGenRun  ochRun = new ObjChGenRun(xOCh, yOch, oder);
//			ochRun.start();
//		}
//
//	}
	
//	public synchronized void generateMap(int chX, int chY) {
//		ObjLoadAsk cheki = new ObjLoadAsk(chX, chY, 0);
//
//		if (!isOrdered(cheki))
//			mapGenq.add(cheki);
//	}

	public synchronized void generateMapp( int xCh, int yCh ) {
		//		System.out.println("gen map " + xCh+ "x" + yCh);
//		if (isOrdered(xCh, yCh)) return;
//		int[] oder = {xCh,yCh};
//		ordered.add( oder );

		MapGenRun mapgen = new MapGenRun(xCh, yCh);
		mapgen.start();

	}

//	public synchronized  boolean isOChOrd(int xOCh, int yOCh) {
//		for (int a = 0; a < ordOCh.size(); a ++ ) {
//			if ( ordOCh.get(a)[0] == xOCh && ordOCh.get(a)[1] == yOCh ) 
//				return true;
//		}
//		return false;
//	}

//	public synchronized boolean isOrdered(ObjLoadAsk cheki) {
//		for (int a = 0; a < olq.size(); a++ ) {
//			//			System.out.println(a + " " + olq.size());
//			if (a != olq.size() && a < olq.size()) {
//				ObjLoadAsk chek = olq.get(a);
//				if (chek != null)
//					if ( chek.chX == cheki.chX && chek.chY == cheki.chY && chek.type == cheki.type ) return true;
//			}
//		}
//		return false;
//
//	}

//	public synchronized boolean isOrdered(int xCh, int yCh) {
//		for (int a = 0; a < ordered.size(); a++ ) {
//			int[] chek = ordered.get(a);
//			if ( chek[0] == xCh && chek[1] == yCh ) return true;
//		}
//		return false;
//	}

//	public synchronized void removeOrder(int xCh, int yCh) {
//		int num= -1;
//		for (int a = 0; a < ordered.size(); a++ ) {
//			int[] chek = ordered.get(a);
//			if ( chek[0] == xCh && chek[1] == yCh ) {
//				num = a;
//				break;
//			}
//		}
//		if (num > -1)
//			ordered.remove(num);
//	}

	//	public static void generateBigMap( int xCh, int yCh ) {
	//		MapGenRun mapgen = new MapGenRun(xCh, yCh, true);
	//		mapgen.start();
	//
	//	}
//	public ObjChunk getOCh( int xOCh, int yOCh ) {
//		//proverka cache, potom zagruzka
//		//		System.out.println(" tryin to load cache ochnk " + xOCh + "x" + yOCh + " woch size = " + World.objChunks.size());
//		for (int a=0; a < World.objChunks.size(); a ++ ) {
//			if (World.objChunks.get(a).xOCh == xOCh && World.objChunks.get(a).yOCh == yOCh ) 
//			{
//				EncounterMan.randomAnimalSpawn(xOCh, yOCh);
//				return World.objChunks.get(a);
//
//			}
//		}
//		//				System.out.println("tryin to load ochnk");
//
//		//		return (ObjChunk) FileManager.loadObject(xOCh, yOCh, 2);
//		initLoad(xOCh, yOCh, 2);
//
//		return null;
//	}





//	public void refreshOCh()	{
//		int[] xyPOCh = Pifagor.getOCHfXY(World.player.x, World.player.y);
//		int oba = 0;
//		for ( int rx = -1; rx < 2; rx++ ) {
//			for ( int ry = -1; ry < 2; ry ++ ) {
//				int xX = xyPOCh[0] + rx, yY = xyPOCh[1] + ry;
//				//				System.out.println("  refresh Och " + xX +"x" + yY + " rXY :" + rx + "x" + ry);
//				getOCh(xX, yY);
//
//			}
//		}
//
//	}

	public void initMap() {
		//		System.out.println(" start gen");
		//
		int[] xyCh = Pifagor.getXchYch((int)World.player.x/32,(int) World.player.y/32);
		//		int[] arc = Pifagor.getXchYch(xyCh[0], xyCh[1], true);
		//		//		for (int x = -1; x < 2; x++ ) {
		//		//			for (int y = -1; y < 2; y++ ) {
		//		//				System.out.println("  map " + x + "x" + y);
		//		System.out.println( " player at " + xyCh[0] + "x" + xyCh[1]);
		//		if (MapGen.getBigMap(arc[0], arc[1]) == null) {
		//			generateBigMap(arc[0], arc[1]);
		////			World.setWMap(MapGen.getBigMap(arc[0], arc[1]));
		//			System.out.println( " big map generated");
		//			
		//
		//		}

		//		generateMap(xyCh[0],xyCh[1]);
		//				System.out.println("xyCH = " + xyCh[0] + "x" + xyCh[1] +  "biome = " + MapGen.getChunk(xyCh[0], xyCh[1]).biome + " heighht = " + MapGen.getChunk(xyCh[0], xyCh[1]).midHt );


		//			}
		//		}
		//		System.out.println(" map pregenerated");
	}
	/**
	 * Проверка кэша и удаление слишком далеких карт и проверка находится ли на ней объекты которые к ней привязаны
	 */
//	public void refreshCache() {
//		for ( int a = 0; a < World.mapCache.size(); a ++ ) {
//			MapChunk test = World.mapCache.get(a);
//			int dist = (int) Pifagor.getDist(test.xCh * (World.chunksize * 32), World.player.x, test.yCh * (World.chunksize * 32), World.player.y);
//			//			System.out.println( test.xCh + "x" + test.yCh + " dist = " + dist );
//			if ( dist > 128*World.chunksize ) {
//				//				System.out.println(" MapChunk go away " + test.xCh + "x" + test.yCh);
//				FileManager.SaveObject(test);
//				World.mapCache.remove(test);
//			}
//		}
//		//obj
//		for ( int b = 0; b < World.objChunks.size(); b++ ) {
//			ObjChunk test2 = World.objChunks.get(b);
//			int dist = (int) Pifagor.getDist(test2.xOCh * (World.objchunksize * 32), World.player.x, test2.yOCh * (World.objchunksize * 32), World.player.y);
//			//						System.out.println("b=" + b + " dist =" + dist);
//			if ( dist > 80*World.objchunksize ) {
//				//				System.out.println(" ObjChunk go away " + test2.xOCh + "x" + test2.yOCh);
//				//				for (int x = 0; x < test2.objcz.size(); x++) 
//				//					if (test2.objcz.get(x).enttype == 1) System.out.println(" helo mafaka, i'm animal!");
//				World.objChunks.remove(b);
//				FileManager.SaveObject(test2);
//				removeObjFrmWrld(test2);
//			}
//
//		}
//		//animalz
//		for ( int c = 0; c < World.animalz.size(); c ++ ) {
//			if ( Pifagor.getDist(World.player, World.animalz.get(c)) > 1400 ) 
//			{
//				World.removeEntityFromWorld(World.animalz.get(c));
//				//				System.out.println("   deleting animal dist > 1400");
//			}
//		}
//	}
//	private void removeObjFrmWrld( ObjChunk oCh ) {
//		//		World.removeEntityFromWorld(oCh.objcz.get(a));
//		for (int a = 0; a < oCh.objcz.size(); a++) 
//			World.removeEntityFromWorld(oCh.objcz.get(a));
//		//			remEntity(oCh.objcz.get(a));
//		World.objChunks.remove(oCh);
//
//	}

	@Override
	public void run() {
		boolean keks = false;
		float alive = 0;
		long start;
		
		generateMapp(chX, chY);
		
//		do
//		{
//			start = System.currentTimeMillis();
//
//			if (alive >= 1000) {
//				System.out.println(" ");
//				alive = 0;
//			}

//			if (objRef) {
//				//				System.out.println("mapmanager objRef");
//				refreshOCh();
//				objRef = false;
//			}
//			if (mapRef) {
//				//				System.out.println("mapmanager mapRef");
//				refreshCache();
//				mapRef = false;
//			}

//			while (swq.size() > 0) {
//				SetWallQue chok = swq.remove(0);
//				if( chok == null) break;
//				int[] chk= Pifagor.getXchYch(chok.swX, chok.swY);
//				MapChunk chunk = getMap(chk[0], chk[1]);
//				int[] kotr = Pifagor.getXYminCHfd(chok.swX, chok.swY);
//				chunk.gridMap.set(kotr[0], kotr[1], chok.swT);
//			}
//
//			while (insertEnQue.size() > 0) {
//				Entity object = insertEnQue.remove(0);
//				if( object == null) break;
//				int[] oxy = Pifagor.getOCHfXY(object.x, object.y);
//				System.out.println("  insertEnq Och " + oxy[0] +"x" + oxy[1]);
//				ObjChunk och = getOCh(oxy[0], oxy[1]);
//				och.objcz.add(object);
//			}
//
//			while (removeEnQue.size() > 0) {
//				Entity object = removeEnQue.remove(0);
//				if( object == null) break;
//				int[] oxy = Pifagor.getOCHfXY(object.x, object.y);
//				System.out.println("  removeEnq Och " + oxy[0] +"x" + oxy[1]);
//
//				ObjChunk och = getOChInC(oxy[0], oxy[1]);
//				if (och != null && och.objcz.contains(object))
//					och.objcz.remove(object);
//				//				deleteEntity(object);
//			}

//			while (olq.size() > 0) {
//				//			for (int a = 0; a < olq.size(); a++ ) {
//
//				ObjLoadAsk loask = olq.remove(0);
//				if (loask == null) continue;
//				FileManager.loadObject(loask.chX, loask.chY, loask.type);
//			}
//
//			while (mapGenq.size() > 0) {
//				ObjLoadAsk geask = mapGenq.remove(0);
//				if (geask == null) break;
//				generateMapp(geask.chX, geask.chY);
//			}
//
//
//
//			alive += System.currentTimeMillis() - start;
//		}
//		while (!keks);
	}


	//	private void deleteEntity(Entity ento	) {
	//		World.objecz.remove(ento);
	//
	//		switch (ento.enttype) {
	//
	//		case 1:
	//			World.animalz.remove(ento);
	//			break;
	//
	//		case 3:
	//		case 9:
	//			World.sledi.remove(ento);
	//			break;
	//		}
	//	}

//	private ObjChunk getOChInC(int xOCh, int yOCh) {
//		for (int a=0; a < World.objChunks.size(); a ++ ) 
//			if (World.objChunks.get(a).xOCh == xOCh && World.objChunks.get(a).yOCh == yOCh ) 
//				return World.objChunks.get(a);
//
//		return null;
//	}

	public void start ()
	{
		//		System.out.println("Starting PF for " + faza );
		if (t == null)
		{
			t = new Thread (this, "MapManager");
			//						t.setPriority(4);

//			swq = new ArrayList();
//			removeEnQue= new ArrayList();
//			insertEnQue= new ArrayList();
//			olq =  new ArrayList();
//			mapGenq = new ArrayList();
//			ordered = new ArrayList<int[]>();
//			ordOCh = new ArrayList<int[]>();

			t.start ();
		}
	}
//	public synchronized void addEntity(Entity ent) {
//		insertEnQue.add(ent);
//	}
//	public synchronized void remEntity(Entity ent) {
//		removeEnQue.add(ent);
//	}


//	public synchronized void setWall(int xK, int yK, int wat) {
//		//	 setWall = true;
//		swq.add(new SetWallQue(xK,yK,wat));
//
//	}
//	public synchronized void initLoad(int xCh, int yCh, int i) {
//		ObjLoadAsk cheki = new ObjLoadAsk(xCh, yCh, i);
//
//		if (!isOrdered(cheki)) {
//			//			System.out.println("request load " + xCh + "x" + yCh + " type= " +  cheki.type);
//			olq.add(new ObjLoadAsk(xCh,yCh, i));
//		}
//	}
//	public void removeOChOder(int[] oder) {
//		ordOCh.remove(oder);
//	}

}


