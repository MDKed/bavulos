package net.mdked.Jrachgame.world.mapgen;

import java.io.Serializable;
import java.util.ArrayList;

import net.mdked.Jrachgame.world.Data;
import net.mdked.Jrachgame.world.Entity;

public class ObjChunk extends Data implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8240371217570044707L;
	
	public ArrayList<Entity> objcz;
	
	public	int xOCh, yOCh;
	
	public ObjChunk(int xOCh, int yOCh) {
		dType = 2;
		objcz = new ArrayList<Entity>();
		this.xOCh = xOCh;
		this.yOCh = yOCh;
	}
	

}
