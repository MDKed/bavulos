package net.mdked.Jrachgame.world;

import net.mdked.Jrachgame.player.inv.Weapon;

public class Missile extends Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7666480527736039429L;
	public float zSpeed, rot, speed;
	public byte type;
	public boolean stuck;
	public Entity host;
	public Weapon wpn;
	
	public Missile(Weapon wpn) {
		this.wpn = wpn;
		enttype = 5;
		
		switch (wpn.drawId) {
		case 100:
			width = 10;
			height = 3;
			break;
		}
	
	
		passable = true;
		speed = 200;
	}
//	public Rectangle getBounds() {
//		return new Rectangle(x-10, y-10, (float) 20, (float) 20);
//	}
	public void fly(int delta) {
		//System.out.println(this.toString() + " z=" + z + " zSpeed =" + zSpeed);
		
		if (!stuck && z > 0) {
			z += (zSpeed/1000f)*delta;
			zSpeed -= (100f/1000f) *delta;
		} else stuck = true;
	}
}
