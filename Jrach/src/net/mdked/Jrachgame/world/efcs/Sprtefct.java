package net.mdked.Jrachgame.world.efcs;

import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.utils.Pifagor;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;

public class Sprtefct extends Sled{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5397214719721156883L;
	public float speed, speedZ;
	public transient Animation anima;
	public String text;
	public int coler;

	public Sprtefct(float x, float y, float z, int kind, float speed, float speedz, int color) {
		super(x, y, kind, 0, false);
		//kind 0 = smoke, 1 = text;
		this.z = z;
		this.speed = speed;
		this.speedZ = speedz;
		enttype = 9;
		
		lifespan = 10000;
		coler = color;
		
		switch (kind) {
		case 0:
			anima = DasGame.smoke1.copy();
			break;
		case 1:
			lifespan = 200;
//			this.speed = 1000;
//			speedZ = 100;
			break;
		}
		
	}
	
	public void refresh(int delta) {
		Pifagor.moveByRotWind(this, delta);
		z += (speedZ/1000) * delta;
		speed += z/100;
		if (z < 0) z = 0;
		
	}
	
	public void draw() {
		//отрисовка анимации движущийся вверх + зависимость от ветра
	
		float[] wtd = DasGame.wtd(this);
		switch (kind) {
		case 0:
			DasGame.drawCntrd(anima, wtd[0], wtd[1]-(z * DasGame.xScale), new Color(1f,1f,1f,0.7f*(lifespan/20000f)), DasGame.xScale);
			break;
		case 1:
			DasGame.drawText(text, wtd[0], wtd[1]-(z * DasGame.xScale), coler);
			break;
		}
}

}
