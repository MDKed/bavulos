package net.mdked.Jrachgame.world.efcs;

import net.mdked.Jrachgame.world.Entity;

public class Sled extends Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -812459020728352878L;
	public int kind;
	public float rot;
	public int lifespan;
	public boolean mirrd;
	//0 - player, 1 - olen, 2 - medved, 3,4,5 - blood, 6 - tashenie;
	public Sled(float x, float y, int kind, float rot,boolean mirrord) {
		mirrd = mirrord;
		this.x = x;
		this.y = y;
		enttype = 3;
		passable = true;
		this.kind = kind;
		this.rot = rot;
		if (kind > 2 && kind < 6) lifespan = 60000; 
		else if (kind == 6) lifespan = 5000; 
		else
		lifespan = 20000;
	}
	
}
