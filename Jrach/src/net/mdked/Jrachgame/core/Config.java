package net.mdked.Jrachgame.core;

import net.mdked.Jrachgame.utils.LoadFont;

import org.newdawn.slick.UnicodeFont;

public class Config {
	public static float xScale;
	public static UnicodeFont font;
	public static boolean ingame, gamecfgch;
	
	public static void changeXScale(float xsc) {
		xScale = xsc;
		font = LoadFont.LoadFont();
		gamecfgch = true;
	}
}
