package net.mdked.Jrachgame.core;

import java.io.File;
import java.lang.reflect.Field;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import net.mdked.Jrachgame.gamelvlz.Introe;
import net.mdked.Jrachgame.gamelvlz.MainMenuState;
import net.mdked.Jrachgame.gamelvlz.Optionz;
import net.mdked.Jrachgame.utils.PropManager;

public class MainKlaz extends StateBasedGame {
	
	public static final int INTRO  =  0;
	public static final int MAINMENUSTATE          = 1;
	public static final int GAMEPLAYSTATE          = 2;
	public static final int OPTIONSSTATE  =  3;



	public MainKlaz() {
		super("Bavulos");
	}

	/**
	 * @param args
	 */

	public static void main(String[] args) 
			throws SlickException
			{
//		prepareSystem();
		PropManager.loadProperties();
		AppGameContainer app = 
				new AppGameContainer(new MainKlaz());

		app.setDisplayMode(Integer.parseInt(PropManager.prop.getProperty("screenWidth")), Integer.parseInt(PropManager.prop.getProperty("screenHeight")), false);
//		app.setIcon("img/player1.gif");
		app.setMinimumLogicUpdateInterval(20);
		app.setShowFPS(true);
		app.setTargetFrameRate(60);
		app.start();
	


			}

//	static void prepareSystem()
//	{
//		try {
//			System.setProperty( "java.class.path", "."+File.separator+"lib"+File.separator);
//			System.setProperty( "java.library.path", "."+File.separator+"lib"+File.separator);
////			System.out.println(System.getProperties().get("java.library.path"));
////			System.out.println(System.getProperties().get("java.class.path"));
//			Field fieldSysPath = ClassLoader.class.getDeclaredField( "sys_paths" );
//			fieldSysPath.setAccessible( true );
//			fieldSysPath.set( null, null );
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}
//
//	}

	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		
//		this.addState(new Introe(INTRO));
		
		this.addState(new MainMenuState(MAINMENUSTATE));
		
		this.addState(new DasGame(GAMEPLAYSTATE));
		
		this.addState(new Optionz(OPTIONSSTATE));
		

	}

}
