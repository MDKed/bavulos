package net.mdked.Jrachgame.core;

import org.newdawn.slick.geom.Rectangle;

import net.mdked.Jrachgame.utils.DamageUtil;
import net.mdked.Jrachgame.utils.Pifagor;
import net.mdked.Jrachgame.world.Entity;
import net.mdked.Jrachgame.world.Missile;
import net.mdked.Jrachgame.world.Mobster;
import net.mdked.Jrachgame.world.MousePoint;
import net.mdked.Jrachgame.world.Wall;
import net.mdked.Jrachgame.world.World;

public class CollisionMan {
	
	public static boolean isColided(Entity o1, Entity o2) {
		
		if (o2.enttype == 11)
			return isWalCol(o1, (Wall) o2);
		
		return isColided(0, o1.x, o1.y, o1.width, o2.x, o2.y, o2.width);
	}
	
	public static boolean isWalCol(Entity o1, Wall wall) {
		Rectangle obj = new Rectangle(o1.x-o1.width/2, o1.y-o1.width/2 , o1.width, o1.width );
//				o1.width, o1.height);
		
		Rectangle waleone = new Rectangle(wall.x, wall.y, 2, 2), waletwo = new Rectangle(wall.x, wall.y, 2, 2);
		
		switch (wall.ico) {
		case 1:
		case 7:
		case 11:
			waleone = new Rectangle(wall.x-2, wall.y -2, 18, 4);
			break;
			
		case 3:
		case 8:
		case 9:
		case 13:
			waleone = new Rectangle(wall.x-16, wall.y -2, 18, 4);
			break;
			
		case 5:
		case 12:
		case 14:
		case 15:
			waleone = new Rectangle(wall.x-16, wall.y -2, 36, 4);
			break;
		
			
		}
		
		//VERT
		switch (wall.ico) {
		case 2:
		case 7:
		case 8:
			waletwo = new Rectangle(wall.x-2, wall.y -2, 4, 18);
			break;
			
		case 4:
		case 9:
		case 10:
		case 14:
			waletwo = new Rectangle(wall.x-2, wall.y -16, 4, 18);
			break;
			
		case 6:
		case 11:
		case 13:
		case 15:
			waletwo = new Rectangle(wall.x-2, wall.y -16, 4, 36);
			break;
			
		}
		if (waleone.intersects(obj)) 
			return true;
		if (waletwo.intersects(obj)) 
			return true;
		
		return false;
	}
	
	public static boolean isColided(int reach, Entity o1, Entity o2) {
		return isColided(reach, o1.x, o1.y, o1.width, o2.x, o2.y, o2.width);
	}
		
	public static boolean isColided(int reach, float x1, float y1, int w1, float x2, float y2, int w2) {
		double dist = Pifagor.getDist(x1, x2,
				y1, y2) - w2;
		
		if ( dist  <= w1 + reach ) {
//			System.out.println( "coll dist = " + dist);
			return true;
		}
		
		return false;
	}
	
	public static boolean checkCollison(Entity obj) {
		boolean ismouse = false, anical = false, misile = false, 
				player = false;
			
		

			
		switch (obj.enttype) {

		case 1:
			/// instanceof Animal
			anical = true;
			break;
		case 2:
//			instanceof Facility
			break;
			
		case 4:
			//instanceof MousePoint)
//			System.out.println("miaus");
			ismouse = true;
			break;
			
		case 5:
			//			instanceof Missile
			misile = true;
			break;
		case 6:
			//			 instanceof Player
			player = true;
			break;
		}

		//		if (obj.enttype == 1) {
		//			anical = true;
		//			//			Animal chk = (Animal) obj;
		//			//			obj1r = chk.getBounds();
		//		}
		//instanceof MousePoint)
		//		if (obj.enttype == 4)  {
		//			ismouse = true;
		//			//			MousePoint ms = (MousePoint) obj;
		//			//			obj1r = ms.getBounds();
		//		}

		//		if (obj. instanceof Missile) {
		//			misile = true;
		//			Missile mis = (Missile) obj;
		//			inittr = mis;
		//			obj1r = mis.getBounds();
		//		}

		for (int a = 0; a < World.map.objects.size(); a ++) {
			boolean 
					animal = false, misileget = false, pullable = false, wall = false;
			
			//tgt=false,
			Entity chCol = World.map.objects.get(a);
			//TODO ебаная каша collis
			if (obj.id == chCol.id) continue;
//			System.out.println(" Chcol type = " + chCol.enttype + " an " + animal);
			switch (chCol.enttype) {
			
			case 0:
				break;
			case 1:
				animal = true;
				break;
				
			case 5:
				Missile an = (Missile) chCol;
				if (an.stuck && !anical) 
					misileget = true;
				break;
				
			case 7:
				pullable = true;
//				tgt = true;
				break;
				
			
				
			}
			
			if (isColided(obj, chCol)) {
				if (misile && !chCol.passable) {
//					System.out.println("  Missile colided type:" + chCol.enttype + " pass = " + chCol.passable);
					Missile mis = (Missile) obj;
					if (animal && !mis.stuck) {
//						System.out.println(" chCol type "+ chCol.enttype + " animal" + animal + " stuck" + mis.stuck);
						Mobster an = (Mobster) chCol;
//						tgt = true;

						mis.stuck = true;
						mis.z = 0;
						//						kopie
						DasGame.playSound(5,an);

						DamageUtil.dealDamage(mis.wpn, an);
//						an.criticalWound = true;
//						an.hp -= 50;
//						an.attacked(mis.host);
						
						//System.out.println(" olens hp = " + an.hp);
					}
					else if (!mis.stuck && !player && !misileget) {
						Missile misa = (Missile) obj;
						misa.stuck = true;
					}
					else if ( misileget ) return false;
				}
				//&& World.player.attacking
				
				if (ismouse && isColided(20, obj, chCol)) {
					MousePoint meos = (MousePoint) obj;
					meos.target = chCol;
					World.player.target = chCol;
					
					if (World.acbar)
						DasGame.akbar.setTgt(chCol);
					
					chCol.sveto = true;
				}
//				if (ismouse ) {
//ataka
//					if (isColided(10, obj, chCol)  && World.player.playerAction(2)) {
//
//						//System.out.println(" attaka igroka tzeli " + tgt);
//						World.player.target = chCol;
//					}

//				}

//				if (ismouse && World.acbar) {
////					System.out.println(" akbary " + tgt);
//
//					if (isColided(20, obj, chCol)) {
//						DasGame.akbar.setTgt(chCol);
//					}
//				}

				if (player && misileget)  {
					Missile missle = (Missile) chCol;
					if ( World.player.inventory.addItem(missle.wpn))
					World.removeEntityFromWorld(chCol);
//					
				}
				if (pullable) return false;
				if (!chCol.passable) {
//					if (player) System.out.println("  Player colided type:" + chCol.enttype);
//					if (anical) System.out.println("  Animal colided type:" + chCol.enttype);
					return true;
				}
			}
		}

		

//		}

		return false;

	}
}
