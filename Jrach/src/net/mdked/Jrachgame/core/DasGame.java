package net.mdked.Jrachgame.core;

import java.io.File;
import java.util.ArrayList;

import net.mdked.Jrachgame.hud.ActionBar;
import net.mdked.Jrachgame.hud.InteractionWindow;
import net.mdked.Jrachgame.hud.TextCloud;
import net.mdked.Jrachgame.player.Player;
import net.mdked.Jrachgame.player.Recipe;
import net.mdked.Jrachgame.utils.FileManager;
import net.mdked.Jrachgame.utils.Pifagor;
import net.mdked.Jrachgame.utils.PropManager;
import net.mdked.Jrachgame.utils.SpriteSheetRazor;
import net.mdked.Jrachgame.world.Charakter;
import net.mdked.Jrachgame.world.DragblObj;
import net.mdked.Jrachgame.world.Drop;
import net.mdked.Jrachgame.world.EncounterMan;
import net.mdked.Jrachgame.world.Entity;
import net.mdked.Jrachgame.world.Facility;
import net.mdked.Jrachgame.world.Minable;
import net.mdked.Jrachgame.world.Missile;
import net.mdked.Jrachgame.world.Mobster;
import net.mdked.Jrachgame.world.MousePoint;
import net.mdked.Jrachgame.world.Pribor;
import net.mdked.Jrachgame.world.Wall;
import net.mdked.Jrachgame.world.World;



import net.mdked.Jrachgame.world.efcs.Sled;
import net.mdked.Jrachgame.world.efcs.Sprtefct;
import net.mdked.Jrachgame.world.mapgen.MapChunk;
import net.mdked.Jrachgame.world.mapgen.MapGen;
import net.mdked.Jrachgame.world.mapgen.MapManager;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;


/*
 * TODO 
 * сон в палатке отнимает пищу, кравт лекарств
 * 
 *  
 */

public class DasGame extends BasicGameState {

	private Color hglitClr;

	public static int DOWN = 2, UP = 0, RIGHT = 1, LEFT = 3;

	Image player, bear,
	tree, stone, spearPlStand, spearGrnd, playerDead
	, deerDead, bearDead, scoreTable, timer, ammo,
	palatka, fallentree, workshopA, shadow, terrain,
	bushB, bushF, grassB, grassF, treeB, treeF, floatplr;

	Image[] ground;

	static public Animation  fire, smoke1, deer, walls1;

	//	

	

	public static SpriteSheet br;

	SpriteSheet ammonum, statIco, sled, blood;

	public static Entity delCache;

	public static boolean isIntrd1, muteMusic, gOver, gameEnded,
	drawvoid, txtcld,  plLeftSl;

	public static boolean construct, canbuild, interwin, worldIsReady;

	public static float xScale=Config.xScale, time, timeEnd;

	public static float mX, mY;

	public static int constType;

	public static Entity consFac;

	int minleft, secleft, cuttimer, cutnum;

	UnicodeFont font;

	World world;

	//	Image[][] map;

	Player playerC;

	public static ActionBar akbar;

	public static TextCloud textC;

	public static InteractionWindow interw;

	//debug
	//	Color[][] minimap;
	boolean startload;


	ArrayList<Entity> upRen, downRen;

	static public int scrWi;

	static public int scrHe;

	static Sound plStep, deerStp, bearStp, swing, thrw, spHit, dmgs,
	deerAgr, deerAtk, deerDmg, deerDin, bearAgr,
	bearAtk, bearDmg, bearDin, maceHit, treechop, plHit;

	Music polka;

	static public MousePoint mouz;

	int stateID;

	private Image ykaz;

	private boolean drawDir, drawdbg;

	private float drawDirRot, rfrshTimer, oRfrshTimer;

	private SpriteSheet drop;

	//TODO sdelat hranilishe animatzii
	public static Animation plr, plrSh, plrJeans, plrBoots, plrHands, plrHat, plrHAxe, plrSpear, missiles;


	private static GameContainer gcc;

	public static MapManager mapMan;

	private short[][] drawgrnd;
	int xGW = 30, yGW = 22, xGWm = xGW/2, yGWm = yGW/2;

	public DasGame(int stateID )
	{
		this.stateID = stateID;
	}

	private void loadRsrcs() throws SlickException {
		int stSpriteSize = 16;
		drop = new SpriteSheet(new Image("img/drop.png", false, Image.FILTER_NEAREST), 16, 16);

		grassB = new Image("img/tgrass.png",false,Image.FILTER_NEAREST);
		grassF  = new Image("img/tgrass_f.png",false,Image.FILTER_NEAREST);
		bushB  = new Image("img/tkust_n.png",false,Image.FILTER_NEAREST);
		bushF  = new Image("img/tkust_f.png",false,Image.FILTER_NEAREST);
		treeB = new Image("img/ttree_n.png",false,Image.FILTER_NEAREST);
		treeF = new Image("img/ttree_f.png",false,Image.FILTER_NEAREST);

		walls1 = new Animation( new SpriteSheet(new Image("img/wals1.png",false,Image.FILTER_NEAREST), stSpriteSize*2, stSpriteSize*2), 100);

		//		snow1 = new Image("img/snow.png",false,Image.FILTER_NEAREST);
		//		snow2 = new Image("img/snow2.png",false,Image.FILTER_NEAREST);
		terrain = new Image("img/terrain.png", false, Image.FILTER_NEAREST);
		ground = SpriteSheetRazor.rapeSheet( terrain, 135, 32, 1);

		deer = new Animation( new SpriteSheet(new Image("img/deer.png",false,Image.FILTER_NEAREST), stSpriteSize*2, stSpriteSize*2), 100) ;


		plr = new Animation( new SpriteSheet(new Image("img/plr/player.png",false,Image.FILTER_NEAREST), stSpriteSize, stSpriteSize), 100) ;
		plrSh = new Animation( new SpriteSheet(new Image("img/plr/shirt.png",false,Image.FILTER_NEAREST), stSpriteSize, stSpriteSize), 100) ;
		plrJeans = new Animation( new SpriteSheet(new Image("img/plr/jeans.png",false,Image.FILTER_NEAREST), stSpriteSize, stSpriteSize), 100) ;
		plrBoots = new Animation( new SpriteSheet(new Image("img/plr/boots.png",false,Image.FILTER_NEAREST), stSpriteSize, stSpriteSize), 100) ;
		plrHands = new Animation( new SpriteSheet(new Image("img/plr/gloves.png",false,Image.FILTER_NEAREST), stSpriteSize, stSpriteSize), 100) ;
		plrHat = new Animation( new SpriteSheet(new Image("img/plr/hat.png",false,Image.FILTER_NEAREST), stSpriteSize, stSpriteSize), 100) ;
		plrHAxe  = new Animation( new SpriteSheet(new Image("img/plr/axe.png",false,Image.FILTER_NEAREST), stSpriteSize*2, stSpriteSize*2), 100) ;
		plrSpear = new Animation( new SpriteSheet(new Image("img/plr/spr1.png",false,Image.FILTER_NEAREST), stSpriteSize*2, stSpriteSize*2), 100) ;

		player = plr.getImage(1);
		//		floatplr = plr.getSprite(0, 0).getSubImage(0, 0, plr.getSprite(0, 0).getWidth(), 14);
		missiles = new Animation( new SpriteSheet(new Image("img/missile.png",false,Image.FILTER_NEAREST), stSpriteSize, stSpriteSize), 100) ;

		br = new SpriteSheet(new Image("img/bear.gif",false,Image.FILTER_NEAREST), stSpriteSize, stSpriteSize);
		//		dr = new SpriteSheet(new Image("img/deer.gif",false,Image.FILTER_NEAREST), stSpriteSize, stSpriteSize);

		tree = new Image("img/tree1.gif",false,Image.FILTER_NEAREST);
		stone = new Image("img/rock1.gif",false,Image.FILTER_NEAREST);

		//		spearPlStand = plr.getSprite(0, 3);
		//		spearGrnd = new Image("img/speargr.gif",false,Image.FILTER_NEAREST);
		//		playerDead = plr.getSprite(2, 3);
		//FIXME Дорисовать топор, копье, дописать картиночки сделать анимации и шмот

		deerDead = deer.getImage(25);
		shadow = new Image("img/shadow.gif",false,Image.FILTER_NEAREST);

		//		scoreTable = new Image("img/sctable.gif",false,Image.FILTER_NEAREST);
		//		gameOver = new Image("img/GO.gif",false,Image.FILTER_NEAREST);
		//		timer = new Image("img/timer.gif",false,Image.FILTER_NEAREST);
		//		ammo = new Image("img/spearamm.gif",false,Image.FILTER_NEAREST);
		palatka = new Image("img/palatk.gif",false,Image.FILTER_NEAREST);
		workshopA = new Image("img/Workshep1.png",false,Image.FILTER_NEAREST);
		ykaz = new Image("img/homedir.png",false,Image.FILTER_NEAREST);

		//		ammonum = new SpriteSheet(new Image("img/spearammn.gif",false,Image.FILTER_NEAREST), 32, 32);
		fire = new Animation(new SpriteSheet(new Image("img/fire.gif",false,Image.FILTER_NEAREST), stSpriteSize, stSpriteSize),100);

		//		firePlace = fire.getImage(1);
		statIco = new SpriteSheet(new Image("img/statico.png",false,Image.FILTER_NEAREST), stSpriteSize, stSpriteSize);
		sled = new SpriteSheet(new Image("img/sled.gif",false,Image.FILTER_NEAREST), 4, 4);
		blood = new SpriteSheet(new Image("img/blood.gif",false,Image.FILTER_NEAREST), stSpriteSize/2, stSpriteSize/4);


		//Player Anim


		//FIXME Efcsz
		smoke1 = new Animation(new SpriteSheet(new Image("img/smoke16.png",false,Image.FILTER_NEAREST), 16, 16),100);

		bearDead = br.getSprite(5, 2);
		//		fallentree = fallingtree.getImage(5);

		treechop = new Sound("snd/treechop.wav");
		plStep = new Sound("snd/plStep.wav");
		deerStp = new Sound("snd/deerwlk.wav");
		bearStp= new Sound("snd/berWalk.wav");
		swing = new Sound("snd/swing.wav");
		thrw = new Sound("snd/seartrw.wav"); 
		spHit = new Sound("snd/sphit.wav");
		maceHit =  new Sound("snd/smash.wav");
		dmgs = new Sound("snd/dmgs.wav");
		deerAgr = new Sound("snd/elkroar.wav");
		deerAtk = new Sound("snd/deeratk.wav");
		deerDmg = new Sound("snd/deerdmgd.wav");
		deerDin = new Sound("snd/deerdyin.wav");
		bearAgr = new Sound("snd/broar1.wav");
		bearAtk = new Sound("snd/bata.wav");
		bearDmg = new Sound("snd/bdmgd.wav");
		bearDin = new Sound("snd/bded.wav");
		plHit = new Sound("snd/ouch1.wav");


		polka = new Music("snd/msc/SpazzmaticaPolka.ogg");
	}

	//
	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		//deleting all shiet

		File map = new File("map");
		map.mkdirs();
		//		while (map.list().length > 0) {
		//			System.out.println(map.list().length);
		//			File clr1 = new File(map.getName() + map.list()[0]);
		//			clr1.delete();
		//		}
		//		
		//		map = new File("map");
		//		while (map.list().length > 0) {
		//			File clr1 = new File(map.getName() + map.list()[0]);
		//			clr1.delete();
		//		}
		//		map.delete();


		drawgrnd = new short[xGW][yGW];

		rfrshTimer = 0;

		gc.setSoundVolume(Float.parseFloat(PropManager.prop.getProperty("soundLevel")));
		xScale = Config.xScale;
		font = Config.font;
		gcc = gc;

		loadRsrcs();

		isIntrd1 = false;
		//xScale = gc.getWidth()/320f;

		scrWi =  gc.getWidth();
		scrHe = gc.getHeight();

		mouz = new MousePoint();




		world = new World();
		world.setCurCH(0,0);
		worldIsReady = false;


		//		Facility palatk = new Facility((byte) 4, (World.chunksize/2)*32, (World.chunksize/2)*32 -200);
		//		World.objecz.add(palatk);
		//		World.stObjecz.add(palatk);

		//player init
		playerC = World.player;
		
		makeNPCAnim(playerC, 1);
//		playerC.corpusA = Pifagor.makePlrAnim(plr);
//		playerC.shirt = Pifagor.makePlrAnim(plrSh);
//		playerC.legs = Pifagor.makePlrAnim(plrJeans);
//		playerC.boots = Pifagor.makePlrAnim(plrBoots);
//		playerC.hands = Pifagor.makePlrAnim(plrHands);
//		playerC.hat = Pifagor.makePlrAnim(plrHat);
//		playerC.onehaxe = Pifagor.makePlrAnim(plrHAxe);
//		playerC.spear = Pifagor.makePlrAnim(plrSpear);
		//		playerC.setHomePoint(palatk);

		//		sWorld.objecz.add(playerC);
		consFac = null;
		//(World.chunksize/2)*32
		playerC.x = (World.chunksize/2)*32;
		playerC.y = (World.chunksize/2)*32;



		//		MapManager.initMap();

		upRen = new ArrayList<Entity>();
		downRen = new ArrayList<Entity>();


		//		placeObjcts();

		//		setleAnimals();
	}


	public static void makeNPCAnim(Charakter charajh, int var) {
		charajh.corpusA = Pifagor.makePlrAnim(plr);
		charajh.shirt = Pifagor.makePlrAnim(plrSh);
		charajh.legs = Pifagor.makePlrAnim(plrJeans);
		charajh.boots = Pifagor.makePlrAnim(plrBoots);
		charajh.hands = Pifagor.makePlrAnim(plrHands);
		charajh.hat = Pifagor.makePlrAnim(plrHat);
		charajh.onehaxe = Pifagor.makePlrAnim(plrHAxe);
		charajh.spear = Pifagor.makePlrAnim(plrSpear);
	}

	//	private void placeObjcts() {
	//
	//		int elknum = (int) ((map.length*4) * (Math.random()+0.2));
	//		for (int num = 0; num < elknum; num++) {
	//
	//			float x = (float) ((map.length * 32) * Math.random());
	//			float y = (float) ((map.length * 32) * Math.random());
	//			double rnd = Math.random();
	//			byte type;
	//			if (rnd < 0.5) type = 0; // elka
	//			else type = 1; // kamen
	//			Minable prdmt = new Minable(x, y, type, 1);
	//			World.objecz.add(prdmt);
	//		}
	//		for (int i = 0; i < World.objecz.size(); i++) {
	//
	//			float min = World.objecz.get(i).y;
	//			int imin = i;
	//			for (int j = i; j < World.objecz.size(); j++) {
	//
	//				if (World.objecz.get(j).y < min) {
	//					min = World.objecz.get(j).y;
	//					imin = j;
	//				}
	//			}
	//
	//			if (i != imin) {
	//				Entity temp = World.objecz.get(i);
	//				World.objecz.set(i, World.objecz.get(imin));
	//				World.objecz.set(imin, temp);
	//			}
	//		}
	//	}
	//
	//	private void setleAnimals() {
	//		//jivotnii
	//		int jiv = 45;
	//		for (int num = 0; num < jiv; num++) {
	//			Animal prdmt = new Animal(0, deerAttack.copy(), deerDyin.copy(),deerWalk.copy());
	//			prdmt.x = (float) ((map.length * 32) * Math.random());
	//			prdmt.y = (float) ((map.length * 32) * Math.random());
	//			World.objecz.add(prdmt);
	//			World.animalz.add(prdmt);
	//		}
	//		int jiv2 = 10;
	//		for (int numa = 0; numa < jiv2; numa++) {
	//			Animal prdmts = new Animal(1, bearAttack.copy(),bearDyin.copy(),bearWalk.copy());
	//			prdmts.x = (float) ((map.length * 32) * Math.random());
	//			prdmts.y = (float) ((map.length * 32) * Math.random());
	//
	//			World.objecz.add(prdmts);
	//			World.animalz.add(prdmts);
	//		}
	//
	//	}

	//
	private void makeGroundDraw() {

		float chX = scrWi/2;
		float chY = scrHe/2;



		int cX = (int) (playerC.x / 32);
		int cY = (int) (playerC.y / 32);

		for (int x = 0; x < xGW; x++) {
			for (int y = 0; y < yGW; y++) {

				//kotr
				int drCX = (int) (x+cX-xGWm);
				int drCY = (int) (y+cY-yGWm);
				//
				drawgrnd[x][y] = world.getColor(drCX, drCY);
			}
		}
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
		//start world load\gen;

		if (!startload && FileManager.loadObject(world.currCHXY[0], world.currCHXY[0], 1) == null) {
			mapMan = new MapManager(0,0);
			mapMan.start();
		}

		startload = true;
		if (!worldIsReady) 
			return;


		makerenlists(gc);
		World.deleteQuery();
		World.addQuery();

		if (playerC.homePoint != null && Pifagor.getDist(playerC, playerC.homePoint) > 200) {
			drawDir = true;
			drawDirRot = (float) Pifagor.getDirectionTo(playerC, playerC.homePoint);
			ykaz.setRotation((float) Math.toDegrees(drawDirRot) );
			//			System.out.println(drawDirRot);
		} else drawDir = false;
		//		System.out.println(" player rot = " + playerC.rot );

		if (Config.gamecfgch) {
			font = Config.font;
			xScale = Config.xScale;
		}


		if (!isIntrd1) {

			isIntrd1 = true;

		}




		if (!gOver && isIntrd1 && gc.hasFocus() && !gc.isPaused()) {
			//FIXME TickTimer
			//			long starata = System.currentTimeMillis();
			//			long endn = System.currentTimeMillis() - starata;
			//			 endn = System.currentTimeMillis() - starata;
			//				if (endn > 1)
			//				System.out.println("    map refresh " + endn );



			rfrshTimer+=delta;
			//			oRfrshTimer += delta;
			//			s
			//			if ( oRfrshTimer >= 1500 ) {
			//				//				System.out.println("AsK or");
			//				//				MapManager.refreshOCh();
			//				mapMan.askObjRefr();
			//				oRfrshTimer = 0;
			//			}



			if ( rfrshTimer > 5000) {
				EncounterMan.randomAnimalSpawn(0, 0);
			}
			//			World.checkmaps();




			playerC.playerTurn(delta);
			activePribors(delta);


			mouz.setXY(DasGame.mX, DasGame.mY);
			mouz.checkHilit();

			for (int sld = 0; sld < World.sledi.size(); sld ++) {
				Sled slda = World.sledi.get(sld);
				slda.lifespan -= delta;

				if (slda.enttype == 9) {
					Sprtefct efc = (Sprtefct) slda;
					efc.refresh(delta);
				}

				if (World.sledi.get(sld).lifespan < 0) {
					World.removeEntityFromWorld(World.sledi.get(sld));
					//					World.objecz.remove(slda);
					//					World.sledi.remove(sld);
				}

			}
			//			long starata = System.currentTimeMillis();

			//			 endn = System.currentTimeMillis() - starata;
			//				if (endn > 1)
			//				System.out.println("    map refresh " + endn );

			animalDelta(delta);

			//			long endn = System.currentTimeMillis() - starata;
			//			if (endn > 1)
			//				System.out.println("    animal delta " + endn );

			for (int msl = 0; msl < World.misll.size(); msl ++ ) {
				Missile misl = World.misll.get(msl);

				if (!misl.stuck && !CollisionMan.checkCollison(misl)) {

					Pifagor.moveByRot(misl, delta);
					misl.fly(delta);
				}

			}

			//Soind


			//			if (!playerC.makestep && playerWalk.getFrame() == 5  && playerC.moving ||
			//					playerWalk.getFrame() == 1  && playerC.moving) {
			//				//playerC.makestep = true;
			//				playSound(0, playerC);
			//				switch (playerWalk.getFrame()) {
			//				case 1:
			//					if (!plLeftSl ) {
			//						if (!playerC.inWater)	makeSled(playerC.x-4,playerC.y, (byte) 0, playerC.rot,false);
			//						plLeftSl = true;
			//					}
			//					break;
			//				case 5:
			//					if (plLeftSl) {
			//						if (!playerC.inWater)	makeSled(playerC.x+4,playerC.y, (byte) 0, playerC.rot,true);
			//						plLeftSl = false;
			//					}
			//					break;
			//
			//				}
			//
			//			}
			//if (playerC.makestep && !plStep.playing())  playerC.makestep = false;

			//			if (playerAttack.getFrame() == 4) {
			//				playSound(3, playerC);
			//
			//			}
			//			if (spearAttack.getFrame() == 3) {
			//				playSound(4, playerC);
			//
			//			}

			//player


			if (playerC.hp <= 0) {
				playerC.attacking = false;
				playerC.moving = false;
				playerC.dyin = true;
			}


			// animazionen
			playerC.moving = false;
			//			managePlAnim();



			Input input = gc.getInput();

			mX = ((input.getMouseX()  - gc.getWidth()/2))/xScale + playerC.x;
			mY = ((input.getMouseY()  - gc.getHeight()/2))/xScale + playerC.y;
			//			System.out.println(" mxy " + mX + "x" + mY);
			//			float[] wtdd = wtd(mouz);
			//Construction
			if (construct) {
				delInterWin();
				if (playerC.recipeBuff.snap) {
					consFac.x = mX - mX%32 + 16;
					consFac.y = mY - mY%32 + 16;
				} else {
					consFac.x = mX;
					consFac.y = mY;
				}


				if ( Pifagor.getDist(mX, playerC.x, mY, playerC.y) <= 100 && !CollisionMan.checkCollison(consFac) && !World.inWater(consFac)) {
					hglitClr = new Color(0,255,0,0.4f);	
				} else hglitClr = new Color(255,0,0,0.4f);

			}

			//Input cntrl
			playerC.oldX = playerC.x;
			playerC.oldY = playerC.y;
			controlsManager(delta, input, game);

			playerC.move(delta);


			makeGroundDraw();

			//			endn = System.currentTimeMillis() - starata;
			//			if (endn > 1)
			//				System.out.println("    renlist delta " + endn );

		}
		else if (!drawvoid && isIntrd1) {
			if (!muteMusic) {
				System.out.println("igsray!");
				polka.fade(2000, 0, true);
				muteMusic = true;
			}
			if (gOver && !gameEnded) {
				Input input = gc.getInput();
				//				if (input.isKeyPressed(input.KEY_R)) restartgame();
			}
			Input input = gc.getInput();
			if (input.isKeyPressed(input.KEY_P)) {
				gc.setPaused(false);
			}


		}

		//System.out.println("up= " + upRen.size() + " down= " + downRen.size());

	}


	private void renlst(boolean one, GameContainer gc) {
		ArrayList <Entity> tgt;
		if (one) 
			tgt = upRen;
		else 
			tgt = downRen;

		for (int i = 0; i < tgt.size(); i++) {
			float min = tgt.get(i).y;
			int imin = i;
			for (int j = i; j < tgt.size(); j++) {
				if (tgt.get(j).y < min) {
					min = tgt.get(j).y;
					imin = j;
				}
			}
			if (i != imin) {
				Entity temp = tgt.get(i);
				tgt.set(i, tgt.get(imin));
				tgt.set(imin, temp);
			}
		}
		//		for (int i = 0; i < tgt.size(); i++) {
		//			float min = tgt.get(i).x;
		//			int imin = i;
		//			for (int j = i; j < tgt.size(); j++) {
		//				if (tgt.get(j).x < min) {
		//					min = tgt.get(j).x;
		//					imin = j;
		//				}
		//			}
		//			if (i != imin) {
		//				Entity temp = tgt.get(i);
		//				tgt.set(i, tgt.get(imin));
		//				tgt.set(imin, temp);
		//			}
		//		}
	}

	private void makerenlists(GameContainer gc) {

		upRen.clear();
		downRen.clear();

		for ( int a = 0; a < World.map.objects.size(); a ++) {
			Entity obj = World.map.objects.get(a);
			float wtdX = (obj.x - playerC.x) * xScale + gc.getWidth()/2;
			float wtdY = (obj.y - playerC.y)* xScale + gc.getHeight()/2;


			if (obj.y <= playerC.y &&  inFocus(wtdX, wtdY)) upRen.add(obj);
			else if (inFocus(wtdX, wtdY)) downRen.add(obj);
		}
		renlst(true, gc);
		renlst(false, gc);
	}

	private void openIW(int type) {
		if (!interwin) {
			interw = new InteractionWindow(type, null);
			interwin = true;

		} else {
			interwin = false;
			interw = null;
		}
	}

	private void controlsManager(int delta, Input input, StateBasedGame game) {
		if(input.isKeyPressed(Input.KEY_X)) {
			world.addEntityToWorld(new Drop(playerC.x, playerC.y, 0), false);
			drawdbg = !drawdbg;
			System.out.println("wobjects = " + World.map.objects.size() + " animalsu " + world.animalz.size());
		}

		if(input.isKeyPressed(Input.KEY_ESCAPE))
		{
			gcc.pause();
			game.enterState(MainKlaz.OPTIONSSTATE);
		}


		if (!playerC.dead && !playerC.dyin ) {

			World.controls.isButPressed(input);

			if(input.isKeyDown(Input.KEY_A) && !playerC.attacking) {
				playerC.addhd(LEFT);
				canclMove();
			}

			if(input.isKeyDown(Input.KEY_D) && !playerC.attacking)
			{
				playerC.addhd(RIGHT);
				canclMove();
			}

			if(input.isKeyDown(Input.KEY_W) && !playerC.attacking)
			{
				playerC.addhd(UP);
				canclMove();

			}
			if(input.isKeyDown(Input.KEY_S) && !playerC.attacking)
			{
				playerC.addhd(DOWN);
				canclMove();
			}

			//sprint
			if(input.isKeyDown(Input.KEY_LSHIFT) && !playerC.attacking && !playerC.sprint && playerC.stamina > 20)
			{
				playerC.beginSprint();
			}
			if (!input.isKeyDown(Input.KEY_LSHIFT) && playerC.sprint) {
				playerC.stopSprint();
			}

			if (!playerC.inWater) {

				//				if(!playerC.attacking && input.isKeyDown(Input.KEY_1))
				//				{
				//					//bulowa
				//					playerC.weaponWld = 1;
				//					canclMove();
				//
				//				}
				//				if(!playerC.attacking && input.isKeyDown(Input.KEY_2) && playerC.spearQ > 0)
				//				{
				//					//kopie
				//					playerC.weaponWld = 2;
				//					canclMove();
				//				}



				if(input.isKeyPressed(Input.KEY_M))
				{
					if (!muteMusic) {
						muteMusic = true;
						polka.stop();
					}
					else {
						muteMusic = false;
						polka.loop(1, 0.2f);
					}
				}


				if(input.isKeyPressed(Input.KEY_C))
				{
					openIW(2);
				}
				if(input.isKeyPressed(Input.KEY_V))
				{
					openIW(4);
				}
				if(input.isKeyPressed(Input.KEY_B))
				{
					openIW(3);
				}
				//				// mouse input
				//				if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
				//
				//				}
				//				if (input.isMousePressed(Input.MOUSE_RIGHT_BUTTON)) {
				//					//					if (!World.acbar && !construct && !interwin) {
				//					//
				//					//						akbar = new ActionBar(input.getMouseX(), input.getMouseY());
				//					//						World.acbar = true;
				//					//
				//					//						mouz.setXY(mX, mY);
				//					//						CollisionMan.checkCollison(mouz);
				//					//
				//					//					} else if (World.acbar) {
				//					//						delActBar();
				//					//					} else if (construct) {
				//					//						playerC.recipeBuff = null;
				//					//						construct = false;
				//					//						consFac = null;
				//					//					}
				//				}

				if (World.acbar && akbar.highlite(input.getMouseX(), input.getMouseY()))
				{
				}
				else delActBar();

				if (interwin) {
					interw.highlite(input.getMouseX(), input.getMouseY());
				}
			}
		}
	}

	private void animalDelta(int delta) {

		for (int a = 0; a < World.animalz.size(); a++ ) {
			Mobster animal = World.animalz.get(a);

			animal.think(delta);


		}
	}
	//private void credits(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
	//	if (!isIntrd1  || drawEnd) {
	//		if(!drawEnd
	//				 && cuttimer < 2000)
	//			introbg.draw(0,0,xScale);
	//		else {
	//			if (!drawvoid)
	//			cutscene.getSprite(cutnum, 0).draw(0, 0, xScale);
	//			else {
	//				g.setColor(Color.white);
	//				g.drawString(" CREDITS: ", 80*xScale, 0*xScale);
	//				g.drawString(" SPASIBO ZA IGRY! ", 80*xScale, 10*xScale);
	//				g.drawString("Vpadlo prikruchivat russkie shrifty, lol.", 80*xScale, 20*xScale);
	//				g.drawString("Sound- www.FreeSound.org", 80*xScale, 40*xScale);
	//				g.drawString("Drugie zapisal sam, takie dela.", 80*xScale, 50*xScale);
	//				g.drawString("Music - Spazzmatica Polka by Kevin MacLeod" , 80*xScale, 70*xScale);
	//				g.drawString("www.incompetech.com/music/royalty-free/" , 85*xScale, 80*xScale);
	//
	//				g.drawString("Sprites - MDKed." , 80*xScale, 100*xScale);
	//				g.drawString("Programming - MDKed." , 80*xScale, 110*xScale);
	//				g.drawString("Etc - x-and1988, MDKed." , 80*xScale, 120*xScale);
	//
	//				g.drawString("Special for GcUp.ru 2012-2013." , 80*xScale, 230*xScale);
	//
	//
	//
	//
	//			}
	//		}
	//	}
	//}
	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		
		g.setFont(font);


		float chX = gc.getWidth()/2;
		float chY = gc.getHeight()/2;

		float cX = playerC.x / 32;
		float cY = playerC.y / 32;
		//System.out.println(" Player locCh= " + cX + "x" + cY + " xy= " + playerC.x + "x" +  playerC.y );


		// otrisovken polen, ja-ja
		int xGW = 30, yGW = 22, xGWm = xGW/2, yGWm = yGW/2;

		for (int x = 0; x < xGW; x++) {
			for (int y = 0; y < yGW; y++) {

				//kotr
				//				int drCX = (int) (x+cX-xGWm);
				//				int drCY = (int) (y+cY-yGWm);
				//
				//				int drX = (int) ((( drCX * 32)  - playerC.x)*xScale  + chX);
				//				int drY =(int) ((( drCY * 32) - playerC.y)*xScale + chY);

				//				getGround(MapManager.getColor(drCX, drCY)).draw(drX, drY, xScale);
				//				getGround(world.getColor(drCX, drCY)).draw(drX, drY, xScale);
				int drCX = (int) (x+cX-xGWm);
				int drCY = (int) (y+cY-yGWm);

				int drX = (int) ((( drCX * 32)  - playerC.x)*xScale  + chX);
				int drY =(int) ((( drCY * 32) - playerC.y)*xScale + chY);

				getGround(drawgrnd[x][y]).draw(drX, drY, xScale);

				if (drawdbg) {
					int[] xy = Pifagor.getXchYch(drCX, drCY);
					MapChunk target = World.map;
					//					int[] xx = Pifagor.getXYminCH(drCX, drCY);
					font.drawString(drX, drY,drCX +"x" + drCY +"\n"
							+ target.biome
							, Color.black);
					int[] ala = Pifagor.getXYminCHfd(drCX, drCY);
					//					if (target.gridMap.get(ala[0], ala[1]) == GridMap.WALL)
					font.drawString(drX+ 1, drY+32, "xy=" + ala[0] + "x" + ala[1] +"\n" + Integer.toString((int) target.gridMap.get(ala[0], ala[1])), Color.red);

					//					if (!rdyMap) {
					//						minimap = new Color[World.chunksize][World.chunksize];
					//						for (int ax = 0; ax < World.chunksize; ax++) {
					//							for (int ay = 0; ay < World.chunksize; ay++) {
					////								MapChunk mapa = MapGen.getChunkXY((int)playerC.x, (int)playerC.y);
					//								float clr = MapGen.byteToDouble(target.heightMap[ax][ay]).floatValue();
					//								if ( clr > World.waterlevel)
					//									minimap[ax][ay] = new Color(clr, clr, clr);
					//								else
					//									minimap[ax][ay] = new Color(0f, 0f, 1f);
					//
					//							}
					//						}
					//						rdyMap = true;
					//					} else
					//						for (int ax = 0; ax < World.chunksize; ax++) {
					//							for (int ay = 0; ay < World.chunksize; ay++) {
					//								g.setColor(minimap[ax][ay]);
					//								g.fillRect(10+ax, 10+ay, 1, 1);
					//							}
					//						}
					//					g.setColor(Color.red);
					//					g.fillRect(10+(playerC.x/32), 10+(playerC.y/32), 1, 1);
				}
			}
		}


		for (int a = 0; a < upRen.size(); a++) {
			drawEntity(upRen.get(a));
		}

		if (drawDir) {
			drawDirections(chX, chY);
		}

		drawPlayer(chX, chY);



		if (construct) {
			drawConstruct(playerC.recipeBuff, gc);
		}

		for (int b = 0; b < downRen.size(); b++) {
			drawEntity(downRen.get(b));
		}


		//	HUD

		//		g.setColor(Color.white);
		//		heart.draw(10*xScale, 200 *xScale , 32*xScale, 32*xScale);
		g.drawString("HP - " + Integer.toString((int)playerC.hp) , 20*xScale, 210 *xScale);
		g.drawString("ST - " + Integer.toString((int)playerC.stamina) , 20*xScale, 222 *xScale);
		g.drawString("SPD - " + Float.toString(playerC.speed) , 20*xScale, 234 *xScale);

		//
		//		scoreTable.draw(256*xScale, 0, xScale); 
		//		g.drawString(Integer.toString((int)playerC.score), 276*xScale, 17*xScale);

		/*timer.draw(128*xScale, 0, xScale);
		if (time < timeEnd) {
			minleft = (int) (((timeEnd - time) / 1000)/60);
			secleft = (int) ((timeEnd - time)/1000 - minleft*60); 
		}
		g.drawString(Integer.toString(minleft), 154*xScale, 14*xScale);
		g.drawString(Integer.toString(secleft), 162*xScale, 14*xScale);
		 */
		//		ammo.draw(278*xScale, 200*xScale, xScale);
		//		ammonum.getSprite(playerC.spearQ, 0).draw(278*xScale, 200*xScale, xScale);

		//		if (gOver) gameOver.draw(96*xScale, 128*xScale, xScale);
		if (gOver && !gameEnded) 	{
			g.setColor(Color.black);
			g.drawString("Press R - to restart", 128*xScale, 80*xScale);
		}
		if (World.acbar) akbar.draw(g);
		if (interwin) interw.draw(g);
		if (txtcld) {
			//System.out.println("  drawTextC");
			textC.draw(g);
		}
		//buffs
		if (playerC.buffs.size() > 0) {
			for (int a=0; a < playerC.buffs.size(); a ++) {
				statIco.getSubImage(playerC.buffs.get(a).icon, 0).draw((10 + (17*a))*xScale, 10*xScale, xScale);
			}

		}
		if (gc.isPaused()) {
			g.setColor(new Color(0,0,0,0.2f));
			g.fillRect(0, 0, gc.getWidth(), gc.getHeight());
			g.setColor(Color.white);
			g.drawString("Пауза", 150*xScale, 120*xScale);
			g.drawString("Нажмите латинскую 'P'", 130*xScale, 130*xScale);
		}
		if (playerC.inWater) g.drawString(Float.toString(playerC.oxygen), chX, chY + 64);
	}

	private boolean inFocus(float wtdX, float wtdY) {
		if (wtdX >= -64*xScale && wtdX < gcc.getWidth()+64*xScale 
				&& wtdY >= -64*xScale && wtdY < gcc.getHeight()+64*xScale) return true;
		return false;
	}



	private void drawDirections(float chX, float chY) {
		float cdx = chX- (16*xScale);
		float cdy = chY - (16*xScale);

		ykaz.draw(cdx, cdy, xScale);

	}

	private void drawConstruct(Recipe recipeBuff, GameContainer gc) {
		
		float[] wtd = wtd(consFac);

		//			wtd[0] = (int) (wtd[0]/32) * 32;
		//			wtd[1] = (int) (wtd[1]/32) * 32;

		switch (recipeBuff.getOutType()) {
		case 1:
			drawCntrd(palatka, wtd[0], wtd[1], hglitClr, xScale);
			//			palatka.draw(-32, -64, hglitClr);
			break;
		case 2:
			drawCntrd(workshopA, wtd[0], wtd[1], hglitClr, xScale);
			//			lesopilk.draw(wtd[0]-32, wtd[1]-64, hglitClr);
			break;
		case 6:
			drawCntrd(fire.getImage(0), wtd[0], wtd[1], hglitClr, xScale);

			//			firePlace.draw(wtd[0]-16, wtd[1]-32, hglitClr);
			break;
		case 11:
			drawCntrd(walls1.getImage(0), wtd[0], wtd[1] + 16*xScale, hglitClr, xScale);
			break;
		}
	}

	private void drawPlayer(float chX, float chY) {

		int cdx = (int) chX;
		int cdy = (int) chY;
		//		System.out.println(playerC.actionType);
		//		drawPlrAnim(cdx,cdy, playerC.actionType);
		playerC.drawAnim(cdx,cdy);


	}
	public static int getADir(Entity ent) {
		float rot = 0;
Charakter chalk = (Charakter) ent;
//		switch (ent.enttype) {
//		case 1:
			rot = (float) Math.toDegrees(Pifagor.corrRot(chalk.rot));
//
//			break;
//		case 6:
//		case 12:
//			Player plr = (Player) ent;
//			rot = (float) Math.toDegrees(Pifagor.corrRot(plr.rot));
//			break;
//		}
			
//System.out.println(" rota = " + rot );

		if (rot >= -45 && rot < 45 ) return UP;
		else if ( rot >= 315) return UP;
		else if (rot >= -135 && rot < -45 ) return LEFT;
		else if ( rot >= 225 && rot < 315) return LEFT;
		else if (rot >= -225 && rot < -135 ) return DOWN;
		else if (rot >= 135 && rot < 225 ) return DOWN;
		else  return RIGHT;


	}

	//	private void drawPlrAnim(int wtdX, int wtdY, int anim) {
	//	
	//	}





	public static void playSound(int numer, Entity ist) {
		float[] wts = wtd(ist);
		float x = wts[0];
		float y = wts[1];

		float pitch;
		pitch = (float) (0.9 + (Math.random() * 0.1));

		switch (numer) {
		case 0:
			if (!plStep.playing()) {
				plStep.playAt(pitch, 0.01f,x,y,0.0f);
			}
			break;
		case 1:
			//if (!deerStp.playing()) {
			deerStp.playAt(pitch, 0.01f,x,y,0.0f);
			//}
			break;
		case 2:
			//if (!bearStp.playing()) {
			bearStp.playAt(pitch, 0.15f,x,y,0.0f);
			//}
			break;
		case 3:
			if (!swing.playing()) {
				swing.playAt(pitch, 0.3f,x,y,0.0f);
			}
			break;
		case 4:
			//if (!thrw.playing()) {
			thrw.playAt(pitch, 0.2f,x,y,0.0f);
			//	}
			break;
		case 5:
			//if (!spHit.playing()) {
			spHit.playAt(pitch, 0.4f,x,y,1.0f);
			//	}
			break;
		case 6:
			if (!dmgs.playing()) {
				dmgs.playAt(pitch, 0.4f,x,y,0.0f);
			}
			break;
		case 7:
			//	if (!deerAgr.playing()) {
			deerAgr.playAt(pitch, 0.4f,x,y,0.0f);;
			//	}
			break;
		case 8:
			//if (!deerAtk.playing()) {
			deerAtk.playAt(pitch, 0.4f,x,y,0.0f);
			//}
			break;
		case 9:
			//	if (!deerDmg.playing()) {
			deerDmg.playAt(pitch, 0.4f,x,y,1.0f);
			//	}
			break;
		case 10:
			//if (!deerDin.playing()) {
			deerDin.playAt(pitch, 0.4f,x,y,0.0f);
			//}
			break;
		case 11:
			//if (!bearAgr.playing()) {
			bearAgr.playAt(pitch, 0.4f,x,y,0.0f);
			//}
			break;
		case 12:
			//if (!bearAtk.playing()) {
			bearAtk.playAt(pitch, 0.4f,x,y,0.0f);
			//}
			break;
		case 13:
			//if (!bearDmg.playing()) {
			bearDmg.playAt(pitch, 0.4f,x,y,1.0f);
			//}
			break;
		case 14:
			//if (!bearDin.playing()) {
			bearDin.playAt(pitch, 0.4f,x,y,0.0f);
			//}
			break;
		case 15:
			//if (!maceHit.playing()) {
			maceHit.playAt(pitch, 1f,x,y,1.0f);
			//			System.out.println(" ydar dubinoy!");
			//}
			break;
		case 16:
			treechop.playAt(pitch, 1f, x, y, 1.0f);
			//			System.out.println(" ydar toporom!");
			break;
		case 17:
			plHit.playAt(pitch, 1f, x, y, 1.0f);
			break;

		}
	}



	public static float[] wtd(Entity obj) {
		float[] wtd = {(obj.x - World.player.x)*xScale  + (gcc.getWidth()/2), (obj.y - World.player.y)*xScale + (gcc.getHeight()/2) };
		return	wtd;
	}

	public static void drawFlosh(Image img, float wtdX, float wtdY, float scale) {
		img.drawFlash(wtdX- (img.getWidth()/2)*scale-3, wtdY-img.getHeight()*scale-3, img.getWidth()*scale+6, img.getHeight()*scale+6);
	}

	public static void drawCntrd(Image img, float wtdX, float wtdY, float scale) {
		img.draw(wtdX- (img.getWidth()/2)*scale, wtdY-img.getHeight()*scale, scale);
	}

	public static void drawCntrd(Image img, float wtdX, float wtdY, boolean mirror) {
		if (!mirror)
			img.draw(wtdX- img.getWidth()/2, wtdY-img.getHeight());
		else
			img.draw(wtdX- (img.getWidth()/2) + img.getWidth(), wtdY-img.getHeight(), -img.getWidth(), img.getHeight());

	}

	public static void drawCntrd(Image img, float wtdX, float wtdY, boolean mirror, float scale) {
		if (!mirror)
			img.draw(wtdX- (img.getWidth()/2)*scale, wtdY-img.getHeight()*scale, scale);
		else
			img.draw(wtdX - ((img.getWidth()*scale)/2) + img.getWidth()*scale+ (1*scale), wtdY-img.getHeight()*scale, -img.getWidth()*scale, img.getHeight()*scale);

	}

	public static void drawCntrd(Image img, float wtdX, float wtdY, Color c, boolean mirror) {
		if (!mirror)
			img.draw(wtdX- img.getWidth()/2, wtdY-img.getHeight(), c);
		else
			img.draw(wtdX- (img.getWidth()/2) + img.getWidth(), wtdY-img.getHeight(), -img.getWidth(), img.getHeight(), c);
	}
	public static void drawCntrd(Image img, float wtdX, float wtdY, Color c, boolean mirror, float scale) {
		if (!mirror)
			drawCntrd(img, wtdX, wtdY, scale);
		else
			img.draw(wtdX- (((img.getWidth()/2) + img.getWidth())*scale), wtdY-(img.getHeight()*scale), -img.getWidth()*scale, img.getHeight()*scale, c);
	}

	public static void drawCntrd(Image img, float wtdX, float wtdY, Color clr) {
		img.draw(wtdX- img.getWidth()/2, wtdY-img.getHeight(), clr);
	}

	public static void drawCntrd(Image img, float wtdX, float wtdY, Color clr, float scale) {
		img.draw(wtdX- (img.getWidth()/2)*scale, wtdY-img.getHeight()*scale, scale, clr);
	}

	public static void drawCntrd(Image img, float wtdX, float wtdY, float scale, Color clr) {
		img.draw(wtdX- img.getWidth()/2, wtdY-img.getHeight(), scale, clr);
	}
	public static void drawCntrd(Image img, float wtdX, float wtdY, float scale, Color clr, float xScale) {
		img.draw(wtdX- (img.getWidth()*xScale)/2, wtdY-(img.getHeight()*xScale), scale+xScale, clr);
	}

	public static void drawCntrd(Animation anim, float wtdX, float wtdY) {
		anim.draw(wtdX- anim.getWidth()/2, wtdY-anim.getHeight());
	}
	public static void drawText(String text, float wtdX, float wtdY, int color) {
		Color one = Color.white;
		Color two = Color.black;
		switch (color ) {
		case 1:
			one = Color.black;
			two = Color.red;
		}
		gcc.getGraphics().getFont().drawString(wtdX-1, wtdY-1, text, one);
		gcc.getGraphics().getFont().drawString(wtdX, wtdY, text, two);
		//		gcc.getGraphics().drawString(text, wtdX, wtdY);

	}

	public static void drawCntrd(Animation anim, float wtdX, float wtdY, float scale) {
		anim.draw(wtdX- (anim.getWidth()*scale)/2, wtdY-anim.getHeight()*scale, anim.getWidth()*scale, anim.getHeight()*scale);
	}

	public static void drawCntrd(Animation anima, float wtdX, float wtdY,
			Color clr) {
		anima.draw(wtdX- anima.getWidth()/2, wtdY-anima.getHeight(), clr);
	}
	public static void drawCntrd(Animation anima, float wtdX, float wtdY,
			Color clr, float scale) {
		anima.draw(wtdX- anima.getWidth()/2, wtdY-anima.getHeight(), anima.getWidth()*scale, anima.getHeight()*scale, clr);
	}

	public static void drawCntrd(Animation anim, float wtdX, float wtdY, boolean mirror) {
		if (!mirror)
			anim.draw(wtdX- anim.getWidth()/2, wtdY-anim.getHeight());
		else 
			anim.draw(wtdX- (anim.getWidth()/2) + anim.getWidth(), wtdY-anim.getHeight(), -anim.getWidth(), anim.getHeight());
	}

	public static void drawCntrd(Animation anim, float wtdX, float wtdY, boolean mirror, float scale) {
		if (!mirror)
			drawCntrd(anim, wtdX, wtdY, scale);
		else 
			anim.draw(wtdX - ((anim.getWidth()*scale)/2) + anim.getWidth()*scale + (1*scale), wtdY-anim.getHeight()*scale, -anim.getWidth()*scale, anim.getHeight()*scale);
	}
	//	private void drawCntrd(Animation anim, float wtdX, float wtdY, Color c, boolean mirror) {
	//		if (!mirror)
	//			anim.draw(wtdX- anim.getWidth()/2, wtdY-anim.getHeight(), c);
	//		else 
	//			anim.draw(wtdX- (anim.getWidth()/2) + anim.getWidth(), wtdY-anim.getHeight(), -anim.getWidth(), anim.getHeight(), c);
	//
	//	}
	//	private void drawMCntrd(Animation anim, float wtdX, float wtdY) {
	//		anim.draw(wtdX- (anim.getWidth()/2) + anim.getWidth(), wtdY-anim.getHeight(), -anim.getWidth(), anim.getHeight());
	//	}
	//	private void drawMCntrd(Image img, float wtdX, float wtdY) {
	//		img.draw(wtdX- (img.getWidth()/2) + img.getWidth(), wtdY-img.getHeight(), -img.getWidth(), img.getHeight());
	//	}
	//	private void drawMCntrd(Image img, float wtdX, float wtdY, Color c) {
	//		img.draw(wtdX- (img.getWidth()/2) + img.getWidth(), wtdY-img.getHeight(), -img.getWidth(), img.getHeight(), c);
	//	}

	private void drawEntity(Entity obj) {
		float[] wtd = wtd(obj);
		int wtdX = (int) (wtd[0]);
		int wtdY = (int) (wtd[1]);

		//		Image hilito = null;


		if ( inFocus(wtdX, wtdY)) {
			

			if (drawdbg) {
				int[] kotr = Pifagor.getKOTRfromfXY(obj.x, obj.y);
				//			int[] kotr = Pifagor.getOtnXYKotr(obj.x, obj.y);
				font.drawString(wtdX, wtdY, kotr[0] +"x" + kotr[1], Color.red);
				//			System.out.println( "text wtdxy = " + wtdX + "x" + wtdY);
				font.drawString(wtdX, wtdY, "x");
			}
			
			
			
			switch (obj.enttype) {

			case 0:
				Minable so = (Minable) obj;
				switch (so.mtype) {
				case 0:
					if (obj.sveto) 
						drawFlosh(tree, wtdX, wtdY, xScale);
					drawCntrd(tree, wtdX , wtdY, xScale);

					break;

				case 1:
					if (obj.sveto) 
						drawFlosh(stone, wtdX, wtdY, xScale);
					drawCntrd(stone, wtdX , wtdY, xScale);

					break;

					//				case 6:


				case 2:
					//zelenii kust
					if (obj.sveto) 
						drawFlosh(bushF, wtdX, wtdY, xScale);
					drawCntrd(bushB, wtdX, wtdY, xScale);
					drawCntrd(bushF, wtdX, wtdY,  so.getFColor(), xScale);
					break;

				case 3:
					//lisii kust
					if (obj.sveto) 
						drawFlosh(bushB, wtdX, wtdY, xScale);
					drawCntrd(bushB, wtdX, wtdY, xScale);
					break;

				case 4:
					if (obj.sveto) 
						drawFlosh(grassB, wtdX, wtdY, xScale);
					drawCntrd(grassB,wtdX, wtdY, so.getFColor(), xScale);
					break;

				case 5:
					if (obj.sveto) 
						drawFlosh(grassB, wtdX, wtdY, xScale);
					drawCntrd(grassB,wtdX, wtdY, so.getFColor(), xScale);
					drawCntrd(grassF,wtdX, wtdY, so.getSColor(), xScale);
					break;
				case 6:
					if (obj.sveto) 
						drawFlosh(treeB, wtdX, wtdY, xScale);
					drawCntrd(treeB, wtdX, wtdY, xScale);
					drawCntrd(treeF,wtdX, wtdY, so.getFColor(), xScale);
					break;
				case 7:
					if (obj.sveto) 
						drawFlosh(treeB, wtdX, wtdY, xScale);
					drawCntrd(treeB, wtdX, wtdY, xScale);
					break;


				}
				break;

			case 1:
			case 12:

				Mobster mob = (Mobster) obj;

				if (drawdbg && mob.tgtLoc != null) {
					Graphics gg = gcc.getGraphics();
					gg.setLineWidth(10);
					gg.setColor(Color.orange);
					int[] xyu = Pifagor.getXYfromKOTR(Pifagor.getCh(mob), mob.tgtLoc.getX(), mob.tgtLoc.getY());
					Entity tgt = new Entity();
					tgt.x = xyu[0];
					tgt.y = xyu[1];
					float[]	wtdt = wtd(tgt);
					//					Entity target = 
					gg.drawLine(wtdX, wtdY, wtdt[0], wtdt[1]);
				}
				if (obj.sveto) 
					drawFlosh(shadow, wtdX, wtdY, xScale);
				drawCntrd(shadow, wtdX- 16,  wtdY-2*xScale, 2, new Color(1,1,1,0.2f), 1);

				//				drawCntrd(bearDead, wtdX, wtdY, xScale);
				mob.drawAnim(wtdX, wtdY);

				//				if (anim.moving) drawCntrd(anim.moveani, wtdX, wtdY, anim.faceRight, xScale); 
				//				else if ( anim.attacking ) drawCntrd(anim.atani,wtdX, wtdY, anim.faceRight, xScale);
				//				else if ( anim.dying ) drawCntrd(anim.deadani, wtdX, wtdY, anim.faceRight, xScale); 
				//				else
				//					drawCntrd(anim.stand, wtdX, wtdY, anim.faceRight, xScale);


				break;

			case 2:
				//factory
				Facility fac = (Facility) obj;
				switch (fac.facType) {
				case 1:
					if (obj.sveto) 
						drawFlosh(palatka, wtdX, wtdY, xScale);
					drawCntrd(palatka, wtdX, wtdY, xScale);
					break;
				case 2:
					if (obj.sveto) 
						drawFlosh(workshopA, wtdX, wtdY, xScale);
					drawCntrd(workshopA, wtdX, wtdY, xScale);
					break;
				}

				break;

			case 5:
				Missile misl = (Missile) obj;

				drawCntrd(shadow, wtdX-8,  wtdY, 2, new Color(1,1,1,0.2f));
				Image msl = missiles.getImage(0);
				msl.setRotation((float) Math.toDegrees(misl.rot) - 180);
				drawCntrd(msl, wtdX, wtdY - misl.z+16, xScale);
				break;

			case 7:
				DragblObj dro = (DragblObj) obj;
				//				System.out.println("draw deer drgbl");

				switch (dro.resType) {
				case 0:
					if (obj.sveto) 
						drawFlosh(drop.getSprite(0, 0), wtdX, wtdY, xScale);
					drawCntrd(drop.getSprite(0, 0), wtdX, wtdY, xScale);
					//derevo
					break;
				case 2:
					//olen
					//					System.out.println("draw deer ");
					if (obj.sveto) 
						drawFlosh(deerDead, wtdX, wtdY, xScale);
					drawCntrd(deerDead, wtdX, wtdY, xScale);
					break;
				case 3:
					//bear
					if (obj.sveto) 
						drawFlosh(bearDead, wtdX, wtdY, xScale);
					drawCntrd(bearDead, wtdX, wtdY, xScale);
					break;
				}
				break;

			case 3:
				Sled sld = (Sled) obj;

				if (sld.kind < 3) {
					sled.getSubImage(sld.kind, 0).setRotation(sld.rot);
					if (!sld.mirrd)
						drawCntrd(sled.getSubImage(sld.kind, 0), wtdX, wtdY, new Color(1f,1f,1f,0.5f*(sld.lifespan/20000f)), xScale);
					else drawCntrd(sled.getSubImage(sld.kind, 0), wtdX, wtdY, new Color(1f,1f,1f,0.5f*(sld.lifespan/20000f)), true, xScale);
				}
				else if (sld.kind > 2 && sld.kind < 6) {
					drawCntrd(blood.getSubImage(sld.kind-2, 0), wtdX, wtdY, new Color(1f,1f,1f,1f*(sld.lifespan/60000f)), xScale);
				} else 
				{
					float xfr = sld.lifespan/5000f;
					drawCntrd(shadow, wtdX - ((shadow.getWidth()/2)*xfr), wtdY-((shadow.getHeight()/2)*xfr), 2*xfr, new Color(1f,1f,1f,0.07f*xfr), xScale- 0.5f);
				}
				break;

			case 8:
				Pribor prib = (Pribor) obj;
				switch (prib.ptype) {
				case 1:
					if (obj.sveto) 
						drawFlosh(fire.getImage(0), wtdX, wtdY, xScale);

					if (prib.active()) {
						drawCntrd(fire, wtdX, wtdY, xScale);
					} 
					drawCntrd(fire.getImage(0), wtdX, wtdY, xScale);
					break;
				}
				break;

			case 9:
				Sprtefct eff = (Sprtefct) obj;
				eff.draw();
				break;

			case 10:
				//drop
				Drop drp = (Drop) obj;
				switch (drp.ico) {
				case 0:
					if (obj.sveto) 
						drawFlosh(drop.getSprite(0, 1), wtdX, wtdY, xScale);
					drawCntrd(drop.getSprite(0, 1), wtdX, wtdY, xScale);
					break;
				}
				break;
			case 11:
				//wall
				Wall wol = (Wall) obj;
				Color clr = new Color(113, 56, 0);
				wtdY += 16*xScale;
				if (obj.sveto) 
					drawFlosh(walls1.getImage(wol.ico), wtdX, wtdY, xScale);
				drawCntrd(walls1.getImage(wol.ico), wtdX, wtdY, clr, xScale);
				break;

			}

		}
	}
	//	public static float getDRX(float x) {
	//		int xm = 64;
	//
	//		float wtdX = (x - World.player.x) * xScale + gcc.getWidth()/2;
	//		if (wtdX >= ((128 * 32) - xm)*xScale ) wtdX -= ((128 * 32)*xScale) ;
	//		if (wtdX < scrWi + ((xm - (128 * 32))*xScale) ) wtdX =  wtdX + ((128 * 32)*xScale);
	//
	//		return wtdX;
	//	}
	//
	//	public static float getDRY(float y) {
	//		int xm = 64;
	//		float wtdY = (y - World.player.y)* xScale + gcc.getHeight()/2;
	//		if (wtdY >= ((128 * 32) - xm)*xScale ) wtdY -= ((128 * 32)*xScale) ;
	//		if (wtdY < scrHe+ ((xm - (128 * 32))*xScale)  ) wtdY = wtdY + ((128 * 32)*xScale);
	//		return wtdY;
	//	}
	//	public static void spawnObj(int type) {
	//		Mobster prdmt;
	//		switch (type) {
	//		case 2:
	//			prdmt = new Mobster(type);
	//			prdmt.x = (float) ((128 * 32) * Math.random());
	//			prdmt.y = (float) ((128 * 32) * Math.random());
	//			World.objecz.add(prdmt);
	//			World.animalz.add(prdmt);
	//			break;
	//		case 3:
	//			prdmt = new Mobster(type);
	//			prdmt.x = (float) ((128 * 32) * Math.random());
	//			prdmt.y = (float) ((128 * 32) * Math.random());
	//			World.objecz.add(prdmt);
	//			World.animalz.add(prdmt);
	//			break;
	//		}
	//	}

	public static void makeSled(float x, float y, byte kind, float rot, boolean mrrd) {
		//System.out.println(" rot= " + rot);
		boolean mirored = mrrd;
		if (rot < 0 ) {
			if (mrrd) mirored = false; else mirored = true;
		}
		Sled sled = new Sled(x,y,kind,(float) Math.toDegrees(rot)-90, mirored);
		World.addEntityToWorld(sled, false);
		//		World.objecz.add(sled);
		//		World.sledi.add(sled);
	}

	public static void delActBar() {
		World.acbar = false;
		akbar = null;
		txtcld = false;
		textC = null;
	}
	public void delInterWin() {
		interwin = false;
		if (interw != null) {
			interw.removedrop();
			interw = null;
		}
	}

	public static void beginConstr(int type) {
		//System.out.println(" new consfac");
		switch (type) {
		case 1:
		case 2:
			consFac = new Facility(type,mX,mY);
			break;
		case 6:
			//kostr
			consFac = new Pribor(mX,mY,(byte) 1);
			break;
		case 11:
			consFac = new Wall(mX,mY, 0);
			break;
		}

		construct = true;
		constType = type;
	}


	public static void checkPribors() {
		boolean fire = false, wShopA = false;
		for (int a=0; a < World.pribrs.size(); a++) {
			Pribor prib = World.pribrs.get(a);
			if (prib.active() && Pifagor.getDist(prib.x, World.player.x, prib.y, World.player.y) <= 50) {
				switch (prib.ptype) {
				case 1:
					//System.out.println(" игрок у огня");
					fire = true;
					break;
				}
			}
		}
		for (int b = 0; b < World.facs.size(); b++	) {
			Facility faca = World.facs.get(b);
			if (Pifagor.getDist(faca.x, World.player.x, faca.y, World.player.y) <= 50) {
				switch (faca.facType) {
				case 2:
					wShopA = true;
					break;
				}
			}
		}
		if (wShopA ) {
			World.player.nearWshpA = true;
		} else {
			World.player.nearWshpA = false;
		}

		//System.out.println("Fire : " + fire);
		if (fire && !World.player.nearFire) {
			//System.out.println(" игрок греется");
			World.player.tempRes += 70;
			World.player.nearFire = true;
		}
		else if (!fire && World.player.nearFire) {
			//System.out.println(" игрок остужается");
			World.player.tempRes -= 70;
			World.player.nearFire = false;
		}
	}

	private void activePribors(int delta) {
		for (int prib = 0; prib < World.pribrs.size(); prib++) {
			Pribor prbr = World.pribrs.get(prib);
			if (prbr.isFueled()) {
				if (!prbr.burn(delta)) checkPribors();
			}
		}
	}

	private void canclMove() {

		checkPribors();
		delActBar();
		delInterWin();
	}



	//	public void restartgame() {
	//
	//		playerC.x = 1500;
	//		playerC.y = 1500;
	//		playerC.hp = 100;
	//		playerC.temp = 100;
	//		playerC.hung = 100;
	//		playerC.stamina = 100;
	//		playerC.dyin = false;
	//		playerC.dead = false;
	//		//		playerC.score = 0;
	//		playerC.moving = false;
	//		playerC.attacking = false;
	//		playerC.stopPullin();
	//		//		playerDyin.restart();
	//
	//
	//		if (World.misll.size() > 0 ) {
	//			for (int a = 0 ; a < World.misll.size(); a++) {
	//				World.objecz.remove(World.misll.get(a));
	//			}
	//			World.misll.clear();
	//		}
	//
	//		time = 0;
	//		gOver = false;
	//
	//		muteMusic = false;
	//
	//		polka.loop(1, 0.0f);
	//		polka.fade(2000, 0.2f, false);
	//	}


	private Image getGround(short colorNum) {
		int ret = 0;
		//		System.out.println( "::: " + colorNum);
		switch ( colorNum ) {
		case 1000:
			//presnaya
			ret = 102;
			break;
		case 1010:
			ret = 103;
			break;
		case 1020:
			ret = 104;
			break;

		case 900 :
			//wasser
			ret = 99;
			break;
		case 910 :
			//wasser
			ret = 100;
			break;
		case 920 :
			//wasser
			ret = 101;
			break;

		case 404  :
			//снег
			ret = 0;
			break;
		case 414  :
			//снег
			ret = 1;
			break;
		case 424  :
			//снег
			ret = 2;
			break;
			//
		case 403 :
			//tundra
			ret = 3;
			break;
		case 413 :
			//tundra
			ret = 4;
			break;
		case 423 :
			//tundra
			ret = 5;
			break;
			//
		case 304:
			//pustosh
			ret = 6;
			break;
		case 314:
			//pustosh
			ret = 7;
			break;
		case 324:
			//pustosh
			ret = 8;
			break;
			//
			//		case 104:
			//			//suha zemlya
			//			ret = 9;
			//			break;
			//		case 114:
			//			//suha zemlya
			//			ret = 10;
			//			break;
			//		case 124:
			//			//suha zemlya
			//			ret = 11;
			//			break;
			//
			//		case 503:
		case 402:
			//taiga
			ret = 12;
			break;
			//		case 513:
		case 412:
			//taiga
			ret = 13;
			break;
			//		case 523:
		case 422:
			//taiga
			ret = 14;
			break;
			//
			//		case 303:
		case 401:
			//kustarniki
			ret = 45;
			break;
			//		case 313:
		case 411:
			//kustarniki
			ret = 46;
			break;
			//		case 323:
		case 421:
			//kustarniki
			ret = 47;
			break;
			//
			//		case 102:
		case 302:
		case 303:
			//ym pustinya
			ret = 48;
			break;
		case 312:
		case 313:
			//ym pustinya
			ret = 49;
			break;
			//		case 122:
		case 322:
		case 323:
			//ym pustinya
			ret = 50;
			break;
			//
		case 204:
			//ym. dojd. les
			ret = 51;
			break;
		case 214:
			//ym. dojd. les
			ret = 52;
			break;
		case 224:
			//ym. dojd. les
			ret = 53;
			break;
			//
		case 202:
		case 203:
			//ym. list. les
			ret = 54;
			break;
		case 212:
		case 213:
			//ym. list. les
			ret = 55;
			break;
		case 222:
		case 223:
			//ym. list. les
			ret = 56;
			break;
			//
			//		case 201:
			//		case 202:
		case 201:
			//trava
			ret = 57;
			break;
			//		case 211:
			//		case 212:
		case 211:
			//trava
			ret = 58;
			break;
		case 221:
			//		case 222:
			//		case 322:
			//trava
			ret = 59;
			break;
			//
		case 101:
		case 102:
			//trop. dojd. les
			ret = 90;
			break;
		case 111:
		case 112:
			//trop. dojd. les
			ret = 91;
			break;
		case 121:
		case 122:
			//trop. dojd. les
			ret = 92;
			break;
			//
		case 103:
		case 104:
			//trop. sezon. les
			ret = 93;
			break;
		case 113:
		case 114:
			//trop. sezon. les
			ret = 94;
			break;
		case 123:
		case 124:
			//trop. sezon. les
			ret = 95;
			break;
			//
		case 301:
			//subtropic pus
			ret = 96;
			break;
		case 311:
			//subtropic pus
			ret = 97;
			break;
		case 321:
			//subtropic pus
			ret = 98;
			break;

		}

		return ground[ret];
	}






	@Override
	public int getID() {
		return stateID;
	}


}
