package net.mdked.Jrachgame.gamelvlz;

import net.mdked.Jrachgame.core.Config;
import net.mdked.Jrachgame.core.MainKlaz;
import net.mdked.Jrachgame.utils.DynImage;
import net.mdked.Jrachgame.utils.PropManager;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class MainMenuState extends BasicGameState{

	public int stateID;
//	private Image ,
//			;
	private DynImage planet, sun;
	float xScale = Config.xScale;
	private long tesat = 0;
	private boolean sHlt, oHlt, eHlt;

	public MainMenuState( int stateID)
	{
		this.stateID = stateID;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		Config.changeXScale(Float.parseFloat(PropManager.prop.getProperty("xScale")));

		int cX =gc.getWidth()/2, cY = gc.getHeight()/2;

		sun = new DynImage( new Animation(new SpriteSheet(new Image("img/intro/sun.png",false,Image.FILTER_NEAREST), 64, 64), 100 ), cX, cY, xScale);
		planet = new DynImage(new Image("img/intro/planet.png",false,Image.FILTER_NEAREST), cX,cY , xScale+2);


		//		mainmenu = new Image("img/menu/mainmenuscreen.gif",false,Image.FILTER_NEAREST);
		//		startGame = mainmenu.getSubImage(7, 184, 68, 51);
		//		loadGame = mainmenu.getSubImage(79, 183, 98, 52);
		//		options = mainmenu.getSubImage(185, 185, 75, 50);
		//		exit = mainmenu.getSubImage(269, 190, 43, 44);

	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		
		sun.drawCntrd();
		planet.drawCntrd();
		
		Color co = new Color(0,0,0,0.6f);
		g.setColor(co);
		g.fillOval(170*xScale, 20*xScale, gc.getHeight()- 10*xScale, gc.getHeight()+ 30*xScale);
		
		g.setColor(Color.white);
		
		
		if (sHlt)
			g.setColor(Color.red);
		g.drawString("Start Gaem", 7*xScale, 184*xScale);
		g.setColor(Color.white);

		if (oHlt)
			g.setColor(Color.red);
		g.drawString("Opshnz", 7*xScale, 204*xScale);
		g.setColor(Color.white);

		if (eHlt)
			g.setColor(Color.red);

		g.drawString("Exit", 7*xScale, 224*xScale);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
		xScale = Config.xScale;
		Input input = gc.getInput();
		float mX = input.getMouseX();
		float mY = input.getMouseY();
		sun.scale = 1*xScale;
		sun.x = 37*xScale;
		sun.y = 37*xScale;
		planet.x = 222*xScale;
		planet.scale = 3*xScale;

		if (mX >= 7*xScale && mX <= 47*xScale &&
				mY >= 174*xScale && mY <= 194*xScale) {
			sHlt = true;

			if (input.isMousePressed(input.MOUSE_LEFT_BUTTON)) {
				Config.ingame = true;
				game.enterState(MainKlaz.GAMEPLAYSTATE);
			}
		}
		else sHlt = false;

		//		if (mX >= 79*xScale && mX <= 20*xScale &&
		//				mY >= 184*xScale && mY <= 20*xScale) 
		//			lGameSc = 0.1f;
		//		else lGameSc = 0;
		if (mX >= 7*xScale && mX <= 47*xScale &&
				mY >= 194*xScale && mY <= 214*xScale) {
			oHlt = true;
			if (input.isMousePressed(input.MOUSE_LEFT_BUTTON)) game.enterState(MainKlaz.OPTIONSSTATE);
			//			optSc = 0.1f;
		} else oHlt = false;

		//		else optSc = 0;
		if (mX >= 7*xScale && mX <= 47*xScale &&
				mY >= 214*xScale && mY <= 234*xScale) {
			eHlt = true;
			if (input.isMousePressed(input.MOUSE_LEFT_BUTTON)) gc.exit();
		} else eHlt = false;
	}

	@Override
	public int getID() {
		return stateID;
	}

}
