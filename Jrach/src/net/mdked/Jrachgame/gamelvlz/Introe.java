package net.mdked.Jrachgame.gamelvlz;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import net.mdked.Jrachgame.core.Config;
import net.mdked.Jrachgame.utils.DynImage;
import net.mdked.Jrachgame.utils.PropManager;

public class Introe extends BasicGameState {

	private DynImage planet, sun, cokpit, pilot1, pilot2, door, cdr1, cdr2, cdr3, trpka;

	int stateID;
	private float move;
	private int timer;
	float xScale, dScale;
	private boolean drawSecondPlane, drawTP, dFP, dFVP, dSP;
	private boolean debugStop, debugRewind, debugFast;

	public Introe( int stateID)
	{
		this.stateID = stateID;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame game) throws SlickException {
		Config.changeXScale(Float.parseFloat(PropManager.prop.getProperty("xScale")));
		xScale = Config.xScale;
		int cX =gc.getWidth()/2, cY = gc.getHeight()/2;

		sun = new DynImage( new Animation(new SpriteSheet(new Image("img/intro/sun.png",false,Image.FILTER_NEAREST), 64, 64), 100 ), cX, cY, xScale);
		planet = new DynImage(new Image("img/intro/planet.png",false,Image.FILTER_NEAREST), cX,cY , xScale+2);
		move = 0;

		cokpit = new DynImage(new Image("img/intro/cockpit.png",false,Image.FILTER_NEAREST), (450*xScale),(450*xScale) , xScale+30);
		pilot1 = new DynImage(new Image("img/intro/pelod.png",false,Image.FILTER_NEAREST), cX,cY , xScale+2);
		pilot2 = new DynImage(new Image("img/intro/pelod2.png",false,Image.FILTER_NEAREST), cX,cY , xScale+2);
		door = new DynImage(new Image("img/intro/pilotdoor.png",false,Image.FILTER_NEAREST), cX,cY , xScale+2);
		cdr1 = new DynImage(new Image("img/intro/coridoor.png",false,Image.FILTER_NEAREST), cX,cY , xScale+2);
		cdr2 =new DynImage(new Image("img/intro/coridoor.png",false,Image.FILTER_NEAREST), cX,cY , xScale+2);
		cdr3 =new DynImage(new Image("img/intro/coridoor.png",false,Image.FILTER_NEAREST), cX,cY , xScale+2);
		trpka =new DynImage(new Image("img/intro/tropka.png",false,Image.FILTER_NEAREST), cX,cY , xScale+2);
		timer = 0;
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {

		g.setBackground(Color.black);
		if (!dFP)
			drawCntrd(sun);



		drawCntrd(planet);

		//		if (!dFP) {
		//planetshadow
		Color co = new Color(0,0,0,0.9f);
		g.setColor(co);
		g.fillOval(gc.getWidth()/3+move, gc.getHeight()/4, gc.getHeight()/2, gc.getHeight()/2);
		//		}
		//		if (!dFP)
		drawCntrd(cokpit); 
		if (dFP) 
			drawCntrd(trpka);


		if (!dFP && drawSecondPlane) {
			drawCntrd(pilot1);
			drawCntrd(pilot2);
		}

		if (drawTP) {
			drawCntrd(door);
		}
		if (dFP) {
			drawCntrd(cdr1);
			g.setBackground(new Color(44,44,44));
		}
		if (dFVP) {
			drawCntrd(cdr2);
		}
		if (dSP) {
			drawCntrd(cdr3);
		}

		g.setColor(Color.white);
		g.drawString(Integer.toString(timer/1000), 20, 20);

	}

	public static void drawCntrd(DynImage img) {
		if (!img.ani)
			img.image.draw(img.x- (img.image.getWidth()/2)*img.scale, img.y-(img.image.getHeight()/2)*img.scale, img.scale);
		else 
			img.anim.draw(img.x- (img.anim.getWidth()/2)*img.scale, img.y-(img.anim.getHeight()/2)*img.scale, img.anim.getWidth()*img.scale, img.anim.getHeight()*img.scale );

	}


	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta) throws SlickException {
		Input input = gc.getInput();
		//		System.out.println("intro move= " + move);	
		if (timer < 70000 ) {
			if ( !debugStop) {

				if (debugRewind) delta = -delta;
				if (debugFast) delta *= 4;

				timer += delta;


				move += delta/500f;
				sun.x -= delta/200f;
				float xd = (45*xScale)*delta/10000f;
				float yd = (55*xScale)*delta/10000f;
				float xs = delta/2000f;

				if (!dFP) {
					cokpit.x -= xd;
					cokpit.y -= yd;
					cokpit.scale -= xs;
				}

				if (drawTP)
					xs /= 3;

				if (drawSecondPlane) {
					pilot1.x -= xd;
					pilot1.y -= yd;
					pilot1.scale -= xs;

					pilot2.x -= xd;
					pilot2.y -= yd;
					pilot2.scale -= xs;
				}

				if (drawTP) {
					door.scale -= xs;
					//				trpka.scale -= xs;
				}

				if (dFP) {
					cdr1.scale -= xs;
				}
				if (dFVP) {
					cdr2.scale -= xs;
				}
				if (dSP) {
					cdr3.scale -= xs;
				}
			}
		}

		//FIXME TIMING
		if (!drawSecondPlane && timer/1000 > 40) 		{	
			pilot1.scale = cokpit.scale;
			pilot2.scale = cokpit.scale;
			pilot1.x = 515;
			pilot1.y = 925;

			pilot2.x = 1095;
			pilot2.y = 898;
			drawSecondPlane = true;

		}

		if (!drawTP && timer > 57000) {
			door.scale = cokpit.scale;
			//			trpka.scale = cokpit.scale;
			drawTP = true;
		}

		if (!dFP  && timer > 59000) {
			cdr1.scale = door.scale+0.3f;
			dFP = true;

		}
		if (!dFVP  && timer > 66500) {
			cdr2.scale = cdr1.scale+0.6f;
			dFVP = true;
		}
		//		else dFVP = false;
		if (!dSP  && timer > 70000) {
			cdr3.scale = cdr2.scale+0.5f;
			dSP = true;
		}
		//FIXME DEBUG
		if (input.isKeyPressed(Input.KEY_SPACE)) 
			debugStop = !debugStop;
		if (input.isKeyPressed(Input.KEY_LEFT)) 
			debugRewind = !debugRewind;
		if (input.isKeyPressed(Input.KEY_UP)) 
			debugFast = !debugFast;

		DynImage obja = cdr1;

		if (input.isKeyPressed(Input.KEY_NUMPAD2)) 
			obja.y += 10;
		if (input.isKeyPressed(Input.KEY_NUMPAD8)) 
			obja.y -= 10;
		if (input.isKeyPressed(Input.KEY_NUMPAD6)) 
			obja.x += 10;
		if (input.isKeyPressed(Input.KEY_NUMPAD4)) 
			obja.x -= 10;
		if (input.isKeyPressed(Input.KEY_NUMPADENTER)) 
			System.out.println("obj xy = " + obja.x + "x" + obja.y );
		//		+ " pilot2 xy = " + pilot2.x + "x" + pilot2.y

	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return stateID;
	}

}
