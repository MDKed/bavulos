package net.mdked.Jrachgame.gamelvlz;

import net.mdked.Jrachgame.core.Config;
import net.mdked.Jrachgame.core.MainKlaz;
import net.mdked.Jrachgame.utils.LoadFont;
import net.mdked.Jrachgame.utils.PropManager;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Optionz extends BasicGameState {

	int stateID, screenWidth, screenHeight;
	boolean fullscreen, hgltResChng, fulscreenmd, hgltfulmd;
	float xScale = Config.xScale, sScle=0, bkScle=0;
	int razrCh=0;
	//TODO Доделать опции, сделать выбор разрешения
	Image bg = null, savestng = null, backtomenu = null;

	UnicodeFont font;

	DisplayMode[] razr;

	public Optionz( int stateID) {
		this.stateID = stateID;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		fulscreenmd = gc.isFullscreen();
		bg = new Image("img/menu/optScreen.gif",false,Image.FILTER_NEAREST);
		savestng = bg.getSubImage(8, 179, 59, 57);
		backtomenu = bg.getSubImage(228, 178, 75, 58);
		xScale = Float.parseFloat(PropManager.prop.getProperty("xScale"));
		try {
			razr = Display.getAvailableDisplayModes();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		font =  LoadFont.LoadFont();
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		bg.draw(0, 0, xScale);
		savestng.draw((8-((savestng.getWidth()/4)*sScle))*xScale, (179-((savestng.getHeight()/4)*sScle))*xScale, xScale+sScle);
		backtomenu.draw((228-((backtomenu.getWidth()/4)*bkScle))*xScale, (178-((backtomenu.getHeight()/4)*bkScle))*xScale, xScale+bkScle);

		g.setFont(font);

		g.drawString("Текущее разрешение: ", 50*xScale, 110*xScale);
		g.drawString(gc.getWidth() + "x" + gc.getHeight() , 70*xScale, 120*xScale);
		if (hgltResChng) {
			g.setColor(new Color(0, 1, 0, 0.5f));
			g.fillRoundRect(45*xScale, 125*xScale, 70*xScale, 30*xScale, (int) (6*xScale));
			g.setColor(Color.white);
		}
		g.drawString("Сменить разрешение:", 50*xScale, 130*xScale);
		g.drawString(razr[razrCh].getWidth() + "x" + razr[razrCh].getHeight() , 70*xScale, 140*xScale);

		if (hgltfulmd) {
			g.setColor(new Color(0, 1, 0, 0.5f));
			g.fillRoundRect(115*xScale, 106*xScale, 70*xScale, 15*xScale, (int) (6*xScale));
			g.setColor(Color.white);
		}
		if (!fulscreenmd) 
			g.drawString("Полноэкранный: Выкл.", 120*xScale, 110*xScale);
		else g.drawString("Полноэкранный: Вкл.", 120*xScale, 110*xScale);

		g.drawString("Громкость:" ,  190*xScale, 110*xScale);
		g.setColor(Color.orange);
		g.fillRect(190*xScale, 120*xScale, (50*gc.getSoundVolume())*xScale, 10*xScale);
		g.setColor(Color.white);
		g.drawRect(190*xScale, 120*xScale, 50*xScale, 10*xScale);
		//g.drawString(Float.toString(gc.getSoundVolume()), 130*xScale, 140*xScale);

	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
		Input input = gc.getInput();
		float mX = input.getMouseX();
		float mY = input.getMouseY();

		if (mX >= 8*xScale && mX <= (8 + savestng.getWidth())*xScale &&
				mY >= 179*xScale && mY <= (179+savestng.getHeight())*xScale) {
			sScle = 0.1f;
			if (input.isMousePressed(input.MOUSE_LEFT_BUTTON)) saveChanges(gc);
		}
		else sScle = 0;

		if (mX >= 228*xScale && mX <= (228 + backtomenu.getWidth())*xScale &&
				mY >= 178*xScale && mY <= (178+backtomenu.getHeight())*xScale) {
			bkScle = 0.1f;
			if (input.isMousePressed(input.MOUSE_LEFT_BUTTON)) {
				if (Config.ingame) game.enterState(MainKlaz.GAMEPLAYSTATE); else
					game.enterState(MainKlaz.MAINMENUSTATE);
			}
		}
		else bkScle = 0;

		if (mX >= 40*xScale && mX <= 115*xScale &&
				mY >= 130*xScale && mY <= (150*xScale)) {
			hgltResChng = true;
			if (input.isMousePressed(input.MOUSE_LEFT_BUTTON)) {
				if (razrCh < razr.length-1 ) {
					razrCh++;
				} else razrCh = 0;
			}
		}
		else hgltResChng = false;

		if (mX >= 120*xScale && mX <= 185 *xScale &&
				mY >= 110*xScale && mY <= 120*xScale) {
			hgltfulmd = true;
			if (input.isMousePressed(input.MOUSE_LEFT_BUTTON)) fulscreenmd = !fulscreenmd;
		} else hgltfulmd = false;


		if (mX >= 190*xScale && mX <= 240*xScale &&
				mY >= 120*xScale && mY <= (130*xScale)) {
			if (input.isMouseButtonDown(input.MOUSE_LEFT_BUTTON)) {
				gc.setSoundVolume((mX-190*xScale) / (50*xScale));
			}
		}
	}
	private void saveChanges(GameContainer gc) {
		//смена разрешения
		if (razr[razrCh].getWidth() != gc.getWidth() || razr[razrCh].getHeight() != gc.getHeight() || fulscreenmd != gc.isFullscreen()) {
			PropManager.saveProperty("screenWidth", Integer.toString(razr[razrCh].getWidth()));
			PropManager.saveProperty("screenHeight", Integer.toString(razr[razrCh].getHeight()));
			xScale = (int) razr[razrCh].getHeight()/240f;
			PropManager.saveProperty("xScale", Float.toString(xScale));
			Config.changeXScale(xScale);
			font = Config.font;
			AppGameContainer cnt = (AppGameContainer) gc;
			try {
				cnt.setDisplayMode(razr[razrCh].getWidth(), razr[razrCh].getHeight(), fulscreenmd);
			} catch (SlickException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//Звук
		PropManager.saveProperty("soundLevel", Float.toString(gc.getSoundVolume()));
		
		PropManager.saveProperties();
	}
	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return stateID;
	}

}
