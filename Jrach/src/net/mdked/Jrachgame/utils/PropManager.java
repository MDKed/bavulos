package net.mdked.Jrachgame.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropManager {
public static Properties prop;
	public static void saveProperty(String key, String value) {
		// prop = loadProperties();

		prop.setProperty(key, value);
		saveProperties();
	}
	
	public static Properties loadProperties() {
		prop = new Properties();
		File propF = new File("config.prop");
		if (propF.exists()) {
			try {
				prop.load(new FileInputStream(propF));
			} catch (IOException e) {
				e.printStackTrace();
			}
			return prop;
		} else {
			// default settings
			prop.setProperty("screenWidth", "800");
			prop.setProperty("screenHeight", "600");
			prop.setProperty("xScale", "2.5f");
			prop.setProperty("soundLevel", "1.0f");
			prop.setProperty("musicOn", "false");
			prop.setProperty("font", "fnt/FreeSans.ttf");
			prop.setProperty("cMapX", "0");
			prop.setProperty("cMapY", "0");
			saveProperties();
			return prop;
		}
	}

	public static void saveProperties() {
		try {
			prop.store(new FileOutputStream("config.prop"), null);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
