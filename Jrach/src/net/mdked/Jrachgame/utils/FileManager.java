package net.mdked.Jrachgame.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.world.Data;
import net.mdked.Jrachgame.world.EncounterMan;
import net.mdked.Jrachgame.world.World;
import net.mdked.Jrachgame.world.mapgen.MapChunk;
import net.mdked.Jrachgame.world.mapgen.MapManager;
import net.mdked.Jrachgame.world.mapgen.ObjChunk;




public class FileManager {

	public static void SaveObject(Object obj) {
		File where = new File("");

		Data sData = (Data) obj;
		switch (sData.dType) {
		case 1:
			MapChunk chunk = (MapChunk) obj;
			where = new File("map/"+ chunk.xCh +"x"+chunk.yCh+".map");
			break;
		case 2:
			ObjChunk ochunk = (ObjChunk) obj;
			where = new File("map/obj/"+ ochunk.xOCh +"x"+ 
					ochunk.yOCh+".och");
			break;
		}

		try
		{
			FileOutputStream fileOut =
					new FileOutputStream(where);
			ObjectOutputStream out =
					new ObjectOutputStream(fileOut);
			out.writeObject(obj);
			out.close();
			fileOut.close();
		}catch(IOException i)
		{
			System.out.println(" SaveObject error " + where);
			i.printStackTrace();
		}
	}


	/**
	 * 1 - map
	 * 2 - oChunk
	 * @param xCh
	 * @param yCh
	 * @param type
	 * @return
	 */
	public static Object loadObject(int xCh, int yCh, int type) {

		//загрузка карты
		Object returno = null;
		File objToLoad = new File("");

		switch (type) {
		//map
		case 1:
//			if (DasGame.mapMan.isOrdered(xCh, yCh)) return null;

			objToLoad = new File("map/"+ xCh +"x"+ yCh + ".map");
			break;

			//object chunk
		case 2:
//			if (DasGame.mapMan.isOChOrd(xCh, yCh)) return null;
			objToLoad = new File("map/obj/"+ xCh +"x"+ yCh + ".och");
			break;

		}

		if (!objToLoad.exists()) {
//			System.out.println(" нету map файла " + objToLoad + " возвращаем нуль и заказываем новую");
			switch (type) {

			case 1:
//				DasGame.mapMan.generateMapp(xCh, yCh);
				return null;

			case 2:
//				DasGame.mapMan.generateOChnk(xCh, yCh);
				return null;

			}
		}

		try
		{

			FileInputStream fileIn =
					new FileInputStream(objToLoad);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			returno =  in.readObject();
			in.close();
			fileIn.close();
		}catch(IOException i)
		{
			i.printStackTrace();

		}catch(ClassNotFoundException c)
		{
			//			System.out.println("MapChunk not found, возвращаем новую карту");
			c.printStackTrace();
			//			return new MapChunk(xCh, yCh);
		}
		switch (type) {

		case 1:
//			World.map = (MapChunk) returno;
			World.setMap((MapChunk) returno);
			break;
		case 2:
//			World.insertObjFromChunkToWorld((ObjChunk) returno);
//			EncounterMan.randomAnimalSpawn(xCh, yCh);
//
//			System.out.println(" objChunk loaded");
			break;
		}

		return returno;


	}
}
