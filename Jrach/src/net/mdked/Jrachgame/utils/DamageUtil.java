package net.mdked.Jrachgame.utils;

import net.mdked.Jrachgame.player.inv.Weapon;
import net.mdked.Jrachgame.world.Charakter;
import net.mdked.Jrachgame.world.Mobster;

public class DamageUtil {

	public static void dealDamage(Weapon attack, Charakter targer) {
//		float dmg = 0;
		
		/*
		 * сделать разброс пробития равный 10 пунктам каждый пункт
		 * идет за 5%
		 */
		
		
		//blunt
		//найти разницу между защитой и нападением и потом отнять/прибавить её
		int armor = targer.equip.getArmor();
		int penetration = attack.pen;
		
		int raz =  penetration - armor;
		
		
		int proz = 50 + ( 5 * raz);
//		System.out.println(" deal Damage proz = " + proz);

		if ( Math.random()* 100 <= proz) {
			targer.hp -= attack.damage;
			targer.emmitText(Integer.toString(attack.damage), 20, 1);
			if (targer.enttype == 1) {
				Mobster mob = (Mobster) targer;
				mob.target = attack.mastr;
				mob.takeDamage();
			}
			targer.makeSound(1);
//			System.out.println("   Damage = " + attack.damage + " animal hp = " + targer.hp);
		} 
//		else System.out.println("   No Damage");
		
	}
	
}
