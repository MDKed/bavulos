package net.mdked.Jrachgame.utils;

import org.newdawn.slick.Image;

public class SpriteSheetRazor {
	
	public static Image[] rapeSheet(Image sprite, int sizeMatrix, int spriteSize) {
		
		Image ground[] = new Image[sizeMatrix];
		int spriteTblWid = sprite.getWidth() / spriteSize;

		for ( int gn = 0; gn < sizeMatrix; gn ++ ) {
			int x = spriteSize * (gn % spriteTblWid);
			int y = ( gn/spriteTblWid ) * spriteSize;
//			System.out.println(x + "x" + y);
			ground[gn] = sprite.getSubImage(x, y, spriteSize, spriteSize );
		}
		return ground;
	}
	
public static Image[] rapeSheet(Image sprite, int sizeMatrix, int spriteSize, int intrvl) {
		
	Image ground[] = new Image[sizeMatrix];
		
		int spriteTblWid = sprite.getWidth() / spriteSize;
		
		

		for ( int gn = 0; gn < sizeMatrix; gn ++ ) {
			int zx = ( gn % spriteTblWid );
			int x = spriteSize * zx + (zx * intrvl) ;
			
			int zy = ( gn / spriteTblWid );
			int y = zy * spriteSize + (zy * intrvl);
			
//			System.out.println(x + "x" + y);
			ground[gn] = sprite.getSubImage(x, y, spriteSize, spriteSize );
		}
		return ground;
	}
	

}
