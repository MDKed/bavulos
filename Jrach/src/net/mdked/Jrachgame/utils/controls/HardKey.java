package net.mdked.Jrachgame.utils.controls;

public class HardKey extends HotKey {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8465240804136665339L;
	public int action;
	
	/**
	 * 1= use in hand item
	 * @param key
	 * @param action
	 */
	public HardKey(int key, int action) {
		super(key);
		this.action = action;
	}

}
