package net.mdked.Jrachgame.utils.controls;

import java.io.Serializable;

public class HotKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4382333635292604604L;

	
	public int key, hkType;
	
	public HotKey(int key) {
		this.key = key;
		hkType = -1;
	}

	
}
