package net.mdked.Jrachgame.utils.controls;

import java.io.Serializable;
import java.util.ArrayList;

import org.newdawn.slick.Input;

import net.mdked.Jrachgame.core.CollisionMan;
import net.mdked.Jrachgame.core.DasGame;
import net.mdked.Jrachgame.hud.ActionBar;
import net.mdked.Jrachgame.hud.InteractionWindow;
import net.mdked.Jrachgame.hud.TextCloud;
import net.mdked.Jrachgame.player.Player;
import net.mdked.Jrachgame.player.inv.ItmContainer;
import net.mdked.Jrachgame.utils.Pifagor;
import net.mdked.Jrachgame.world.DragblObj;
import net.mdked.Jrachgame.world.Facility;
import net.mdked.Jrachgame.world.Pribor;
import net.mdked.Jrachgame.world.Wall;
import net.mdked.Jrachgame.world.World;

public class Controls implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2055277143350363386L;


	public ArrayList<HotKey> hKeys;
	public ArrayList<HardKey> hardKeys;

	private float mx,my;

	//	public Player playerC;

	public Controls() {
		hKeys = new ArrayList<HotKey>();
		hardKeys = new ArrayList<HardKey>();

		addHkey(new HotKey(Input.KEY_3));
		addHkey(new HotKey(Input.KEY_4));

		//		playerC = World.player;

		initHardKeys();
	}

	private void initHardKeys() {
		hardKeys.add(new HardKey(Input.MOUSE_LEFT_BUTTON, 1));
		hardKeys.add(new HardKey(Input.MOUSE_RIGHT_BUTTON, 2));
	}

	public void addHkey(HotKey hkey) {
		for (int a = 0; a < hKeys.size(); a++) {
			if (hKeys.get(a).key == hkey.key) {
				hKeys.remove(a);
				break;
			}
		}
		hKeys.add(hkey);
	}



	public void isButPressed(Input input) {


		for (int a = 0; a < hKeys.size(); a ++) 
			if ( input.isKeyPressed(hKeys.get(a).key) )

				//					switch (hKeys.get(a).hkType) {
				//					case -1:
				if (DasGame.interwin)
					assignHotkey(hKeys.get(a).key);
		//						break;
		//					case 0:
		//item;
		//					System.out.println( "using hkey " + hKeys.get(a).key);
				else if (hKeys.get(a).hkType != -1) {
					HotItem hitm = (HotItem) hKeys.get(a);
					World.player.useItem(hitm.item);
				}
		//						break;
		//					}


		mx = input.getMouseX();
		my = input.getMouseY();

		for (int b = 0; b < hardKeys.size(); b++)
			if ( input.isKeyPressed(hardKeys.get(b).key) || input.isMousePressed(hardKeys.get(b).key) )
				action(hardKeys.get(b).action);
	}

	private void action(int action) {
		switch (action) {
		case 1:
			mouse1();
			//			World.player.useInHands();
			break;
		case 2:
			mouse2();
			break;
		}

	}

	private void mouse2() {
		if (!World.acbar && !DasGame.construct && !DasGame.interwin) {

			DasGame.akbar = new ActionBar(mx, my);
			World.acbar = true;

			//			DasGame.mouz.setXY(DasGame.mX, DasGame.mY);
			CollisionMan.checkCollison(DasGame.mouz);

		} else if (World.acbar) {
			DasGame.delActBar();
		} else if (DasGame.construct) {
			World.player.recipeBuff = null;
			DasGame.construct = false;
			DasGame.consFac = null;
		}
	}

	private void mouse1() {
		Player playerC = World.player;
		//		System.out.println("mouse1");
		if (!World.acbar && !DasGame.interwin && !DasGame.construct) {
			//			DasGame.mouz.setXY(DasGame.mX, DasGame.mY);
			//			CollisionMan.checkCollison(DasGame.mouz);

			playerC.useInHands();
			//			System.out.println(" mxy = " + DasGame.mX + "x" + DasGame.mY + " plxy = " + playerC.x + "x" + playerC.y);

		} else if (World.acbar) {
			//			System.out.println(" getAction " + akbar.getAction());

			switch (DasGame.akbar.getAction()) {
			case 1:
				if (!DasGame.txtcld) {
					DasGame.textC = new TextCloud(DasGame.akbar.x,DasGame.akbar.y, DasGame.akbar.objInfo);
					DasGame.txtcld = true;
					//System.out.println(" made textC");

				} else {
					DasGame.txtcld = false;
					DasGame.textC = null;

				}

				break;
			case 2:
				if (!playerC.pullin) {
					DragblObj pula = (DragblObj) DasGame.akbar.tgt;
					playerC.pullAnimal(pula);
				} else {
					playerC.stopPullin();
				}

				break;

			case 3:
				//pererabotka
				Facility fac = (Facility) DasGame.akbar.tgt;
				fac.pererab(playerC.pull);
				DasGame.delActBar();
				break;

			case 4:
				//открытие инвентаря
				if (!DasGame.interwin) {
					DasGame.interw = new InteractionWindow(1, DasGame.akbar.tgt);
					DasGame.interwin = true;

				} else {
					DasGame.interwin = false;
					DasGame.interw = null;
				}
				break;
			case 5:
				//dobavit drov
				int num =  playerC.inventory.haveItem(2, 1);
				Pribor prv = (Pribor) DasGame.akbar.tgt;
				prv.addFuel( (byte) 0, (byte) 1);
				playerC.inventory.removeItem(num, 1);
				DasGame.delActBar();
				//activePribors(delta);
				DasGame.checkPribors();
				break;
			case 6:
				//dobavit kost
				int numi =  playerC.inventory.haveItem(1, 1);
				Pribor prva = (Pribor) DasGame.akbar.tgt;
				prva.addFuel( (byte) 0, (byte) 0);
				playerC.inventory.removeItem(numi, 1);
				DasGame.delActBar();
				//activePribors(delta);
				DasGame.checkPribors();
				break;
			case 7:
				//nabrat vodi
				//				System.out.println(Pifagor.getDist(playerC, DasGame.mouz));
				if (Pifagor.getDist(playerC, DasGame.mouz) < 15) {
					ItmContainer cont = playerC.inventory.getEmptWatCont();
					if (cont != null) 
						cont.fillFullWith(DasGame.akbar.terrain);
				}
				break;
			case 8:
				//sbor trav
				System.out.println(" collect " + World.player.target);
				World.player.collect(World.player.target);
				DasGame.delActBar();
				break;


			}
			//delActBar();

		}
		else if (DasGame.interwin) {
			DasGame.interw.action();
		}
		else if (DasGame.construct && !CollisionMan.checkCollison(DasGame.consFac) && playerC.playerAction(playerC.recipeBuff.getPlAct())) {
			//			System.out.println("  Buildin");
			playerC.recipeBuff.remInv();
			playerC.recipeBuff = null;
			DasGame.construct = false;
			World.addEntityToWorld(DasGame.consFac, false);
			switch (DasGame.consFac.enttype) {
			
			case 2:
				Facility mfac = (Facility) DasGame.consFac;
				if (mfac.facType == 2)
					World.player.giveTechLev1();
				break;
				
			case 11:
				//wall
				Wall wa = (Wall) DasGame.consFac;
				wa.checkType(true);
				break;
			}
			//						World.pribrs.add((Pribor) consFac);
			DasGame.checkPribors();
			//					}
			DasGame.consFac = null;
		}
	}


	private void assignHotkey(int key) {
		if (DasGame.interwin) {
			if (DasGame.interw.type == 2 && DasGame.interw.higlt1) {
				for (int a = 0; a < hKeys.size(); a++) 
					if (key == hKeys.get(a).key) {
						hKeys.remove(a);
						break;
					}


				addHkey(new HotItem(key,  DasGame.interw.getHGItem()));
			}
		}
	}

}
