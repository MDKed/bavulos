package net.mdked.Jrachgame.utils.controls;

import net.mdked.Jrachgame.player.inv.Item;

public class HotItem extends HotKey {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2247437152507524123L;
	
	public Item item;
	
	public HotItem(int key, Item itm) {
		super(key);
		
		hkType = 0;
		item = itm;
		
//		System.out.println(" new Hot Item " + key + " itm=" + itm.name);
	}
	
	
}
