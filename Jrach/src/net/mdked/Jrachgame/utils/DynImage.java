package net.mdked.Jrachgame.utils;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;

public class DynImage {

	public Image image;
	public Animation anim;
	public float x, y, scale;
	public boolean ani;
	
	public DynImage(Image img, float x, float y, float scale) {
		image = img;
		this.x = x;
		this.y = y;
		this.scale = scale;
	}
	public DynImage(Animation anim, float x, float y, float scale) {
		ani = true;
		this.anim = anim;
		this.x = x;
		this.y = y;
		this.scale = scale;
	}
	public void drawCntrd() {
		if (!ani)
			image.draw(x- (image.getWidth()/2)*scale, y-(image.getHeight()/2)*scale, scale);
		else 
			anim.draw(x- (anim.getWidth()/2)*scale, y-(anim.getHeight()/2)*scale, anim.getWidth()*scale, anim.getHeight()*scale );

	}
}
