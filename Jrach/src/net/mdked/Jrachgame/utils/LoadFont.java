package net.mdked.Jrachgame.utils;


import net.mdked.Jrachgame.core.DasGame;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public class LoadFont {
public static UnicodeFont LoadFont() {
	UnicodeFont font = null;
	try {
		font = new UnicodeFont(PropManager.prop.getProperty("font"), 15, false, false);
		font.addAsciiGlyphs();
		font.addGlyphs(400, 2000);
		font.getEffects().add(new ColorEffect());  // Create a default white color effect
		font.loadGlyphs();
	} catch (SlickException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return font;
}
}
