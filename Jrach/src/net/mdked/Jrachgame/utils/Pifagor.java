package net.mdked.Jrachgame.utils;

import grid.GridLocation;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;

import net.mdked.Jrachgame.player.Player;
import net.mdked.Jrachgame.world.DragblObj;
import net.mdked.Jrachgame.world.Entity;
import net.mdked.Jrachgame.world.Missile;
import net.mdked.Jrachgame.world.Mobster;
import net.mdked.Jrachgame.world.World;
import net.mdked.Jrachgame.world.efcs.Sprtefct;
import net.mdked.Jrachgame.world.mapgen.MapChunk;

public class Pifagor {


	public static double getDist(float x1, float x2, float y1, float y2) {
		return Math.sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) ) ;
	}

	public static double getDist(Entity one, Entity two) {
		return getDist(one.x, two.x, one.y, two.y);
	}

	public static double getDirectionTo(float x1, float y1, float x2, float y2) {
		return	Math.atan2((y1-y2), (x1-x2));
	}

	public static double getDirectionTo(Entity one, Entity two) {
		return getDirectionTo(one.x, one.y, two.x, two.y);
	}

	//	public static MapChunk getBMapfromChunk(int chX, int chY) {
	//		int[] arhXY = Pifagor.getXchYch(chX, chY, true);
	//		MapChunk bigmap =  MapGen.getBigMap(arhXY[0], arhXY[1]);
	//				return bigmap;
	//	}
/**
 * otn
 * @param x
 * @param y
 * @param end
 * @return
 */
	public static GridLocation getGridLoc(int oxK, int oyK, boolean end ) {
//		int[] kotr = getOtnXYKotr(x, y);
		GridLocation ret = new GridLocation(oxK, oyK, end);
		return ret;
	}
	
	public static int[] getCh(Entity ent) {
		int[] kotr = getKOTRfromfXY(ent.x, ent.y);
		return getXchYch(kotr[0], kotr[1]);
	}

	
	/**
	 * kotr
	 * @param x
	 * @param y
	 * @return возвращает чанк из ху
	 */
	public static int[] getXchYch( int x, int y ) {
		//, boolean bigbig ? World.arhipelago :
		int size =  World.chunksize;
		if (x < 0) x -= size;
		if (y < 0) y -= size;

		int[] ret = { x / size, y / size };
		return ret;
	}

	public static int[] getXYfOP( int kX, int kY ) {
		int[] ch = getXchYch(kX, kY),
				xy = getXYminCHfd(kX, kY);
		int[] x = {(ch[0]+xy[0])*32, (ch[1]+xy[1])*32};
		return x;

	}

	public static int[] getOtnXYKotr( float x, float y ) {
		int[] kotr = getKOTRfromfXY(x, y);
		return getXYminCHfd(kotr[0], kotr[1]);
	}
	
	/**
	 * kotr
	 * @param x
	 * @param y
	 * @return
	 */
	public static int[] getXYminCHfd( int x, int y) {
		//		big ? World.arhipelago : , boolean big
		int size = World.chunksize;

		int[] ret = new int[2];

		if (x < 0) 		
//			ret[0] = (size-1) - Math.abs(x % size);
			ret[0] = size - Math.abs(x % size);
		else 
			ret[0] = Math.abs(x % size);

		if (y < 0) 
//			ret[1] = (size-1) - Math.abs(y % size);
			ret[1] = size - Math.abs(y % size);
		else 
			ret[1] = Math.abs(y % size);



		return ret;
	}
	//, boolean big big ? World.arhipelago :
	public static int[] getXYminCH( int x, int y) {
		int size = World.chunksize;

		if (x < 0) x -= size;
		if (y < 0) y -= size;
		//
		//		int[] ch = getXchYch(x, y);
		//
		int[] ret = new int[2];

		//		ret[0] = Math.max(0, Math.min(31, Math.abs(x - ( ch[0] * World.chunksize ))));
		//		ret[1] = Math.max(0, Math.min(31, Math.abs(y - ( ch[1] * World.chunksize ))));
		ret[0] = Math.abs(x % size);
		ret[1] = Math.abs(y % size);
		return ret;
	}

	public static int[] getXYfromKOTR(MapChunk map, int xK, int yK) {
		int[] xyCH = {map.xCh, map.yCh};
		return getXYfromKOTR(xyCH, xK, yK);
	}
	
	public static int[] getXYfromKOTR(int[] xyCh, int xK, int yK) {
		int[] ret = new int[2];
		ret[0] = (((xyCh[0]*World.chunksize) + xK) * 32) + 16;
		ret[1] = (((xyCh[1]*World.chunksize) + yK) * 32) + 16;
		return ret;
	}
	
	public static int[] getKOTRfromfXY(float x, float y) {
		float chX = (x < 0) ? x - 32: x;
		float chY = (y < 0) ? y - 32: y;
		int[] ret = new int[2];
		ret[0] = (int) (chX/32);
		ret[1] = (int) (chY/32);
		return ret;
	}

	public static int[] getChunkFromOChunk( int x, int y ) {
		//nyjen kotr y po nemy viyasnit
		return getXchYch(x*World.objchunksize, y*World.objchunksize);
	}

	public static int[] getOCHfXY( float x, float y ) {
		int[] ent = getKOTRfromfXY(x, y);
		return getOCHfXY(ent[0], ent[1]);
	}

	public static int[] getOCHfXY( int x, int y ) {

		int ochsz = World.objchunksize;
		int[] ret = new int[2];

		if (x < 0) x -= ochsz;
		if (y < 0) y -= ochsz;
		ret[0] = x / ochsz;
		ret[1] = y / ochsz;

		return ret;
	}
	public static void moveByRotWind(Entity ent, int delta) {
		float spd =0, wndspd = World.windspd/1000f, rot = 0;

		switch (ent.enttype) {
		case 9:
			Sprtefct ef = (Sprtefct) ent;
			spd = ef.speed/1000f;
			float rand = (float) (Math.random()*0.2f) - 0.1f;
			rot = corrRot(ef.rot+rand);
			break;
		}

		ent.x += (spd * Math.sin(rot)) + (wndspd * Math.sin(World.windrot));
		ent.y -= (spd * Math.cos(rot)) + (wndspd * Math.cos(World.windrot));
	}

	public static float corrRot(float rot) {
		return (float) (rot - Math.toRadians(90));
	}

	public static float getSpeed(float speed, int delta) {
		return (speed/1000f) * delta;
	}
	
	public static void moveByRot(Entity ent, int delta) {

		float spe = 0,rotation = 0;
//		System.out.println(ent.enttype);
		switch (ent.enttype) {
		case 1:
		case 12:
			//mobsta
			Mobster mob = (Mobster) ent;
			spe = getSpeed(mob.speed, delta);
			rotation = corrRot(mob.rot);
			break;
			
		case 5:
			Missile msl = (Missile) ent;
			spe = getSpeed(msl.speed, delta);
			rotation = corrRot(msl.rot);
			break;

		case 6:
			Player plr = (Player) ent;
			spe = getSpeed(plr.speed, delta);
			rotation = corrRot(plr.rot);
			break;

		case 7:
			DragblObj drg = (DragblObj) ent;
			spe =  getSpeed(drg.host.speed, delta);
			rotation = corrRot((float) getDirectionTo(drg, drg.host));
			break;
		}


		ent.x += spe * Math.sin(rotation);
		ent.y -= spe * Math.cos(rotation);
	}

	public static Animation[] makeAnimAnim( Animation ani, int aType) {
		Animation[] retAn = new Animation[14];

		switch (aType) {
		case 0:
			//deer
			Image[] idldn = {ani.getImage(13), ani.getImage(14), ani.getImage(15)};
			Animation IdleDwn = new Animation(idldn, 500);
			IdleDwn.setDuration(1, 1200);
			IdleDwn.setDuration(2, 1200);

			retAn[0] = IdleDwn;

			Image[] rundn = {ani.getImage(5), ani.getImage(6), ani.getImage(7)};
			Animation WkDwn = new Animation(rundn, 100);
			WkDwn.setPingPong(true);

			retAn[1] = WkDwn;

			Image[] idlrt = {ani.getImage(0), ani.getImage(1)};
			Animation IdleRight = new Animation(idlrt, 500);
//			IdleRight.setDuration(0, 1200);

			retAn[2] = IdleRight;

			Image[] runrt = {ani.getImage(2), ani.getImage(3), ani.getImage(4)};
			Animation WkRight = new Animation(runrt, 100);
			WkRight.setPingPong(true);

			retAn[3] = WkRight;

			Image[] runup = {ani.getImage(8), ani.getImage(9), ani.getImage(10)};
			Animation RunUp = new Animation(runup, 100);
			RunUp.setPingPong(true);

			retAn[4] = RunUp;

			Image[] idlup = { ani.getImage(11), ani.getImage(12)};
			Animation IdleUp = new Animation(idlup, 300);

			retAn[5] = IdleUp;

//			Image[] swimdn = {plr.getImage(13), plr.getImage(14), plr.getImage(15)};
//			Animation plrSwimDn = new Animation(swimdn, 100);
//			plrSwimDn.setPingPong(true);
//			TODO swiming anim
			retAn[6] = WkDwn;

//			Image[] drwn = {plr.getImage(16), plr.getImage(17), plr.getImage(18)};
//			Animation plrDrwn = new Animation(drwn, 100);
//			plrDrwn.setPingPong(true);

			retAn[7] = WkDwn;

//			Image[] swimrt = {plr.getImage(19), plr.getImage(20), plr.getImage(21)};
//			Animation plrSwimRight = new Animation(swimrt, 100);
//			plrSwimRight.setPingPong(true);

			retAn[8] = WkDwn;

//			Image[] swimup = {plr.getImage(22), plr.getImage(23), plr.getImage(24)};
//			Animation plrSwimUp = new Animation(swimup, 100);
//			plrSwimUp.setPingPong(true);

			retAn[9] = WkDwn;

			Image[] atkdwn = {ani.getImage(19), ani.getImage(20), ani.getImage(21)};
			Animation AtkDwn = new Animation(atkdwn, 100);
			AtkDwn.setPingPong(true);

			retAn[10] = AtkDwn;

			Image[] atkrt = {ani.getImage(16), ani.getImage(17), ani.getImage(18)};
			Animation AtkRight = new Animation(atkrt, 100);
			AtkRight.setPingPong(true);

			retAn[11] = AtkRight;

			Image[] atkup = {ani.getImage(22), ani.getImage(23), ani.getImage(24)};
			Animation AtkUp = new Animation(atkup, 100);
			AtkUp.setPingPong(true);

			retAn[12] = AtkUp;
			
			
			Image[] dead = {ani.getImage(25)};
			Animation deadyt = new Animation(dead, 100);
			retAn[13] = deadyt;
			break;
		}
		
		return retAn;
	}
	/*
	 * 0 - idl down
	 * 1 - wk down
	 * 2 idl right
	 * 3 - wk right
	 * 4 - wk up
	 * 5 - idl up
	 * 6 - swim dn
	 * 7 - drown
	 * 8 - swim right
	 * 9 - swim up
	 * 10 - atk dwn
	 * 11 - atk right
	 * 12 - atk up
	 * 13 - die
	 * 
	 */
	public static Animation[] makePlrAnim( Animation plr ) {

		Animation[] retAn = new Animation[14];

		Image[] idldn = {plr.getImage(0), plr.getImage(1)};
		Animation plrIdleDwn = new Animation(idldn, 100);
		plrIdleDwn.setDuration(0, 1200);

		retAn[0] = plrIdleDwn;

		Image[] rundn = {plr.getImage(2), plr.getImage(3), plr.getImage(4)};
		Animation plrWkDwn = new Animation(rundn, 100);
		plrWkDwn.setPingPong(true);

		retAn[1] = plrWkDwn;

		Image[] idlrt = {plr.getImage(5), plr.getImage(6)};
		Animation plrIdleRight = new Animation(idlrt, 100);
		plrIdleRight.setDuration(0, 1200);

		retAn[2] = plrIdleRight;

		Image[] runrt = {plr.getImage(7), plr.getImage(8), plr.getImage(9)};
		Animation plrWkRight = new Animation(runrt, 100);
		plrWkRight.setPingPong(true);

		retAn[3] = plrWkRight;

		Image[] runup = {plr.getImage(10), plr.getImage(11), plr.getImage(12)};
		Animation plrRunUp = new Animation(runup, 100);
		plrRunUp.setPingPong(true);

		retAn[4] = plrRunUp;

		Image[] idlup = { plr.getImage(11)};
		Animation plrIdleUp = new Animation(idlup, 100);

		retAn[5] = plrIdleUp;

		Image[] swimdn = {plr.getImage(13), plr.getImage(14), plr.getImage(15)};
		Animation plrSwimDn = new Animation(swimdn, 100);
		plrSwimDn.setPingPong(true);

		retAn[6] = plrSwimDn;

		Image[] drwn = {plr.getImage(16), plr.getImage(17), plr.getImage(18)};
		Animation plrDrwn = new Animation(drwn, 100);
		plrDrwn.setPingPong(true);

		retAn[7] = plrDrwn;

		Image[] swimrt = {plr.getImage(19), plr.getImage(20), plr.getImage(21)};
		Animation plrSwimRight = new Animation(swimrt, 100);
		plrSwimRight.setPingPong(true);

		retAn[8] = plrSwimRight;

		Image[] swimup = {plr.getImage(22), plr.getImage(23), plr.getImage(24)};
		Animation plrSwimUp = new Animation(swimup, 100);
		plrSwimUp.setPingPong(true);

		retAn[9] = plrSwimUp;

		Image[] atkdwn = {plr.getImage(25), plr.getImage(26), plr.getImage(27)};
		Animation plrAtkDwn = new Animation(atkdwn, 100);
		plrAtkDwn.setPingPong(true);

		retAn[10] = plrAtkDwn;

		Image[] atkrt = {plr.getImage(28), plr.getImage(29), plr.getImage(30)};
		Animation plrAtkRight = new Animation(atkrt, 100);
		plrAtkRight.setPingPong(true);

		retAn[11] = plrAtkRight;

		Image[] atkup = {plr.getImage(31), plr.getImage(32), plr.getImage(33)};
		Animation plrAtkUp = new Animation(atkup, 100);
		plrAtkUp.setPingPong(true);

		retAn[12] = plrAtkUp;
		
		//TODO нарисовать гибель персонажа
		retAn[13] = plrDrwn;

		return retAn;
	}
}
